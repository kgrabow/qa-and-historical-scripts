-- Generate scripts to LTRIM and RTRIM any varchar columns, then, generates
-- scripts to set '' columns to NULL
 
DECLARE @SQLTRIM VARCHAR(MAX);
DECLARE @SQLNULL VARCHAR(MAX);
DECLARE @SQLTAB VARCHAR(MAX);

DECLARE @TableName NVARCHAR(128);

DECLARE @adt_final VARCHAR(100);
DECLARE @lab_final VARCHAR(100);
DECLARE @nurse_final VARCHAR(100);
DECLARE @vital_final VARCHAR(100);

 
SET @adt_final = 'adt_final';
SET @lab_final = 'lab_final';
SET @nurse_final = 'nurse_final';
SET @vital_final = 'vital_final';

-- adt 
SELECT @SQLTRIM = COALESCE(@SQLTRIM+',[', '[')+COLUMN_NAME+']=RTRIM(LTRIM(['+COLUMN_NAME+']))'+CHAR(10)
FROM INFORMATION_SCHEMA.COLUMNS
WHERE TABLE_NAME = @adt_final
      AND DATA_TYPE = 'varchar';
      
SET @SQLTRIM = 'UPDATE ['+@adt_final+'] '+CHAR(10)+'SET '+@SQLTRIM;
PRINT @SQLTRIM;
 
SELECT @SQLNULL = COALESCE(@SQLNULL+',[', '[')+COLUMN_NAME+']=NULLIF(['+COLUMN_NAME+'],'''')'+CHAR(10)
FROM INFORMATION_SCHEMA.COLUMNS
WHERE TABLE_NAME = @adt_final
      AND DATA_TYPE = 'varchar';
SET @SQLNULL = 'UPDATE ['+@adt_final+'] '+CHAR(10)+'SET '+@SQLNULL;
PRINT @SQLNULL;
 
SELECT @SQLTAB = COALESCE(@SQLTAB+',[', '[')+COLUMN_NAME+']=REPLACE(['+COLUMN_NAME+'],CHAR(9),'''')'+CHAR(10)
FROM INFORMATION_SCHEMA.COLUMNS
WHERE TABLE_NAME = @adt_final
      AND DATA_TYPE = 'varchar';
SET @SQLTAB = 'UPDATE ['+@adt_final+'] '+CHAR(10)+'SET '+@SQLTAB;
PRINT @SQLTAB;

-- lab
SELECT @SQLTRIM = COALESCE(@SQLTRIM+',[', '[')+COLUMN_NAME+']=RTRIM(LTRIM(['+COLUMN_NAME+']))'+CHAR(10)
FROM INFORMATION_SCHEMA.COLUMNS
WHERE TABLE_NAME = @lab_final
      AND DATA_TYPE = 'varchar';
      
SET @SQLTRIM = 'UPDATE ['+@lab_final+'] '+CHAR(10)+'SET '+@SQLTRIM;
PRINT @SQLTRIM;
 
SELECT @SQLNULL = COALESCE(@SQLNULL+',[', '[')+COLUMN_NAME+']=NULLIF(['+COLUMN_NAME+'],'''')'+CHAR(10)
FROM INFORMATION_SCHEMA.COLUMNS
WHERE TABLE_NAME = @lab_final
      AND DATA_TYPE = 'varchar';
SET @SQLNULL = 'UPDATE ['+@lab_final+'] '+CHAR(10)+'SET '+@SQLNULL;
PRINT @SQLNULL;
 
SELECT @SQLTAB = COALESCE(@SQLTAB+',[', '[')+COLUMN_NAME+']=REPLACE(['+COLUMN_NAME+'],CHAR(9),'''')'+CHAR(10)
FROM INFORMATION_SCHEMA.COLUMNS
WHERE TABLE_NAME = @lab_final
      AND DATA_TYPE = 'varchar';
SET @SQLTAB = 'UPDATE ['+@lab_final+'] '+CHAR(10)+'SET '+@SQLTAB;
PRINT @SQLTAB;

-- nurse
SELECT @SQLTRIM = COALESCE(@SQLTRIM+',[', '[')+COLUMN_NAME+']=RTRIM(LTRIM(['+COLUMN_NAME+']))'+CHAR(10)
FROM INFORMATION_SCHEMA.COLUMNS
WHERE TABLE_NAME = @nurse_final
      AND DATA_TYPE = 'varchar';
      
SET @SQLTRIM = 'UPDATE ['+@nurse_final+'] '+CHAR(10)+'SET '+@SQLTRIM;
PRINT @SQLTRIM;
 
SELECT @SQLNULL = COALESCE(@SQLNULL+',[', '[')+COLUMN_NAME+']=NULLIF(['+COLUMN_NAME+'],'''')'+CHAR(10)
FROM INFORMATION_SCHEMA.COLUMNS
WHERE TABLE_NAME = @nurse_final
      AND DATA_TYPE = 'varchar';
SET @SQLNULL = 'UPDATE ['+@nurse_final+'] '+CHAR(10)+'SET '+@SQLNULL;
PRINT @SQLNULL;
 
SELECT @SQLTAB = COALESCE(@SQLTAB+',[', '[')+COLUMN_NAME+']=REPLACE(['+COLUMN_NAME+'],CHAR(9),'''')'+CHAR(10)
FROM INFORMATION_SCHEMA.COLUMNS
WHERE TABLE_NAME = @nurse_final
      AND DATA_TYPE = 'varchar';
SET @SQLTAB = 'UPDATE ['+@nurse_final+'] '+CHAR(10)+'SET '+@SQLTAB;
PRINT @SQLTAB;

-- vital
SELECT @SQLTRIM = COALESCE(@SQLTRIM+',[', '[')+COLUMN_NAME+']=RTRIM(LTRIM(['+COLUMN_NAME+']))'+CHAR(10)
FROM INFORMATION_SCHEMA.COLUMNS
WHERE TABLE_NAME = @vital_final
      AND DATA_TYPE = 'varchar';
      
SET @SQLTRIM = 'UPDATE ['+@vital_final+'] '+CHAR(10)+'SET '+@SQLTRIM;
PRINT @SQLTRIM;
 
SELECT @SQLNULL = COALESCE(@SQLNULL+',[', '[')+COLUMN_NAME+']=NULLIF(['+COLUMN_NAME+'],'''')'+CHAR(10)
FROM INFORMATION_SCHEMA.COLUMNS
WHERE TABLE_NAME = @vital_final
      AND DATA_TYPE = 'varchar';
SET @SQLNULL = 'UPDATE ['+@vital_final+'] '+CHAR(10)+'SET '+@SQLNULL;
PRINT @SQLNULL;
 
SELECT @SQLTAB = COALESCE(@SQLTAB+',[', '[')+COLUMN_NAME+']=REPLACE(['+COLUMN_NAME+'],CHAR(9),'''')'+CHAR(10)
FROM INFORMATION_SCHEMA.COLUMNS
WHERE TABLE_NAME = @vital_final
      AND DATA_TYPE = 'varchar';
SET @SQLTAB = 'UPDATE ['+@vital_final+'] '+CHAR(10)+'SET '+@SQLTAB;
PRINT @SQLTAB;