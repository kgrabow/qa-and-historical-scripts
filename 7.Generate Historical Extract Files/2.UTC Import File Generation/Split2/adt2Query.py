import pypyodbc
import csv

connection = pypyodbc.connect('Driver={Sql Server};'
                              'Server=localhost;'
                              'Database=KevinInput;'
                             'dsn=PHLTH-PHI-DB;'
                              'uid=kgrabow;'

                                'Trusted_Connection=True'
                              )


cursor = connection.cursor()
query_string = "SELECT * FROM adt_utc_adjusted \
where right(str(visitnum),1) in ('0','2','4','6','8') \
order by mrn,msgDt"
cursor.execute(query_string)
with open('adt0.txt', 'w', newline='') as tsv:
    tsv.write("fac\tmrn\tvisitNum\tacctNum\tein\tfirstName\tlastName\tbirthDt\tsex\tmaritalSt\tzipCd\tprovider\tpatClass\tadmitDt\tdischDt\tdischDisp\tunit\troom\tbed\tdiagnosis\tdiagCode\tdiagType\tmsgDt\r\n")
    csv.writer(tsv, delimiter='\t').writerows(cursor)


cursor = connection.cursor()
query_string = "SELECT * FROM adt_utc_adjusted \
where right(str(visitnum),1) in ('1','3','5','7','9') \
order by mrn,msgDt"
cursor.execute(query_string)
with open('adt1.txt', 'w', newline='') as tsv:
    tsv.write("fac\tmrn\tvisitNum\tacctNum\tein\tfirstName\tlastName\tbirthDt\tsex\tmaritalSt\tzipCd\tprovider\tpatClass\tadmitDt\tdischDt\tdischDisp\tunit\troom\tbed\tdiagnosis\tdiagCode\tdiagType\tmsgDt\r\n")
    csv.writer(tsv, delimiter='\t').writerows(cursor)
connection.close()

