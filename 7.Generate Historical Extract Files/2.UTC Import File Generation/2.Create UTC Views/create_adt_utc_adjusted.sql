


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[adt_UTC_adjusted] as
SELECT [fac]
      ,[mrn]
      ,[visitNum]
      ,[acctNum]
      ,[ein]
      ,[firstName]
      ,[lastName]
      ,[birthDt]
      ,[sex]
      ,[maritalSt]
      ,[zipCd]
      ,[provider]
      ,[patClass]
      ,[UTC_admitDt] as admitDt
      ,[UTC_dischDt] as dischDt
      ,[dischDisp]
      ,[unit]
      ,[room]
      ,[bed]
      ,[diagnosis]
      ,[diagCode]
      ,[diagType]
      ,[UTC_msgDt] as msgDt
  FROM [PeraHealthInput].[dbo].[adt_final]
GO


