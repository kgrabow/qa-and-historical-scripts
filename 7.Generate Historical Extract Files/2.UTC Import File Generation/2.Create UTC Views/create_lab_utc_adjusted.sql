
/****** Object:  View [dbo].[lab_historical_gap]    Script Date: 3/16/2017 3:23:09 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create view  [dbo].[lab_utc_adjusted] as
SELECT [fac]
      ,[mrn]
      ,[VisitNum]
      ,[msgVar]
      ,[msgVal]
      ,[msgOrigVal]
      ,[UTC_recordedDt] as recordedDt
      ,[UTC_msgDt] as msgDt

  FROM [PeraHealthInput].[dbo].[lab_final]
GO