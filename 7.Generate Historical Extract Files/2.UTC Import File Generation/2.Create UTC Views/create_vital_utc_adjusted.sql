

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create view  [dbo].[vital_utc_adjusted] as
SELECT [fac]
      ,[mrn]
      ,[VisitNum]
      ,[msgVar]
      ,[msgVal]
      ,[msgOrigVal]
      ,[UTC_recordedDt] as recordedDt
      ,[UTC_msgDt] as msgDt

  FROM [PeraHealthInput].[dbo].[vital_final]
GO