/*
Before using this script, make sure you have imported the FacilityTimezone.txt file with timezone mapping
into the perahealthdata database.

You need to also add the names of the 4 historical import tables below
*/


DECLARE @SQL nvarchar(3000);
DECLARE @adtTable nvarchar(100);
DECLARE @labTable nvarchar(100);
DECLARE @nurseTable nvarchar(100);
DECLARE @vitalTable nvarchar(100);


-- Set the table names of the 4 tables containing historical import information
SET @adtTable = 'adt_gap_final';
SET @labTable = 'lab_gap_final';
SET @nurseTable = 'nurse_gap_final';
SET @vitalTable = 'vital_gap_final';

-- Drop existing UTC columns if they exist
SET @SQL = '
IF COL_LENGTH('''+@adtTable+''',''utc_admitdt'') IS NOT NULL
BEGIN
alter table '+@adtTable+' drop column utc_admitdt;
END

IF COL_LENGTH('''+@adtTable+''',''utc_dischdt'') IS NOT NULL
BEGIN
alter table '+@adtTable+' drop column utc_dischdt;
END

IF COL_LENGTH('''+@adtTable+''',''utc_msgdt'') IS NOT NULL
BEGIN
alter table '+@adtTable+' drop column utc_msgdt;
END

IF COL_LENGTH('''+@labTable+''',''utc_recordeddt'') IS NOT NULL
BEGIN
alter table '+@labTable+' drop column utc_recordeddt;
END

IF COL_LENGTH('''+@labTable+''',''utc_msgdt'') IS NOT NULL
BEGIN
alter table '+@labTable+' drop column utc_msgdt;
END

IF COL_LENGTH('''+@nurseTable+''',''utc_recordeddt'') IS NOT NULL
BEGIN
alter table '+@nurseTable+' drop column utc_recordeddt;
END

IF COL_LENGTH('''+@nurseTable+''',''utc_msgdt'') IS NOT NULL
BEGIN
alter table '+@nurseTable+' drop column utc_msgdt;
END

IF COL_LENGTH('''+@vitalTable+''',''utc_recordeddt'') IS NOT NULL
BEGIN
alter table '+@vitalTable+' drop column utc_recordeddt;
END

IF COL_LENGTH('''+@vitalTable+''',''utc_msgdt'') IS NOT NULL
BEGIN
alter table '+@vitalTable+' drop column utc_msgdt;
END

'
              PRINT 'Checking for UTC Columns and Dropping if the Exist '
              EXEC sp_executesql @SQL

/*
This adds the UTC columns.
This tricks SQL into allowing both DDL and DML in the same script, while sharing variables
*/
IF COL_LENGTH('Auburn','Tigers') IS NULL
BEGIN
PRINT 'UTC Columns dont exist'

-- ADT
SET @SQL = '
ALTER TABLE '+@adtTable+' ADD UTC_admitDt DATETIME;
ALTER TABLE '+@adtTable+' ADD UTC_dischDt DATETIME;
ALTER TABLE '+@adtTable+' ADD UTC_msgDt DATETIME;
'
              PRINT 'Adding UTC columns to: ' + @adtTable
              EXEC sp_executesql @SQL
-- Lab
SET @SQL = '
ALTER TABLE '+@labTable+' ADD UTC_recordedDt DATETIME;
ALTER TABLE '+@labTable+' ADD UTC_msgDt DATETIME;
'
              PRINT 'Adding UTC columns to: ' + @labTable
              EXEC sp_executesql @SQL
-- Nurse
SET @SQL = '
ALTER TABLE '+@nurseTable+' ADD UTC_recordedDt DATETIME;
ALTER TABLE '+@nurseTable+' ADD UTC_msgDt DATETIME;
'
              PRINT 'Adding UTC columns to: ' + @nurseTable
              EXEC sp_executesql @SQL
-- Vital
SET @SQL = '
ALTER TABLE '+@vitalTable+' ADD UTC_recordedDt DATETIME;
ALTER TABLE '+@vitalTable+' ADD UTC_msgDt DATETIME;
'
              PRINT 'Adding UTC columns to: ' + @vitalTable
              EXEC sp_executesql @SQL


END
ELSE 
BEGIN
PRINT 'UTC Columns exist'
END

/*
ADT
Convert Admit Date to UTC
*/

SET @SQL = '
UPDATE T
  SET
      UTC_admitDt = DATEADD(hh,
                            CASE
                                WHEN(T.admitdt >= TZ.et_dst_start
                                     AND T.admitdt < TZ.et_dst_end)
                                THEN TZ.dst_diff_hours
                                ELSE TZ.st_diff_hours
                            END, T.admitDt)
FROM '+@adtTable+' T
     INNER JOIN TZCalendar TZ 
	ON(YEAR(T.admitDt) = TZ.[year])
     INNER JOIN FacilityTimezone FT ON FT.tzname = TZ.time_zone_name
     INNER JOIN facility F ON F.id = FT.id
                                              AND T.fac = F.descr;
									 '
              PRINT 'Adding UTC admitDt for: ' + @adtTable
              EXEC sp_executesql @SQL
/*
ADT
Convert Discharge Date to UTC
*/
SET @SQL = '
UPDATE T
  SET
      UTC_dischDt = DATEADD(hh,
                            CASE
                                WHEN(T.dischdt >= TZ.et_dst_start
                                     AND T.dischdt < TZ.et_dst_end)
                                THEN TZ.dst_diff_hours
                                ELSE TZ.st_diff_hours
                            END, T.dischDt)
FROM '+@adtTable+' T
     INNER JOIN TZCalendar TZ ON(YEAR(T.dischDt) = TZ.[year])
     INNER JOIN FacilityTimezone FT 
	ON FT.tzname = TZ.time_zone_name
     INNER JOIN facility F 
	ON F.id = FT.id
                                            AND T.fac = F.descr;
								    '
              PRINT 'Adding UTC dischDt for: ' + @labTable
              EXEC sp_executesql @SQL

/*
ADT
Convert msgDate to UTC
*/
SET @SQL = '
UPDATE T
  SET
      UTC_msgDt = DATEADD(hh,
                            CASE
                                WHEN(T.msgdt >= TZ.et_dst_start
                                     AND T.msgdt < TZ.et_dst_end)
                                THEN TZ.dst_diff_hours
                                ELSE TZ.st_diff_hours
                            END, T.msgDt)
FROM '+@adtTable+' T
     INNER JOIN TZCalendar TZ ON(YEAR(T.msgDt) = TZ.[year])
     INNER JOIN FacilityTimezone FT 
	ON FT.tzname = TZ.time_zone_name
     INNER JOIN facility F 
	ON F.id = FT.id
                                            AND T.fac = F.descr;
								    '
              PRINT 'Adding UTC dischDt for: ' + @adtTable
              EXEC sp_executesql @SQL;

/*
Lab
Convert recordedDt to UTC
*/
SET @SQL = '
UPDATE T
  SET
      UTC_recordedDt = DATEADD(hh,
                            CASE
                                WHEN(T.recordeddt >= TZ.et_dst_start
                                     AND T.recordeddt < TZ.et_dst_end)
                                THEN TZ.dst_diff_hours
                                ELSE TZ.st_diff_hours
                            END, T.recordedDt)
FROM '+@labTable+' T
     INNER JOIN TZCalendar TZ ON(YEAR(T.recordedDt) = TZ.[year])
     INNER JOIN FacilityTimezone FT 
	ON FT.tzname = TZ.time_zone_name
     INNER JOIN facility F 
	ON F.id = FT.id
                                            AND T.fac = F.descr;
								    '
              PRINT 'Adding UTC recordedDt for: ' + @labTable
              EXEC sp_executesql @SQL

/*
Lab
Convert msgDate to UTC
*/
SET @SQL = '
UPDATE T
  SET
      UTC_msgDt = DATEADD(hh,
                            CASE
                                WHEN(T.msgdt >= TZ.et_dst_start
                                     AND T.msgdt < TZ.et_dst_end)
                                THEN TZ.dst_diff_hours
                                ELSE TZ.st_diff_hours
                            END, T.msgDt)
FROM '+@labTable+' T
     INNER JOIN TZCalendar TZ ON(YEAR(T.msgDt) = TZ.[year])
     INNER JOIN FacilityTimezone FT 
	ON FT.tzname = TZ.time_zone_name
     INNER JOIN facility F 
	ON F.id = FT.id
                                            AND T.fac = F.descr;
								    '
              PRINT 'Adding UTC dischDt for: ' + @labTable
              EXEC sp_executesql @SQL;
/*
nurse
Convert recordedDt to UTC
*/
SET @SQL = '
UPDATE T
  SET
      UTC_recordedDt = DATEADD(hh,
                            CASE
                                WHEN(T.recordeddt >= TZ.et_dst_start
                                     AND T.recordeddt < TZ.et_dst_end)
                                THEN TZ.dst_diff_hours
                                ELSE TZ.st_diff_hours
                            END, T.recordedDt)
FROM '+@nurseTable+' T
     INNER JOIN TZCalendar TZ ON(YEAR(T.recordedDt) = TZ.[year])
     INNER JOIN FacilityTimezone FT 
	ON FT.tzname = TZ.time_zone_name
     INNER JOIN facility F 
	ON F.id = FT.id
                                            AND T.fac = F.descr;
								    '
              PRINT 'Adding UTC recordedDt for: ' + @nurseTable
              EXEC sp_executesql @SQL

/*
nurse
Convert msgDate to UTC
*/
SET @SQL = '
UPDATE T
  SET
      UTC_msgDt = DATEADD(hh,
                            CASE
                                WHEN(T.msgdt >= TZ.et_dst_start
                                     AND T.msgdt < TZ.et_dst_end)
                                THEN TZ.dst_diff_hours
                                ELSE TZ.st_diff_hours
                            END, T.msgDt)
FROM '+@nurseTable+' T
     INNER JOIN TZCalendar TZ ON(YEAR(T.msgDt) = TZ.[year])
     INNER JOIN FacilityTimezone FT 
	ON FT.tzname = TZ.time_zone_name
     INNER JOIN facility F 
	ON F.id = FT.id
                                            AND T.fac = F.descr;
								    '
              PRINT 'Adding UTC dischDt for: ' + @nurseTable
              EXEC sp_executesql @SQL;

/*
Vital
Convert recordedDt to UTC
*/
SET @SQL = '
UPDATE T
  SET
      UTC_recordedDt = DATEADD(hh,
                            CASE
                                WHEN(T.recordeddt >= TZ.et_dst_start
                                     AND T.recordeddt < TZ.et_dst_end)
                                THEN TZ.dst_diff_hours
                                ELSE TZ.st_diff_hours
                            END, T.recordedDt)
FROM '+@vitalTable+' T
     INNER JOIN TZCalendar TZ ON(YEAR(T.recordedDt) = TZ.[year])
     INNER JOIN FacilityTimezone FT 
	ON FT.tzname = TZ.time_zone_name
     INNER JOIN facility F 
	ON F.id = FT.id
                                            AND T.fac = F.descr;
								    '
              PRINT 'Adding UTC recordedDt for: ' + @vitalTable
              EXEC sp_executesql @SQL

/*
Vital
Convert msgDate to UTC
*/
SET @SQL = '
UPDATE T
  SET
      UTC_msgDt = DATEADD(hh,
                            CASE
                                WHEN(T.msgdt >= TZ.et_dst_start
                                     AND T.msgdt < TZ.et_dst_end)
                                THEN TZ.dst_diff_hours
                                ELSE TZ.st_diff_hours
                            END, T.msgDt)
FROM '+@vitalTable+' T
     INNER JOIN TZCalendar TZ ON(YEAR(T.msgDt) = TZ.[year])
     INNER JOIN FacilityTimezone FT 
	ON FT.tzname = TZ.time_zone_name
     INNER JOIN facility F 
	ON F.id = FT.id
                                            AND T.fac = F.descr;
								    '
              PRINT 'Adding UTC dischDt for: ' + @vitalTable
              EXEC sp_executesql @SQL;





GO
/*
These queries validate that the UTC columns exist and that they've been populated.
You should see 3 UTC columns for ADT and 2 UTC columns for lab, nurse, and vital
*/

SELECT top 5 fac,
       admitdt,
       UTC_admitDt,
       DATEDIFF(hour, utc_admitdt, admitdt) as 'admit diff',
	  dischDt,
       UTC_dischDt,
	  DATEDIFF(hour, utc_dischdt, dischdt) as 'disch diff',
       msgDt,
       UTC_msgDt,
	  DATEDIFF(hour, UTC_msgDt, msgDt) as 'msgDt diff'
FROM adt_gap_final

SELECT top 5 fac,
       recordedDt,
       UTC_recordedDt,
       DATEDIFF(hour, utc_recordeddt, recordedDt) as 'admit diff',
       msgDt,
       UTC_msgDt,
	  DATEDIFF(hour, UTC_msgDt, msgDt) as 'msgDt diff'
FROM lab_gap_final

SELECT top 5 fac,
       recordedDt,
       UTC_recordedDt,
       DATEDIFF(hour, utc_recordeddt, recordedDt) as 'admit diff',
       msgDt,
       UTC_msgDt,
	  DATEDIFF(hour, UTC_msgDt, msgDt) as 'msgDt diff'
FROM nurse_gap_final

SELECT top 5 fac,
       recordedDt,
       UTC_recordedDt,
       DATEDIFF(hour, utc_recordeddt, recordedDt) as 'admit diff',
       msgDt,
       UTC_msgDt,
	  DATEDIFF(hour, UTC_msgDt, msgDt) as 'msgDt diff'
FROM vital_gap_final



/*
-- For debugging purposes. Change the table names below if you want to drop the UTC columns.

alter table adt_gap_final drop column utc_admitdt;
alter table adt_gap_final drop column utc_dischdt;
alter table adt_gap_final drop column utc_msgdt;


alter table lab_gap_final drop column utc_recordeddt;
alter table lab_gap_final drop column utc_msgdt;

alter table nurse_gap_final drop column utc_recordeddt;
alter table nurse_gap_final drop column utc_msgdt;

alter table vital_gap_final drop column utc_recordeddt;
alter table vital_gap_final drop column utc_msgdt;



*/