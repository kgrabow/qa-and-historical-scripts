import pypyodbc
import csv




connection = pypyodbc.connect('Driver={Sql Server};'
                              'Server=localhost;'
                              'Database=KevinInput;'
                             'dsn=PHLTH-PHI-DB;'
                              'uid=kgrabow;'

                                'Trusted_Connection=True'
                              )

cursor = connection.cursor()

query_string = "SELECT * FROM __lab_final order by mrn,msgDt \
where visitnum in (select distinct visitnum from __adt_final) \
order by mrn,msgDt"

cursor.execute(query_string)
with open('lab.txt', 'w', newline='') as tsv:
    tsv.write("fac\tmrn\tVisitNum\tmsgVar\tmsgVal\tmsgOrigVal\trecordedDt\tmsgDt\r\n")
    csv.writer(tsv, delimiter='\t').writerows(cursor)
connection.close()
