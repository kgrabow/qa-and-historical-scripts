import pypyodbc
import csv




connection = pypyodbc.connect('Driver={Sql Server};'
                              'Server=localhost;'
                              'Database=KevinInput;'
                             'dsn=PHLTH-PHI-DB;'
                              'uid=kgrabow;'

                                'Trusted_Connection=True'
                              )

cursor = connection.cursor()

query_string = "SELECT * FROM vital_final \
where right(str(visitnum),1) in ('0','2','4','6','8') \
and visitnum in (select distinct visitnum from adt_final) \
order by mrn,msgDt"
cursor.execute(query_string)
with open('vital0.txt', 'w', newline='') as tsv:
    tsv.write("fac\tmrn\tVisitNum\tmsgVar\tmsgVal\tmsgOrigVal\trecordedDt\tmsgDt\r\n")
    csv.writer(tsv, delimiter='\t').writerows(cursor)

query_string = "SELECT * FROM vital_final \
where right(str(visitnum),1) in ('1','3','5','7','9') \
and visitnum in (select distinct visitnum from adt_final) \
order by mrn,msgDt"
cursor.execute(query_string)
with open('vital1.txt', 'w', newline='') as tsv:
    tsv.write("fac\tmrn\tVisitNum\tmsgVar\tmsgVal\tmsgOrigVal\trecordedDt\tmsgDt\r\n")
    csv.writer(tsv, delimiter='\t').writerows(cursor)
    
connection.close()
