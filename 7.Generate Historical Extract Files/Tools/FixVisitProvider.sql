with noprimary as (
select visit_id, sum(case when isprimary=1 then 1 else 0 end) as cnt
from visitprovider
group by visit_id
having sum(case when isprimary=1 then 1 else 0 end) = 0
),
part as (
select ROW_NUMBER() over (partition by visit_id order by id asc) rn, id, visit_id, isprimary
from visitprovider
where visit_id in
(select visit_id from noprimary)
)
update part
set isprimary = 1
where rn = 1