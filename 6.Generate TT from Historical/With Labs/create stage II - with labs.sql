-- This assumes the final StageI TT is imported into the StageI table.

USE [perahealthinput];
GO
DECLARE @TT_StageII TABLE
(ph_factor VARCHAR(MAX),
 hosp_var  VARCHAR(MAX),
 hosp_val  VARCHAR(MAX),
 ph_val    VARCHAR(MAX),
 ignore    VARCHAR(MAX)
);
INSERT INTO @TT_StageII
       SELECT tt.ph_factor,
              tt.hosp_var AS hosp_var,
              qa.msgval AS hosp_val,
              tt.ph_val,
              tt.ignore
       FROM StageI tt
            JOIN nurse_historical_extract_qa qa ON qa.msgvar = tt.hosp_var;
INSERT INTO @TT_StageII
       SELECT tt.ph_factor,
              tt.hosp_var AS hosp_var,
              mn.msg_val AS hosp_val,
              tt.ph_val,
              tt.ignore
       FROM StageI tt
            JOIN msgnurse mn ON mn.msg_var = tt.hosp_var;
INSERT INTO @TT_StageII
       SELECT tt.ph_factor,
              tt.hosp_var AS hosp_var,
              qa.msgval AS hosp_val,
              tt.ph_val,
              tt.ignore
       FROM StageI tt
            JOIN lab_historical_extract_qa qa ON qa.msgvar = tt.hosp_var;
INSERT INTO @TT_StageII
       SELECT tt.ph_factor,
              tt.hosp_var AS hosp_var,
              mn.msg_val AS hosp_val,
              tt.ph_val,
              tt.ignore
       FROM StageI tt
            JOIN msglab mn ON mn.msg_var = tt.hosp_var;

SELECT DISTINCT
       ph_factor,
       hosp_var,
       hosp_val,
       ph_val,
       ignore,
	  count(hosp_val)
FROM @TT_StageII
WHERE ignore IN('NO', 'YES or NO')
GROUP by hosp_val,ph_factor,hosp_var,ph_val,ignore
ORDER BY ph_factor,
         hosp_var,
         hosp_val ASC;