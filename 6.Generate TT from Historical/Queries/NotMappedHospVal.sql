select msg_var, msg_val, count(msg_val)
from msgnurse
where msg_var in
(select hosp_var from hospxlate)
and msg_val not in
(select hosp_val from hospxlate)
group by msg_var, msg_val
order by count(msg_val) desc

select timebefore from ptrulestmt
where op like '<'