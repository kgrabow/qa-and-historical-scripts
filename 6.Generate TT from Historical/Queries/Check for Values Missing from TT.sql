-- This assumes the final StageI TT is imported into the StageI table.

USE [Kevininput];
GO
DECLARE @nurse_data TABLE
(msg_var VARCHAR(MAX),
 msg_val VARCHAR(MAX)
);
DECLARE @lab_data TABLE
(msg_var VARCHAR(MAX),
 msg_val VARCHAR(MAX)
);
DECLARE @vital_data TABLE
(msg_var VARCHAR(MAX),
 msg_val VARCHAR(MAX)
);
INSERT INTO @nurse_data
       SELECT DISTINCT
              msgvar,
              msgval
       FROM [dbo].[nurse_historical_extract_qa] WITH (nolock);
INSERT INTO @lab_data
       SELECT DISTINCT
              msgvar,
              msgval
       FROM [dbo].[lab_historical_extract_qa] WITH (nolock);
INSERT INTO @vital_data
       SELECT DISTINCT
              msgvar,
              msgval
       FROM [dbo].[vital_historical_extract_qa] WITH (nolock);
SELECT 'Historical Lab Data not mapped in TT';
SELECT msg_var,
       msg_val
FROM @lab_data
WHERE msg_var NOT IN
(
    SELECT hosp_var
    FROM hospxlate WITH (nolock)
)
ORDER BY msg_var,
         msg_val ASC;
SELECT 'Historical Nurse Data not mapped in TT';
SELECT msg_var,
       msg_val
FROM @nurse_data
WHERE msg_var NOT IN
(
    SELECT hosp_var
    FROM hospxlate WITH (nolock)
)
ORDER BY msg_var,
         msg_val ASC;
SELECT 'Historical Vital Data not mapped in TT';
SELECT msg_var,
       msg_val
FROM @vital_data
WHERE msg_var NOT IN
(
    SELECT hosp_var
    FROM hospxlate WITH (nolock)
)
ORDER BY msg_var,
         msg_val ASC;