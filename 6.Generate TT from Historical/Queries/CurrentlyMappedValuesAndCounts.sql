select hx.ph_factor, hx.hosp_var,hx.ph_val, mn.msg_val, count(mn.msg_val)
from msgnurse mn
join hospxlate hx
on hx.hosp_var = mn.msg_var
and hx.hosp_val = mn.msg_val
group by hx.ph_factor, hx.hosp_var, hx.ph_val, mn.msg_val
order by ph_factor, count(msg_val) desc