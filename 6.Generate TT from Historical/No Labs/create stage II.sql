
CREATE TABLE #TT_StageII
(ph_factor VARCHAR(MAX),
 hosp_var  VARCHAR(MAX),
 hosp_val  VARCHAR(MAX),
 ph_val    VARCHAR(MAX),
 ignore    VARCHAR(MAX)
);
INSERT INTO #TT_StageII
       SELECT DISTINCT tt.ph_factor,
              tt.hosp_var AS hosp_var,
              qa.msgval AS hosp_val,
              'Please fill in 0,1 or NULL' as ph_val,
              tt.ignore
       FROM StageI tt
            JOIN nurse_historical_extract_qa qa 
			ON qa.msgvar = tt.hosp_var;
GO

SELECT DISTINCT
       ph_factor,
       hosp_var,
       hosp_val,
       ph_val,
       ignore,
	  COUNT(hosp_val) as count
INTO stageII
FROM #TT_StageII
WHERE ignore IN('NO', 'YES or NO')
GROUP by hosp_val,ph_factor,hosp_var,ph_val,ignore
ORDER BY ph_factor,
         hosp_var,
         hosp_val ASC

DROP TABLE #TT_StageII