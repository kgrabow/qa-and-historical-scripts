DECLARE @TT_StageI TABLE
(ph_factor VARCHAR(max),
 hosp_var  VARCHAR(max),
 ph_val    VARCHAR(max),
 ignore    VARCHAR(max)
);

INSERT INTO @TT_StageI
     SELECT 'Please fill in PeraHealth Category' AS 'ph_factor',
            msgvar AS 'hosp_var',
            'Please fill in 0,1 or NULL' AS 'ph_val',
            'YES or NO' AS 'ignore'
     FROM nurse_historical_extract_qa

INSERT INTO @TT_StageI
     SELECT 'Please fill in PeraHealth Category' AS 'ph_factor',
            msg_var AS 'hosp_var',
            'Please fill in 0,1 or NULL' AS 'ph_val',
            'YES or NO' AS 'ignore'
     FROM msgnurse

SELECT DISTINCT ph_factor, hosp_var, ph_val, ignore, count(hosp_var) as count
--into StageI
from @TT_StageI
group by hosp_var, ph_factor, ph_val, ignore
order by ph_factor, hosp_var, ph_val, ignore
