USE [perahealthinput];
GO

/****** Object:  View [dbo].[lab_historical_extract_qa]    Script Date: 11/17/2016 3:06:55 PM ******/

SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO

-- (1) Replace the table in the FROM statement with the table of the imported historical lab File
-- (2) Replace the column/header names to the left of the AS statement in each to the column/header name in the imported historical file.

CREATE VIEW [dbo].[lab_historical_extract_qa]
AS
     SELECT l.fac AS fac,
            l.mrn AS mrn,
            l.visitnum AS VisitNum,
            l.msgvar AS msgVar,
            l.msgval AS msgVal,
            l.msgval AS msgOrigVal,
            DATEADD(mi, DATEDIFF(mi, 0, DATEADD(s, 30, l.recordedDt)), 0) AS recordedDt,
            DATEADD(mi, DATEDIFF(mi, 0, DATEADD(s, 30, l.msgDt)), 0) AS msgDt
     FROM [lab1000] l
     WHERE ISDATE(l.recordeddt) = 1
           AND ISDATE(l.msgdt) = 1;
