USE [perahealthinput];
GO

/****** Object:  View [dbo].[vital_historical_extract_qa]    Script Date: 11/17/2016 3:08:01 PM ******/

SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
CREATE VIEW [dbo].[vital_historical_extract_qa]
AS
     SELECT v.fac AS fac,
            v.mrn AS mrn,
            v.visitnum AS VisitNum,
            v.msgvar AS msgVar,
            v.msgval AS msgVal,
            v.msgval AS msgOrigVal,
            DATEADD(mi, DATEDIFF(mi, 0, DATEADD(s, 30, v.recordedDt)), 0) AS recordedDt,
            DATEADD(mi, DATEDIFF(mi, 0, DATEADD(s, 30, v.msgDt)), 0) AS msgDt
     FROM [dbo].[vital1000] AS v
     WHERE ISDATE(v.recordeddt) = 1
           AND ISDATE(v.msgdt) = 1;
GO