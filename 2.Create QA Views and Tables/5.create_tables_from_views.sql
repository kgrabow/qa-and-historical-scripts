IF EXISTS (SELECT * FROM sys.tables WHERE name = '__adt_final')
 DROP TABLE __adt_final

IF EXISTS (SELECT * FROM sys.tables WHERE name = '__lab_final')
 DROP TABLE __lab_final

IF EXISTS (SELECT * FROM sys.tables WHERE name = '__nurse_final')
 DROP TABLE __nurse_final

IF EXISTS (SELECT * FROM sys.tables WHERE name = '__vital_final')
 DROP TABLE __vital_final

select * 
into __adt_final
from adt_historical_extract_qa

select * 
into __lab_final
from lab_historical_extract_qa

select * 
into __nurse_final
from nurse_historical_extract_qa

select * 
into __vital_final
from vital_historical_extract_qa