-- (1) Replace the table in the FROM statement with the table of the imported historical ADT File
-- (2) Replace the column/header names to the left of the AS statement in each to the column/header name in the imported historical file.

CREATE VIEW adt_historical_extract_qa
AS
     SELECT demo.fac AS fac,
            demo.mrn AS mrn,
            demo.visitNumber AS visitNum,
            demo.AccountNumber AS acctNum,
            demo.ein AS ein,
            demo.firstName AS firstName,
            demo.lastName AS lastName,
            cast((demo.birthDt) as datetime) AS birthDt,
            demo.gender AS sex,
            demo.MaritalStatus AS maritalSt,
            demo.ZIP AS zipCd,
            demo.ProvName AS provider,
            'I' AS patClass,
            DATEADD(mi, DATEDIFF(mi, 0, DATEADD(s, 30, demo.admitDt)), 0) AS admitDt,
            DATEADD(mi, DATEDIFF(mi, 0, DATEADD(s, 30, demo.dischDt)), 0) AS dischDt,
            demo.dischDisp AS dischDisp,
            demo.Unit AS unit,
            demo.Room AS room,
            demo.Bed AS bed,
            demo.diagnosis AS diagnosis,
            demo.DiagnosisCode AS diagCode,
            demo.DiagnosisCodeType AS diagType,
            DATEADD(mi, DATEDIFF(mi, 0, DATEADD(s, 30, demo.msgDt)), 0) AS msgDt
     FROM demo demo WITH (nolock)
     WHERE ISDATE(demo.admitDt) = 1
           AND ISDATE(demo.dischDt) = 1
		 AND ISDATE(demo.msgDt) = 1;