DECLARE @sql VARCHAR(100);
DECLARE @lab_msgvar INT;
DECLARE @lab_msgval INT;
DECLARE @nurse_msgvar INT;
DECLARE @nurse_msgval INT;
DECLARE @vital_msgvar INT;
DECLARE @vital_msgval INT;
SET @lab_msgvar =
(
    SELECT MAX(LEN(msgvar))
    FROM _lab1000
);
SET @lab_msgval =
(
    SELECT MAX(LEN(msgval))
    FROM _lab1000
);
SET @nurse_msgvar =
(
    SELECT MAX(LEN(msgvar))
    FROM _nurse1000
);
SET @nurse_msgval =
(
    SELECT MAX(LEN(msgval))
    FROM _nurse1000
);
SET @vital_msgvar =
(
    SELECT MAX(LEN(msgvar))
    FROM _vital1000
);
SET @vital_msgval =
(
    SELECT MAX(LEN(msgval))
    FROM _vital1000
);
SET @sql = 'Alter Table _lab1000 Alter Column msgvar varchar('+CAST(@lab_msgvar AS VARCHAR(4))+')';
EXEC (@sql);
SET @sql = 'Alter Table _lab1000 Alter Column msgval varchar('+CAST(@lab_msgval AS VARCHAR(4))+')';
EXEC (@sql);
SET @sql = 'Alter Table _nurse1000 Alter Column msgvar varchar('+CAST(@nurse_msgvar AS VARCHAR(4))+')';
EXEC (@sql);
SET @sql = 'Alter Table _nurse1000 Alter Column msgval varchar('+CAST(@nurse_msgval AS VARCHAR(4))+')';
EXEC (@sql);
SET @sql = 'Alter Table _vital1000 Alter Column msgvar varchar('+CAST(@vital_msgvar AS VARCHAR(4))+')';
EXEC (@sql);
SET @sql = 'Alter Table _vital1000 Alter Column msgval varchar('+CAST(@vital_msgval AS VARCHAR(4))+')';
EXEC (@sql);

-- Drop Lab indexes
IF EXISTS(SELECT * FROM sys.indexes WHERE object_id = object_id('dbo._lab1000') AND NAME ='idx_lab_c')
    DROP INDEX idx_lab_c ON dbo._lab1000;
IF EXISTS(SELECT * FROM sys.indexes WHERE object_id = object_id('dbo._lab1000') AND NAME ='idx_lab_nc')
    DROP INDEX idx_lab_nc ON dbo._lab1000;

-- Create Lab Indexes
CREATE CLUSTERED INDEX [idx_lab_c] ON [dbo].[_lab1000]
(
	[mrn] ASC,
	[VisitNum] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

CREATE NONCLUSTERED INDEX [idx_lab_nc] ON [dbo].[_lab1000]
(
	[msgVar] ASC,
	[msgVal] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

-- Drop nurse indexes
IF EXISTS(SELECT * FROM sys.indexes WHERE object_id = object_id('dbo._nurse1000') AND NAME ='idx_nurse_c')
    DROP INDEX idx_nurse_c ON dbo._nurse1000;
IF EXISTS(SELECT * FROM sys.indexes WHERE object_id = object_id('dbo._nurse1000') AND NAME ='idx_nurse_nc')
    DROP INDEX idx_nurse_nc ON dbo._nurse1000;

-- Create nurse Indexes
CREATE CLUSTERED INDEX [idx_nurse_c] ON [dbo].[_nurse1000]
(
	[mrn] ASC,
	[VisitNum] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

CREATE NONCLUSTERED INDEX [idx_nurse_nc] ON [dbo].[_nurse1000]
(
	[msgVar] ASC,
	[msgVal] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO


-- Drop vital indexes
IF EXISTS(SELECT * FROM sys.indexes WHERE object_id = object_id('dbo._vital1000') AND NAME ='idx_vital_c')
    DROP INDEX idx_vital_c ON dbo._vital1000;
IF EXISTS(SELECT * FROM sys.indexes WHERE object_id = object_id('dbo._vital1000') AND NAME ='idx_vital_nc')
    DROP INDEX idx_vital_nc ON dbo._vital1000;

-- Create vital Indexes
CREATE CLUSTERED INDEX [idx_vital_c] ON [dbo].[_vital1000]
(
	[mrn] ASC,
	[VisitNum] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

CREATE NONCLUSTERED INDEX [idx_vital_nc] ON [dbo].[_vital1000]
(
	[msgVar] ASC,
	[msgVal] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO