-- Run these scripts one at a time
-- This script will remove < and > from the lab qa view
UPDATE lab_historical_extract_qa
  SET
      msgVal = REPLACE(msgval, '<', '');
UPDATE lab_historical_extract_qa
  SET
      msgVal = REPLACE(msgval, '>', '');





-- This script will delete any NULL rows from the lab qa view
DELETE FROM lab_historical_extract_qa
WHERE msgVal IS NULL;

-- This script will delete any blank values from the lab qa view
DELETE FROM lab_historical_extract_qa
WHERE msgVal = '';

-- These scripts will delete MOST text from the lab qa view
DELETE FROM lab_historical_extract_qa
WHERE msgVal LIKE '%e%';
DELETE FROM lab_historical_extract_qa
WHERE msgVal LIKE '%t%';
DELETE FROM lab_historical_extract_qa
WHERE msgVal LIKE '%a%';
DELETE FROM lab_historical_extract_qa
WHERE msgVal LIKE '%i%';
DELETE FROM lab_historical_extract_qa
WHERE msgVal LIKE '%o%';
DELETE FROM lab_historical_extract_qa
WHERE msgVal LIKE '%n%';
DELETE FROM lab_historical_extract_qa
WHERE msgVal LIKE '%s%';
DELETE FROM lab_historical_extract_qa
WHERE msgVal LIKE '%h%';
DELETE FROM lab_historical_extract_qa
WHERE msgVal LIKE '%r%';
DELETE FROM lab_historical_extract_qa
WHERE msgVal LIKE '%l%';
DELETE FROM lab_historical_extract_qa
WHERE msgVal LIKE '%d%';
DELETE FROM lab_historical_extract_qa
WHERE msgVal LIKE '%u%';





-- This script will remove < and > from the vital qa view
UPDATE vital_historical_extract_qa
  SET
      msgVal = REPLACE(msgval, '<', '');
UPDATE vital_historical_extract_qa
  SET
      msgVal = REPLACE(msgval, '>', '');


-- This script will delete any NULL rows from the lab qa view
DELETE FROM vital_historical_extract_qa
WHERE msgVal IS NULL;

-- This script will delete any blank values from the vital qa view
DELETE FROM vital_historical_extract_qa
WHERE msgVal = '';

-- These scripts will delete MOST text from the vital qa view
DELETE FROM vital_historical_extract_qa
WHERE msgVal LIKE '%e%';
DELETE FROM vital_historical_extract_qa
WHERE msgVal LIKE '%t%';
DELETE FROM vital_historical_extract_qa
WHERE msgVal LIKE '%a%';
DELETE FROM vital_historical_extract_qa
WHERE msgVal LIKE '%i%';
DELETE FROM vital_historical_extract_qa
WHERE msgVal LIKE '%o%';
DELETE FROM vital_historical_extract_qa
WHERE msgVal LIKE '%n%';
DELETE FROM vital_historical_extract_qa
WHERE msgVal LIKE '%s%';
DELETE FROM vital_historical_extract_qa
WHERE msgVal LIKE '%h%';
DELETE FROM vital_historical_extract_qa
WHERE msgVal LIKE '%r%';
DELETE FROM vital_historical_extract_qa
WHERE msgVal LIKE '%l%';
DELETE FROM vital_historical_extract_qa
WHERE msgVal LIKE '%d%';
DELETE FROM vital_historical_extract_qa
WHERE msgVal LIKE '%u%';


-- Use the below scripts to clean up any remaining text data in the msgVal columns
-- of the lab and vital qa views.

UPDATE lab_historical_extract_qa
  SET
      msgVal = REPLACE(msgval, 'mg', '');
UPDATE vital_historical_extract_qa
  SET
      msgVal = REPLACE(msgval, 'mg', '');