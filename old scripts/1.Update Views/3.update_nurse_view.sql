USE [perahealthinput]
GO

/****** Object:  View [dbo].[nurse_historical_extract_qa]    Script Date: 11/17/2016 3:07:30 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- (1) Replace the table in the FROM statement with the table of the imported historical nurse File
-- (2) Replace the column/header names to the left of the AS statement in each to the column/header name in the imported historical file.

CREATE VIEW [dbo].[nurse_historical_extract_qa]

AS
     SELECT		n.fac AS fac,
                    n.mrn AS mrn,
                    n.visitnum AS VisitNum,
                    n.msgvar AS msgVar,
                    n.msgval AS msgVal,
                    n.msgval AS msgOrigVal,
                    n.recordeddt AS recordedDt,
                    n.msgdt AS msgDt
     FROM [dbo].[_nurse1000] n
     WHERE n.mrn in
	(SELECT distinct a.mrn from adt_historical_extract_qa a)
	


GO


