USE [perahealthinput]
GO

/****** Object:  View [dbo].[vital_historical_extract_qa]    Script Date: 11/17/2016 3:08:01 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vital_historical_extract_qa]
AS
     SELECT		v.fac AS fac,
                    v.mrn AS mrn,
                    v.visitnum AS VisitNum,
                    v.msgvar AS msgVar,
                    v.msgval AS msgVal,
                    v.msgval AS msgOrigVal,
                    v.recordeddt AS recordedDt,
                    v.msgdt AS msgDt
     FROM  [dbo].[_vital1000] v
	WHERE v.visitnum in
	(SELECT DISTINCT a.visitnum from adt_historical_extract_qa a)
GO


