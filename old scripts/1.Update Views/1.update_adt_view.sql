-- (1) Replace the table in the FROM statement with the table of the imported historical ADT File
-- (2) Replace the column/header names to the left of the AS statement in each to the column/header name in the imported historical file.

CREATE VIEW adt_historical_extract_qa
as
SELECT fac AS fac,
       mrn AS mrn,
       visitNum AS visitNum,
       acctNum AS acctNum,
       ein AS ein,
       firstName AS firstName,
       lastName AS lastName,
       birthDt AS birthDt,
       sex AS sex,
       maritalSt AS maritalSt,
       zipCd AS zipCd,
       Provider AS provider,
       patClass AS patClass,
       admitDt AS admitDt,
       dischDt AS dischDt,
       dischDisp AS dischDisp,
       Unit AS unit,
       Room AS room,
       Bed AS bed,
       diagnosis AS diagnosis,
       diagCode AS diagCode,
       diagType AS diagType,
       msgDt AS msgDt
FROM [dbo].[_adt1000] with (nolock)
