DECLARE @sql VARCHAR(100);

DECLARE @lab_msgvar INT;
DECLARE @lab_msgval INT;
DECLARE @nurse_msgvar INT;
DECLARE @nurse_msgval INT;
DECLARE @vital_msgvar INT;
DECLARE @vital_msgval INT;

-- Drop adt indexes
IF EXISTS(SELECT * FROM sys.indexes WHERE object_id = object_id('dbo.adt_final') AND NAME ='idx_adt_c')
    DROP INDEX idx_adt_c ON dbo.adt_final;

-- Drop lab indexes
IF EXISTS(SELECT * FROM sys.indexes WHERE object_id = object_id('dbo.lab_final') AND NAME ='idx_lab_c')
    DROP INDEX idx_lab_c ON dbo.lab_final;
IF EXISTS(SELECT * FROM sys.indexes WHERE object_id = object_id('dbo.lab_final') AND NAME ='idx_lab_nc')
    DROP INDEX idx_lab_nc ON dbo.lab_final;

-- Drop nurse indexes
IF EXISTS(SELECT * FROM sys.indexes WHERE object_id = object_id('dbo.nurse_final') AND NAME ='idx_nurse_c')
    DROP INDEX idx_nurse_c ON dbo.nurse_final;
IF EXISTS(SELECT * FROM sys.indexes WHERE object_id = object_id('dbo.nurse_final') AND NAME ='idx_nurse_nc')
    DROP INDEX idx_nurse_nc ON dbo.nurse_final;

-- Drop vital indexes
IF EXISTS(SELECT * FROM sys.indexes WHERE object_id = object_id('dbo.vital_final') AND NAME ='idx_vital_c')
    DROP INDEX idx_vital_c ON dbo.vital_final;
IF EXISTS(SELECT * FROM sys.indexes WHERE object_id = object_id('dbo.vital_final') AND NAME ='idx_vital_nc')
    DROP INDEX idx_vital_nc ON dbo.vital_final;

-- Determine maximum length of data in msgVar and msgVal fields
SET @lab_msgvar =
(
    SELECT MAX(LEN(msgvar))
    FROM lab_final
);
SET @lab_msgval =
(
    SELECT MAX(LEN(msgval))
    FROM lab_final
);
SET @nurse_msgvar =
(
    SELECT MAX(LEN(msgvar))
    FROM nurse_final
);
SET @nurse_msgval =
(
    SELECT MAX(LEN(msgval))
    FROM nurse_final
);
SET @vital_msgvar =
(
    SELECT MAX(LEN(msgvar))
    FROM vital_final
);
SET @vital_msgval =
(
    SELECT MAX(LEN(msgval))
    FROM vital_final
);
SET @sql = 'Alter Table lab_final Alter Column msgvar varchar('+CAST(@lab_msgvar AS VARCHAR(4))+')';
EXEC (@sql);
SET @sql = 'Alter Table lab_final Alter Column msgval varchar('+CAST(@lab_msgval AS VARCHAR(4))+')';
EXEC (@sql);
SET @sql = 'Alter Table nurse_final Alter Column msgvar varchar('+CAST(@nurse_msgvar AS VARCHAR(4))+')';
EXEC (@sql);
SET @sql = 'Alter Table nurse_final Alter Column msgval varchar('+CAST(@nurse_msgval AS VARCHAR(4))+')';
EXEC (@sql);
SET @sql = 'Alter Table vital_final Alter Column msgvar varchar('+CAST(@vital_msgvar AS VARCHAR(4))+')';
EXEC (@sql);
SET @sql = 'Alter Table vital_final Alter Column msgval varchar('+CAST(@vital_msgval AS VARCHAR(4))+')';
EXEC (@sql);

-- Create adt index
CREATE CLUSTERED INDEX [idx_adt_c] ON [dbo].[adt_final]
(
	[mrn] ASC,
	[VisitNum] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

-- Create lab indexes
CREATE CLUSTERED INDEX [idx_lab_c] ON [dbo].[lab_final]
(
	[mrn] ASC,
	[VisitNum] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

CREATE NONCLUSTERED INDEX [idx_lab_nc] ON [dbo].[lab_final]
(
	[msgVar] ASC,
	[msgVal] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO


-- Create nurse indexes
CREATE CLUSTERED INDEX [idx_nurse_c] ON [dbo].[nurse_final]
(
	[mrn] ASC,
	[VisitNum] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

CREATE NONCLUSTERED INDEX [idx_nurse_nc] ON [dbo].[nurse_final]
(
	[msgVar] ASC,
	[msgVal] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO


-- Create vital indexes
CREATE CLUSTERED INDEX [idx_vital_c] ON [dbo].[vital_final]
(
	[mrn] ASC,
	[VisitNum] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

CREATE NONCLUSTERED INDEX [idx_vital_nc] ON [dbo].[vital_final]
(
	[msgVar] ASC,
	[msgVal] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO