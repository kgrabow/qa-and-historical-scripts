/****** Object:  View [dbo].[adt_random]    Script Date: 9/29/2016 4:24:29 PM ******/

IF OBJECT_ID('[dbo].[adt_random]', 'V') IS NOT NULL
    DROP VIEW [dbo].[adt_random];
GO
IF OBJECT_ID('[dbo].[lab_random]', 'V') IS NOT NULL
    DROP VIEW [dbo].[lab_random];
GO
IF OBJECT_ID('[dbo].[nurse_random]', 'V') IS NOT NULL
    DROP VIEW [dbo].[nurse_random];
GO
IF OBJECT_ID('[dbo].[vital_random]', 'V') IS NOT NULL
    DROP VIEW [dbo].[vital_random];
GO



/****** Object:  View [dbo].[adt_random]    Script Date: 9/29/2016 4:24:29 PM ******/

SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO

-- (1) Replace either the first and last name lines with the TEST lines if the dataset is needed for demo purposes. 

CREATE VIEW [dbo].[adt_random]
AS
     SELECT xFac.FacilityNameRandom AS fac,
            xM.MRNRandom AS mrn,
            xV.visitNumRandom AS visitNum,
            xV.visitNumRandom AS acctNum,
            xM.MRNRandom AS ein,
            -- TST AS firstName,
            xf.FirstNameRandom AS firstName,
            --'TEST' AS lastName,
            xL.LastNameRandom AS lastName,
            birthdt+
     (
         SELECT adj
         FROM _xref_maxDate
         WHERE param = 'Adjustment'
     ) AS birthDt,
            sex AS sex,
            maritalSt AS maritalSt,
            xZ.ZipRandom AS zipCd,
            xPr.providerNameRandom AS provider,
            patClass AS patClass,
            admitDt+
     (
         SELECT adj
         FROM _xref_maxDate
         WHERE param = 'Adjustment'
     ) AS admitDt,
            dischDt+
     (
         SELECT adj
         FROM _xref_maxDate
         WHERE param = 'Adjustment'
     ) AS dischDt,
            dischDisp AS dischDisp,
            xUn.UnitRandom AS unit,
            xRo.RoomRandom AS room,
            xBe.BedRandom AS bed,
            diagnosis AS diagnosis,
            diagCode AS diagCode,
            diagType AS diagType,
            msgDt+
     (
         SELECT adj
         FROM _xref_maxDate
         WHERE param = 'Adjustment'
     ) AS msgDt
     FROM [dbo].[adt_historical_extract_qa] adt WITH (nolock)
          JOIN [dbo].[_xref_facilityName_random] xFac ON xFac.FacilityName = adt.fac
          JOIN [dbo].[_xref_visitNum_random] xV ON xv.visitNum = adt.visitNum
          JOIN [dbo].[_xref_MRN_random] xM ON xM.MRN = adt.mrn
          JOIN [dbo].[_xref_firstName_random] xF ON xF.FirstName = adt.firstName
          JOIN [dbo].[_xref_lastName_random] xL ON xL.LastName = adt.lastName
          JOIN [dbo].[_xref_providerName_random] xPr ON xPr.providerName = adt.provider
          JOIN [dbo].[_xref_Unit_random] xUn ON xUn.unit = adt.unit
          JOIN [dbo].[_xref_Room_random] xRo ON xRo.room = adt.room
          JOIN [dbo].[_xref_Bed_random] xBe ON xBe.bed = adt.bed
		JOIN [dbo].[_xref_Zip_random] xZ ON xZ.Zip = adt.zipCd;
GO

/****** Object:  View [dbo].[lab_random]    Script Date: 9/29/2016 4:25:34 PM ******/

SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
CREATE VIEW [dbo].[lab_random]
AS
     SELECT xFac.facilityNameRandom AS fac,
            xM.mrnRandom AS mrn,
            xV.visitnumRandom AS visitNum,
            msgVar AS msgVar,
            msgVal AS msgVal,
            msgOrigVal AS msgOrigVal,
            recordedDt+
     (
         SELECT adj
         FROM _xref_maxDate
         WHERE param = 'Adjustment'
     ) AS recordedDt,
            msgDt+
     (
         SELECT adj
         FROM _xref_maxDate
         WHERE param = 'Adjustment'
     ) AS msgDt
     FROM [dbo].[lab_historical_extract_qa] lab
          JOIN [dbo].[_xref_visitNum_random] xV ON xv.visitNum = lab.VisitNum
          JOIN [dbo].[_xref_MRN_random] xM ON xM.MRN = lab.MRN
          JOIN [dbo].[_xref_facilityName_random] xFac ON xFac.FacilityName = lab.fac;
GO


/****** Object:  View [dbo].[nurse_random]    Script Date: 9/29/2016 4:27:24 PM ******/

SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
CREATE VIEW [dbo].[nurse_random]
AS
     SELECT xFac.facilityNameRandom AS fac,
            xM.mrnRandom AS mrn,
            xV.visitnumRandom AS visitNum,
            msgVar AS msgVar,
            msgVal AS msgVal,
            msgOrigVal AS msgOrigVal,
            recordedDt+
     (
         SELECT adj
         FROM _xref_maxDate
         WHERE param = 'Adjustment'
     ) AS recordedDt,
            msgDt+
     (
         SELECT adj
         FROM _xref_maxDate
         WHERE param = 'Adjustment'
     ) AS msgDt
     FROM [dbo].[nurse_historical_extract_qa] nurse
          JOIN [dbo].[_xref_visitNum_random] xV ON xv.visitNum = nurse.VisitNum
          JOIN [dbo].[_xref_MRN_random] xM ON xM.MRN = nurse.MRN
          JOIN [dbo].[_xref_facilityName_random] xFac ON xFac.FacilityName = nurse.fac;
GO


/****** Object:  View [dbo].[vital_random]    Script Date: 9/29/2016 4:33:00 PM ******/

SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
CREATE VIEW [dbo].[vital_random]
AS
     SELECT xFac.facilityNameRandom AS fac,
            xM.mrnRandom AS mrn,
            xV.visitnumRandom AS visitNum,
            msgVar AS msgVar,
            msgVal AS msgVal,
            msgOrigVal AS msgOrigVal,
            recordedDt+
     (
         SELECT adj
         FROM _xref_maxDate
         WHERE param = 'Adjustment'
     ) AS recordedDt,
            msgDt+
     (
         SELECT adj
         FROM _xref_maxDate
         WHERE param = 'Adjustment'
     ) AS msgDt
     FROM [dbo].[vital_historical_extract_qa] vital
          JOIN [dbo].[_xref_visitNum_random] xV ON xv.visitNum = vital.VisitNum
          JOIN [dbo].[_xref_MRN_random] xM ON xM.MRN = vital.MRN
          JOIN [dbo].[_xref_facilityName_random] xFac ON xFac.FacilityName = vital.fac;
GO