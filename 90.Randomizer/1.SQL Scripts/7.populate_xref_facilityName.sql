-- You many need to run this more than once to create unique facilities

DELETE FROM [dbo].[_xref_facilityName_random];

DECLARE @randomFacilityNameTemp TABLE(FacilityNameRandom VARCHAR(100));
INSERT INTO @randomFacilityNameTemp(FacilityNameRandom)
VALUES('Auburn'), ('Clemson'), ('Charlotte'), ('Roswell'), 
('Alpharetta'), ('Greenville'), ('Myrtle Beach'), ('Huntsville'),
('Central'), ('Anderson'), ('Tampa'), ('Ft Myers'),
('Wilton'), ('Philadephia'), ('Los Angeles'), ('Westchester'),
('Wayne'), ('Pequannock'), ('Kinnelon'), ('Yonkers');


DECLARE facilityNameCursor CURSOR
FOR SELECT DISTINCT
           fac
    FROM adt_historical_extract_qa;
DECLARE @facilityName VARCHAR(60);
OPEN facilityNameCursor;
FETCH NEXT FROM facilityNameCursor INTO @facilityName;
WHILE @@FETCH_STATUS = 0
    BEGIN
        INSERT INTO _xref_facilityName_random
        (facilityName,
         facilityNameRandom
        )
        VALUES
        ((@facilityName),
        (
            SELECT TOP 1 facilityNameRandom
            FROM @randomFacilityNameTemp
            ORDER BY NEWID()
        )
        );
        FETCH NEXT FROM facilityNameCursor INTO @facilityName;
    END;
CLOSE facilityNameCursor;
DEALLOCATE facilityNameCursor;

select * FROM [dbo].[_xref_facilityName_random];