delete from _xref_MRN_random;
delete from _xref_visitNum_random;
delete from _xref_Unit_random;
delete from _xref_Room_random;
delete from _xref_Bed_random;

-- Change the 1000 in the query below to how many patients you want to get
insert into [dbo].[_xref_MRN_random]
select top 1000  mrn
from [dbo].[adt_historical_extract_qa]
order by newid()
--select * from [dbo].[_xref_MRN_random]

insert into [dbo].[_xref_VisitNum_random]
select distinct VisitNum
from [dbo].[adt_historical_extract_qa]
where mrn in 
(select mrn from _xref_MRN_random)
--select * from [dbo].[_xref_VisitNum_random]

insert into [dbo].[_xref_Unit_random]
select distinct Unit
from [dbo].[adt_historical_extract_qa]
where mrn in 
(select mrn from _xref_MRN_random)
--select * from [dbo].[_xref_Unit_random]

insert into [dbo].[_xref_Room_random]
select distinct room
from [dbo].[adt_historical_extract_qa]
where mrn in 
(select mrn from _xref_MRN_random)

--select * from [dbo].[_xref_Room_random]

insert into [dbo].[_xref_Bed_random]
select distinct bed
from [dbo].[adt_historical_extract_qa]
where mrn in 
(select mrn from _xref_MRN_random)

--select * from [dbo].[_xref_Bed_random]

insert into [dbo].[_xref_Zip_random]
select distinct zipcd
from [dbo].[adt_historical_extract_qa]
where mrn in 
(select mrn from _xref_MRN_random)

--select * from [dbo].[_xref_Zip_random]