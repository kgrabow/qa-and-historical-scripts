IF OBJECT_ID('[dbo].[_xref_Bed_random]', 'U') IS NOT NULL
    DROP TABLE [dbo].[_xref_Bed_random];
GO
IF OBJECT_ID('[dbo].[_xref_facilityName_random]', 'U') IS NOT NULL
    DROP TABLE [dbo].[_xref_facilityName_random];
GO
IF OBJECT_ID('[dbo].[_xref_firstName_random]', 'U') IS NOT NULL
    DROP TABLE [dbo].[_xref_firstName_random];
GO
IF OBJECT_ID('[dbo].[_xref_lastName_random]', 'U') IS NOT NULL
    DROP TABLE [dbo].[_xref_lastName_random];
GO
IF OBJECT_ID('[dbo].[_xref_maxDate]', 'U') IS NOT NULL
    DROP TABLE [dbo].[_xref_maxDate];
GO
IF OBJECT_ID('[dbo].[_xref_MRN_random]', 'U') IS NOT NULL
    DROP TABLE [dbo].[_xref_MRN_random];
GO
IF OBJECT_ID('[dbo].[_xref_providerName_random]', 'U') IS NOT NULL
    DROP TABLE [dbo].[_xref_providerName_random];
GO
IF OBJECT_ID('[dbo].[_xref_Room_random]', 'U') IS NOT NULL
    DROP TABLE [dbo].[_xref_Room_random];
GO
IF OBJECT_ID('[dbo].[_xref_Unit_random]', 'U') IS NOT NULL
    DROP TABLE [dbo].[_xref_Unit_random];
GO
IF OBJECT_ID('[dbo].[_xref_visitNum_random]', 'U') IS NOT NULL
    DROP TABLE [dbo].[_xref_visitNum_random];
GO
IF OBJECT_ID('[dbo].[_xref_Zip_random]', 'U') IS NOT NULL
    DROP TABLE [dbo].[_xref_Zip_random];
GO


/****** Object:  Table [dbo].[_xref_Bed_random]    Script Date: 9/29/2016 9:12:52 PM ******/

SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
SET ANSI_PADDING ON;
GO
CREATE TABLE [dbo].[_xref_Bed_random]
([ID]  [INT] IDENTITY(1, 1)
             NOT NULL,
 [Bed] [VARCHAR](MAX) NULL,
 [BedRandom] AS ((100) + [ID]),
 PRIMARY KEY CLUSTERED([ID] ASC)
 WITH(PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
)
ON [PRIMARY] TEXTIMAGE_ON [PRIMARY];
GO
SET ANSI_PADDING OFF;
GO

/****** Object:  Table [dbo].[_xref_facilityName_random]    Script Date: 9/29/2016 9:14:09 PM ******/

SET QUOTED_IDENTIFIER ON;
GO
SET ANSI_PADDING ON;
GO
CREATE TABLE [dbo].[_xref_facilityName_random]
([ID]                 [INT] IDENTITY(1, 1)
                            NOT NULL,
 [facilityName]       [VARCHAR](90) NULL,
 [facilityNameRandom] [VARCHAR](90) NULL,
 PRIMARY KEY CLUSTERED([ID] ASC)
 WITH(PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
)
ON [PRIMARY];
GO
SET ANSI_PADDING OFF;
GO

/****** Object:  Table [dbo].[_xref_firstName_random]    Script Date: 9/29/2016 9:16:58 PM ******/

SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
SET ANSI_PADDING ON;
GO
CREATE TABLE [dbo].[_xref_firstName_random]
([ID]              [INT] IDENTITY(1, 1)
                         NOT NULL,
 [FirstName]       [VARCHAR](50) NULL,
 [FirstNameRandom] [VARCHAR](10) NULL,
 PRIMARY KEY CLUSTERED([ID] ASC)
 WITH(PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
)
ON [PRIMARY];
GO
SET ANSI_PADDING OFF;
GO

/****** Object:  Table [dbo].[_xref_lastName_random]    Script Date: 9/29/2016 9:18:49 PM ******/

SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
SET ANSI_PADDING ON;
GO
CREATE TABLE [dbo].[_xref_lastName_random]
([ID]             [INT] IDENTITY(1, 1)
                        NOT NULL,
 [LastName]       [VARCHAR](50) NULL,
 [LastNameRandom] [VARCHAR](10) NULL,
 PRIMARY KEY CLUSTERED([ID] ASC)
 WITH(PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
)
ON [PRIMARY];
GO
SET ANSI_PADDING OFF;
GO

/****** Object:  Table [dbo].[_xref_maxDate]    Script Date: 9/29/2016 9:21:43 PM ******/

SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
SET ANSI_PADDING ON;
GO
CREATE TABLE [dbo].[_xref_maxDate]
([ID]      [INT] IDENTITY(1, 1)
                 NOT NULL,
 [param]   [VARCHAR](50) NULL,
 [maxDate] [DATETIME] NULL,
 [Adj]     [FLOAT] NULL,
 PRIMARY KEY CLUSTERED([ID] ASC)
 WITH(PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
)
ON [PRIMARY];
GO
SET ANSI_PADDING OFF;
GO

/****** Object:  Table [dbo].[_xref_MRN_random]    Script Date: 9/29/2016 9:24:14 PM ******/

SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
SET ANSI_PADDING ON;
GO
CREATE TABLE [dbo].[_xref_MRN_random]
([ID]  [INT] IDENTITY(1, 1)
             NOT NULL,
 [MRN] [VARCHAR](MAX) NULL,
 [MRNRandom] AS ((50000) + [ID]),
 PRIMARY KEY CLUSTERED([ID] ASC)
 WITH(PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
)
ON [PRIMARY] TEXTIMAGE_ON [PRIMARY];
GO
SET ANSI_PADDING OFF;
GO

/****** Object:  Table [dbo].[_xref_providerName_random]    Script Date: 9/29/2016 9:26:31 PM ******/

SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
SET ANSI_PADDING ON;
GO
CREATE TABLE [dbo].[_xref_providerName_random]
([ID]                 [INT] IDENTITY(1, 1)
                            NOT NULL,
 [providerName]       [VARCHAR](90) NULL,
 [providerNameRandom] [VARCHAR](90) NULL,
 PRIMARY KEY CLUSTERED([ID] ASC)
 WITH(PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
)
ON [PRIMARY];
GO
SET ANSI_PADDING OFF;
GO

/****** Object:  Table [dbo].[_xref_Room_random]    Script Date: 9/29/2016 9:28:27 PM ******/

SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
SET ANSI_PADDING ON;
GO
CREATE TABLE [dbo].[_xref_Room_random]
([ID]   [INT] IDENTITY(1, 1)
              NOT NULL,
 [Room] [VARCHAR](MAX) NULL,
 [RoomRandom] AS ((200) + [ID]),
 PRIMARY KEY CLUSTERED([ID] ASC)
 WITH(PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
)
ON [PRIMARY] TEXTIMAGE_ON [PRIMARY];
GO
SET ANSI_PADDING OFF;
GO

/****** Object:  Table [dbo].[_xref_Unit_random]    Script Date: 9/29/2016 9:30:15 PM ******/

SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
SET ANSI_PADDING ON;
GO
CREATE TABLE [dbo].[_xref_Unit_random]
([ID]   [INT] IDENTITY(1, 1)
              NOT NULL,
 [Unit] [VARCHAR](50) NULL,
 [UnitRandom] AS (CASE
                      WHEN [unit] LIKE '%ICU%'
                      THEN CONCAT('ICU ', [ID])
                      WHEN [unit] LIKE '%Med Surg%'
                      THEN CONCAT('MedSurg ', [ID])
                      WHEN [unit] LIKE '%ED%'
                      THEN CONCAT('ED ', [ID])
                      ELSE CONCAT('Unit ', [ID])
                  END)
)
ON [PRIMARY];
GO
SET ANSI_PADDING OFF;
GO

/****** Object:  Table [dbo].[_xref_visitNum_random]    Script Date: 9/29/2016 9:32:22 PM ******/

SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
CREATE TABLE [dbo].[_xref_visitNum_random]
([ID]       [INT] IDENTITY(1, 1)
                  NOT NULL,
 [visitNum] [REAL] NULL,
 [visitNumRandom] AS ((800000) + [ID]),
 PRIMARY KEY CLUSTERED([ID] ASC)
 WITH(PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
)
ON [PRIMARY];
GO

/****** Object:  Table [dbo].[_xref_Room_random]    Script Date: 10/3/2016 2:48:59 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[_xref_Zip_random](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Zip] [varchar](max) NULL,
	[ZipRandom]  AS ((28000)+[ID]),
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO