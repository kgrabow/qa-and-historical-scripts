USE [perahealthinput];
GO

/****** Object:  Table [dbo].[_xref_firstName_random]    Script Date: 9/27/2016 3:00:43 PM ******/

SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
SET ANSI_PADDING ON;
GO
CREATE TABLE [dbo].[_xref_firstName_random]
([ID]              [INT] IDENTITY(1, 1)
                         NOT NULL,
 [FirstName]       [VARCHAR](50) NULL,
 [FirstNameRandom] [VARCHAR](10) NULL,
 PRIMARY KEY CLUSTERED([ID] ASC)
 WITH(PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
)
ON [PRIMARY];
GO
CREATE TABLE [dbo].[_xref_lastName_random]
([ID]             [INT] IDENTITY(1, 1)
                        NOT NULL,
 [LastName]       [VARCHAR](50) NULL,
 [LastNameRandom] [VARCHAR](10) NULL,
 PRIMARY KEY CLUSTERED([ID] ASC)
 WITH(PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
)
ON [PRIMARY];
GO
CREATE TABLE [dbo].[_xref_maxDate]
([ID]      [INT] IDENTITY(1, 1)
                 NOT NULL,
 [param]   [VARCHAR](50) NULL,
 [maxDate] [DATETIME] NULL,
 [Adj]     [FLOAT] NULL,
 PRIMARY KEY CLUSTERED([ID] ASC)
 WITH(PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
)
ON [PRIMARY];
GO
CREATE TABLE [dbo].[_xref_MRN_random]
([ID]  [INT] IDENTITY(1, 1)
             NOT NULL,
 [MRN] [VARCHAR](MAX) NULL,
 [MRNRandom] AS ((50000) + [ID]),
 PRIMARY KEY CLUSTERED([ID] ASC)
 WITH(PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
)
ON [PRIMARY] TEXTIMAGE_ON [PRIMARY];
GO
CREATE TABLE [dbo].[_xref_providerName_random]
([ID]                 [INT] IDENTITY(1, 1)
                            NOT NULL,
 [providerName]       [VARCHAR](90) NULL,
 [providerNameRandom] [VARCHAR](90) NULL,
 PRIMARY KEY CLUSTERED([ID] ASC)
 WITH(PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
)
ON [PRIMARY];
GO
CREATE TABLE [dbo].[_xref_visitNum_random]
([ID]       [INT] IDENTITY(1, 1)
                  NOT NULL,
 [visitNum] [REAL] NULL,
 [visitNumRandom] AS ((800000) + [ID]),
 PRIMARY KEY CLUSTERED([ID] ASC)
 WITH(PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
)
ON [PRIMARY];
GO
ALTER VIEW [dbo].[adt_random]
AS
     SELECT fac AS fac,
            xM.MRNRandom AS mrn,
            xV.visitNumRandom AS visitNum,
            xV.visitNumRandom AS acctNum,
            xM.MRNRandom AS ein,
            xf.FirstNameRandom AS firstName,
            'TEST' AS lastName,
            birthdt+
     (
         SELECT adj
         FROM _xref_maxDate
         WHERE param = 'Adjustment'
     ) AS birthDt,
            sex AS sex,
            maritalSt AS maritalSt,
            zipCd AS zipCd,
            xPr.providerNameRandom AS provider,
            patClass AS patClass,
            admitDt+
     (
         SELECT adj
         FROM _xref_maxDate
         WHERE param = 'Adjustment'
     ) AS admitDt,
            dischDt+
     (
         SELECT adj
         FROM _xref_maxDate
         WHERE param = 'Adjustment'
     ) AS dischDt,
            dischDisp AS dischDisp,
            Unit AS unit,
            Room AS room,
            Bed AS bed,
            diagnosis AS diagnosis,
            diagCode AS diagCode,
            diagType AS diagType,
            msgDt+
     (
         SELECT adj
         FROM _xref_maxDate
         WHERE param = 'Adjustment'
     ) AS msgDt
     FROM [dbo].[adt_historical_extract_qa] adt WITH (nolock)
          JOIN [dbo].[_xref_visitNum_random] xV ON xv.visitNum = CAST(adt.visitNum AS INT)
          JOIN [dbo].[_xref_MRN_random] xM ON xM.MRN = adt.mrn
          JOIN [dbo].[_xref_firstName_random] xF ON xF.FirstName = adt.firstName
          JOIN [dbo].[_xref_providerName_random] xPr ON xPr.providerName = adt.provider;
GO
GO
CREATE VIEW [dbo].[lab_random]
AS
     SELECT fac AS fac,
            xM.mrnRandom AS mrn,
            xV.visitnumRandom AS visitNum,
            msgVar AS msgVar,
            msgVal AS msgVal,
            msgOrigVal AS msgOrigVal,
            recordedDt+
     (
         SELECT adj
         FROM _xref_maxDate
         WHERE param = 'Adjustment'
     ) AS recordedDt,
            msgDt+
     (
         SELECT adj
         FROM _xref_maxDate
         WHERE param = 'Adjustment'
     ) AS msgDt
     FROM [dbo].[lab_historical_extract_qa] lab
          JOIN [dbo].[_xref_visitNum_random] xV ON xv.visitNum = lab.VisitNum
          JOIN [dbo].[_xref_MRN_random] xM ON xM.MRN = lab.MRN;
GO
CREATE VIEW [dbo].[nurse_random]
AS
     SELECT fac AS fac,
            xM.mrnRandom AS mrn,
            xV.visitnumRandom AS visitNum,
            msgVar AS msgVar,
            msgVal AS msgVal,
            msgOrigVal AS msgOrigVal,
            recordedDt+
     (
         SELECT adj
         FROM _xref_maxDate
         WHERE param = 'Adjustment'
     ) AS recordedDt,
            msgDt+
     (
         SELECT adj
         FROM _xref_maxDate
         WHERE param = 'Adjustment'
     ) AS msgDt
     FROM [dbo].[nurse_historical_extract_qa] nurse
          JOIN [dbo].[_xref_visitNum_random] xV ON xv.visitNum = nurse.VisitNum
          JOIN [dbo].[_xref_MRN_random] xM ON xM.MRN = nurse.MRN;
GO
CREATE VIEW [dbo].[vital_random]
AS
     SELECT fac AS fac,
            xM.mrnRandom AS mrn,
            xV.visitnumRandom AS visitNum,
            msgVar AS msgVar,
            msgVal AS msgVal,
            msgOrigVal AS msgOrigVal,
            recordedDt+
     (
         SELECT adj
         FROM _xref_maxDate
         WHERE param = 'Adjustment'
     ) AS recordedDt,
            msgDt+
     (
         SELECT adj
         FROM _xref_maxDate
         WHERE param = 'Adjustment'
     ) AS msgDt
     FROM [dbo].[vital_historical_extract_qa] vital
          JOIN [dbo].[_xref_visitNum_random] xV ON xv.visitNum = vital.VisitNum
          JOIN [dbo].[_xref_MRN_random] xM ON xM.MRN = vital.MRN;
GO