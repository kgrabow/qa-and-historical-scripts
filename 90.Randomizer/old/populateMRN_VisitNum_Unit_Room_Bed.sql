delete from _xref_MRN_random;
delete from _xref_visitNum_random;
delete from _xref_Unit_random;
delete from _xref_Room_random;
delete from _xref_Bed_random;


insert into [dbo].[_xref_MRN_random]
select distinct mrn
from [dbo].[adt_historical_extract_qa]

-- select * from [dbo].[_xref_MRN_random]


insert into [dbo].[_xref_VisitNum_random]
select distinct VisitNum
from [dbo].[adt_historical_extract_qa]

-- -- select * from [dbo].[_xref_VisitNum_random]

insert into [dbo].[_xref_Unit_random]
select distinct Unit
from [dbo].[adt_historical_extract_qa]

--select * from [dbo].[_xref_Unit_random]

insert into [dbo].[_xref_Room_random]
select distinct room
from [dbo].[adt_historical_extract_qa]

--select * from [dbo].[_xref_Room_random]

insert into [dbo].[_xref_Bed_random]
select distinct bed
from [dbo].[adt_historical_extract_qa]

--select * from [dbo].[_xref_Bed_random]