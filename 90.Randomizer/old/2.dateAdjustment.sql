-- This query counts the days since the max date in the 4 historical views, then populates
-- Adjustment with that number minus 1.
-- The Random views will then adjust all dates by that factor to anoymize all dates equally

delete from _xref_maxDate

declare @adtDischDate datetime;
declare @adtMsgDate datetime;
declare @labRecordedDate datetime;
declare @labMsgDate datetime;
declare @nurseRecordedDate datetime;
declare @nurseMsgDate datetime;
declare @vitalRecordedDate datetime;
declare @vitalMsgDate datetime;

declare @maxOverall datetime;

declare @adjustment float;

set @adtDischDate = (select max(dischdt) from adt_historical_extract_qa)
set @adtMsgDate = (select max(msgDt) from adt_historical_extract_qa)
set @labRecordedDate = (select max(recordedDt) from lab_historical_extract_qa)
set @labMsgDate = (select max(msgDt) from lab_historical_extract_qa)
set @nurseRecordedDate = (select max(recordedDt) from nurse_historical_extract_qa)
set @nurseMsgDate = (select max(msgDt) from nurse_historical_extract_qa)
set @vitalRecordedDate = (select max(recordedDt) from vital_historical_extract_qa)
set @vitalMsgDate = (select max(msgDt) from vital_historical_extract_qa)

insert into _xref_maxDate (param,maxDate) values ('adt_dischDt',@adtDischDate)
insert into _xref_maxDate (param,maxDate) values ('adt_msgDt',@adtMsgDate)
insert into _xref_maxDate (param,maxDate) values ('lab_recordedDt',@labRecordedDate)
insert into _xref_maxDate (param,maxDate) values ('lab_msgDt',@labMsgDate)
insert into _xref_maxDate (param,maxDate) values ('nurse_recordedDt',@nurseRecordedDate)
insert into _xref_maxDate (param,maxDate) values ('nurse_msgDt',@nurseMsgDate)
insert into _xref_maxDate (param,maxDate) values ('vital_recordedDt',@vitalRecordedDate)
insert into _xref_maxDate (param,maxDate) values ('vital_msgDt',@vitalMsgDate)

set @maxOverall = (select max(maxDate) from _xref_maxDate)
insert into _xref_maxDate (param,maxDate) values ('maxOverall',@maxOverall)

set @adjustment = (select DATEDIFF(dd,@maxOverall, GETDATE()) ) -1
insert into _xref_maxDate (param,Adj) values ('Adjustment',@adjustment)


select * from _xref_maxDate

