delete from _xref_MRN_random;
delete from _xref_visitNum_random;
delete from _xref_Unit_random;
delete from _xref_Room_random;
delete from _xref_Bed_random;

-- 1. Populate MRN xref with unique MRN values
-- Change the 10 in the query below to how many patients you want to get
declare @distinctMRN table (
mrn varchar(20)
)
insert into @distinctMRN
select distinct mrn from adt_historical_extract_qa

insert into [dbo].[_xref_MRN_random] (mrn)
select top 10  mrn
 from @distinctMRN
order by newid()
-- select * from [dbo].[_xref_MRN_random]

-- 2. Populate VisitNum xref
insert into [dbo].[_xref_VisitNum_random]
select distinct VisitNum
from [dbo].[adt_historical_extract_qa]
where mrn in 
(select mrn from _xref_MRN_random)
-- select * from [dbo].[_xref_VisitNum_random]

-- 3. Populate Unit xref
insert into [dbo].[_xref_Unit_random]
select distinct Unit
from [dbo].[adt_historical_extract_qa]
where mrn in 
(select mrn from _xref_MRN_random)
--select * from [dbo].[_xref_Unit_random]

-- 4. Populate Room xref
insert into [dbo].[_xref_Room_random]
select distinct room
from [dbo].[adt_historical_extract_qa]
where mrn in 
(select mrn from _xref_MRN_random)
--select * from [dbo].[_xref_Room_random]

-- 5. Populate Bed xref
insert into [dbo].[_xref_Bed_random]
select distinct bed
from [dbo].[adt_historical_extract_qa]
where mrn in 
(select mrn from _xref_MRN_random)

--select * from [dbo].[_xref_Bed_random]