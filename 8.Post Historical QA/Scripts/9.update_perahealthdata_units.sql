-- Run these to set the unittypes for the units

update unit
set unittype_id = 9
where descr like '%ICU%'

update unit
set unittype_id = 11
where descr like '%MedSurg%'

update unit
set unittype_id = 6
where descr like '%ED%'

