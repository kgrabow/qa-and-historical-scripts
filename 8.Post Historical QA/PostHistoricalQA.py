import pypyodbc

connection = pypyodbc.connect('Driver={Sql Server};'
                              'Server=localhost;'
                              'Database=perahealthdata;'
                              ##                              'dsn=PHLTH-PHI-DB;'
                              'uid=kgrabow;'

                              'Trusted_Connection=True'
                              )

with open("Output.html", "w") as text_file:
    text_file.write("<html>\n")
    text_file.write("  <head>\n")
    text_file.write("  <h2>Post Processing QA</h2><br>\n")

    text_file.write(
        "	<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\" integrity=\"sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u\" crossorigin=\"anonymous\"> \n")
    text_file.write(
        "	<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css\" integrity=\"sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp\" crossorigin=\"anonymous\"> \n")
    text_file.write(
        "	<script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\" integrity=\"sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa\" crossorigin=\"anonymous\"></script> \n")

    text_file.write("    <script type=\"text/javascript\" src=\"https://www.gstatic.com/charts/loader.js\"></script>\n")
    text_file.write("    <script type=\"text/javascript\">\n")
    text_file.write("      google.charts.load('current', {'packages':['corechart','table']});\n")
    text_file.write("     google.charts.setOnLoadCallback(drawBradenChart);\n")
    text_file.write("     google.charts.setOnLoadCallback(drawSystolChart);\n")
    text_file.write("     google.charts.setOnLoadCallback(drawDiastolicChart);\n")

    text_file.write("      google.charts.setOnLoadCallback(drawBUNChart);\n")
    text_file.write("      google.charts.setOnLoadCallback(drawChlorideChart);\n")
    text_file.write("      google.charts.setOnLoadCallback(drawCreatinineChart);\n")

    text_file.write("      google.charts.setOnLoadCallback(drawHeartRateChart);\n")
    text_file.write("      google.charts.setOnLoadCallback(drawHemoglobinChart);\n")
    text_file.write("      google.charts.setOnLoadCallback(drawO2satChart);\n")

    text_file.write("      google.charts.setOnLoadCallback(drawPotassiumChart);\n")
    text_file.write("      google.charts.setOnLoadCallback(drawRespChart);\n")
    text_file.write("      google.charts.setOnLoadCallback(drawSodiumChart);\n")

    text_file.write("      google.charts.setOnLoadCallback(drawTempChart);\n")
    text_file.write("      google.charts.setOnLoadCallback(drawWBCChart);\n")

    text_file.write("      google.charts.setOnLoadCallback(drawHeartRhythmChart);\n")
    text_file.write("      google.charts.setOnLoadCallback(drawCardiacPassFail);\n")
    text_file.write("      google.charts.setOnLoadCallback(drawNutritionPassFail);\n")

    text_file.write("      google.charts.setOnLoadCallback(drawGastroPassFail);\n")
    text_file.write("      google.charts.setOnLoadCallback(drawGenitoPassFail);\n")
    text_file.write("      google.charts.setOnLoadCallback(drawMusculoPassFail);\n")
    text_file.write("      google.charts.setOnLoadCallback(drawNeuroPassFail);\n")

    text_file.write("      google.charts.setOnLoadCallback(drawPerivascPassFail);\n")
    text_file.write("      google.charts.setOnLoadCallback(drawPsychoPassFail);\n")
    text_file.write("      google.charts.setOnLoadCallback(drawRespPassFail);\n")

    text_file.write("      google.charts.setOnLoadCallback(drawSafePassFail);\n")
    text_file.write("      google.charts.setOnLoadCallback(drawSkinPassFail);\n")


    ##############################################
    # Function begin Braden #
    text_file.write("    function drawBradenChart() { \n")
    text_file.write("      var data = google.visualization.arrayToDataTable([ \n")
    text_file.write("        ['Count', 'Score'] \n")

# SQL insert
cursor = connection.cursor()
query_string = "select braden, count(braden) \
from [dbo].[hlthscore] \
where braden is not null \
and valid = 1                \
group by braden \
order by braden asc"
cursor.execute(query_string)

with open('Output.html', 'a') as file:
    for row in cursor:
        file.write(str(row).replace("(", ",[").replace(")", "]").replace("'", ""))

with open("Output.html", "a") as text_file:
    text_file.write("    ]);\n")

    text_file.write("   var options = {\n")
    text_file.write("        title: 'Braden Scale Score (19-23)',\n")
    text_file.write("        curveType: 'function',\n")
    text_file.write("       legend: { position: 'right' }\n")
    text_file.write("    };\n")

    text_file.write("   var chart = new google.visualization.LineChart(document.getElementById('braden_chart'));\n")

    text_file.write("    chart.draw(data, options);\n")
    text_file.write("   }\n")
    # Function end Braden #
    ##############################################
    # Function begin systol#
    text_file.write("    function drawSystolChart() { \n")
    text_file.write("      var data = google.visualization.arrayToDataTable([ \n")
    text_file.write("        ['Count', 'mmHg'] \n")

# SQL insert
cursor = connection.cursor()
query_string = "select cast(systol as char), count(systol) \
from [dbo].[hlthscore] \
where systol is not null \
and valid = 1                \
group by systol \
order by systol asc"
cursor.execute(query_string)

with open('Output.html', 'a') as file:
    for row in cursor:
        file.write(str(row).replace("(", ",[").replace(")", "]").replace("'", ""))

with open("Output.html", "a") as text_file:
    text_file.write("    ]);\n")

    text_file.write("   var options = {\n")
    text_file.write("        title: 'Systolic (101-189)',\n")
    text_file.write("        curveType: 'function',\n")
    text_file.write("       legend: { position: 'right' }\n")
    text_file.write("    };\n")

    text_file.write("   var chart = new google.visualization.LineChart(document.getElementById('systol_chart'));\n")

    text_file.write("    chart.draw(data, options);\n")
    text_file.write("   }\n")
    # Function end Systolic#
    ##############################################
    # Function begin Diastolic#
    text_file.write("    function drawDiastolicChart() { \n")
    text_file.write("      var data = google.visualization.arrayToDataTable([ \n")
    text_file.write("        ['Count', 'mmHg'] \n")

# SQL insert
cursor = connection.cursor()
query_string = "select cast(diastol as char), count(diastol) \
from [dbo].[hlthscore] \
where diastol is not null \
and valid = 1                \
group by diastol \
order by diastol asc"
cursor.execute(query_string)

with open('Output.html', 'a') as file:
    for row in cursor:
        file.write(str(row).rstrip().lstrip().replace("(", ",[").replace(")", "]").replace("'", ""))

with open("Output.html", "a") as text_file:
    text_file.write("    ]);\n")

    text_file.write("   var options = {\n")
    text_file.write("        title: 'Diastolic (49-105)',\n")
    text_file.write("        curveType: 'function',\n")
    text_file.write("       legend: { position: 'right' }\n")
    text_file.write("    };\n")

    text_file.write("   var chart = new google.visualization.LineChart(document.getElementById('diastolic_chart'));\n")

    text_file.write("    chart.draw(data, options);\n")
    text_file.write("   }\n")
    # Function end Diastolic#
    ##############################################
    # Function begin BUN #
    text_file.write("    function drawBUNChart() { \n")
    text_file.write("      var data = google.visualization.arrayToDataTable([ \n")
    text_file.write("        ['Count', 'mg/dL'] \n")

# SQL insert
cursor = connection.cursor()
query_string = "select cast(bun as char), count(bun) \
from [dbo].[hlthscore] \
where bun is not null \
and valid = 1                \
group by bun \
order by bun asc"
cursor.execute(query_string)

with open('Output.html', 'a') as file:
    for row in cursor:
        file.write(str(row).replace("(", ",[").replace(")", "]").replace("'", ""))

with open("Output.html", "a") as text_file:
    text_file.write("    ]);\n")

    text_file.write("   var options = {\n")
    text_file.write("        title: 'BUN (0-22)',\n")
    text_file.write("        curveType: 'function',\n")
    text_file.write("       legend: { position: 'right' }\n")
    text_file.write("    };\n")

    text_file.write("   var chart = new google.visualization.LineChart(document.getElementById('bun_chart'));\n")

    text_file.write("    chart.draw(data, options);\n")
    text_file.write("   }\n")
    # Function end BUN#
    ##############################################
    # Function begin Chloride #
    text_file.write("    function drawChlorideChart() { \n")
    text_file.write("      var data = google.visualization.arrayToDataTable([ \n")
    text_file.write("        ['Count', 'mEq/L'] \n")

# SQL insert
cursor = connection.cursor()
query_string = "select cast(Chloride as char), count(Chloride) \
from [dbo].[hlthscore] \
where Chloride is not null \
and valid = 1                \
group by Chloride \
order by Chloride asc"
cursor.execute(query_string)

with open('Output.html', 'a') as file:
    for row in cursor:
        file.write(str(row).replace("(", ",[").replace(")", "]").replace("'", ""))

with open("Output.html", "a") as text_file:
    text_file.write("    ]);\n")

    text_file.write("   var options = {\n")
    text_file.write("        title: 'Chloride (100-109)',\n")
    text_file.write("        curveType: 'function',\n")
    text_file.write("       legend: { position: 'right' }\n")
    text_file.write("    };\n")

    text_file.write("   var chart = new google.visualization.LineChart(document.getElementById('chloride_chart'));\n")

    text_file.write("    chart.draw(data, options);\n")
    text_file.write("   }\n")
    # Function end Chloride#
    ##############################################
    # Function begin Creatinine #
    text_file.write("    function drawCreatinineChart() { \n")
    text_file.write("      var data = google.visualization.arrayToDataTable([ \n")
    text_file.write("        ['Count', 'mg/dl'] \n")

# SQL insert
cursor = connection.cursor()
query_string = "select cast(creatinine as char), count(creatinine) \
from [dbo].[hlthscore] \
where creatinine is not null \
and valid = 1                \
group by creatinine \
order by creatinine asc"
cursor.execute(query_string)

with open('Output.html', 'a') as file:
    for row in cursor:
        file.write(str(row).replace("(", ",[").replace(")", "]").replace("'", ""))

with open("Output.html", "a") as text_file:
    text_file.write("    ]);\n")

    text_file.write("   var options = {\n")
    text_file.write("        title: 'Creatinine (0.5-1.4)',\n")
    text_file.write("        curveType: 'function',\n")
    text_file.write("       legend: { position: 'right' }\n")
    text_file.write("    };\n")

    text_file.write("   var chart = new google.visualization.LineChart(document.getElementById('creatinine_chart'));\n")

    text_file.write("    chart.draw(data, options);\n")
    text_file.write("   }\n")
    # Function end Creatinine#


    ##############################################
    # Function begin Heart Rate #
    text_file.write("    function drawHeartRateChart() { \n")
    text_file.write("      var data = google.visualization.arrayToDataTable([ \n")
    text_file.write("        ['Count', 'bpm'] \n")

# SQL insert
cursor = connection.cursor()
query_string = "select cast(heart_rt as char), count(heart_rt) \
from [dbo].[hlthscore] \
where heart_rt is not null \
and valid = 1                \
group by heart_rt \
order by heart_rt asc"
cursor.execute(query_string)

with open('Output.html', 'a') as file:
    for row in cursor:
        file.write(str(row).replace("(", ",[").replace(")", "]").replace("'", ""))

with open("Output.html", "a") as text_file:
    text_file.write("    ]);\n")

    text_file.write("   var options = {\n")
    text_file.write("        title: 'Heart Rate (43-92)',\n")
    text_file.write("        curveType: 'function',\n")
    text_file.write("       legend: { position: 'right' }\n")
    text_file.write("    };\n")

    text_file.write("   var chart = new google.visualization.LineChart(document.getElementById('heartrate_chart'));\n")

    text_file.write("    chart.draw(data, options);\n")
    text_file.write("   }\n")
    # Function end Heart Rate#

    ##############################################
    # Function begin Hemoglobin #
    text_file.write("    function drawHemoglobinChart() { \n")
    text_file.write("      var data = google.visualization.arrayToDataTable([ \n")
    text_file.write("        ['Count', 'mg/L'] \n")

# SQL insert
cursor = connection.cursor()
query_string = "select cast(hgb as char), count(hgb) \
from [dbo].[hlthscore] \
where hgb is not null \
and valid = 1                \
group by hgb \
order by hgb asc"
cursor.execute(query_string)

with open('Output.html', 'a') as file:
    for row in cursor:
        file.write(str(row).replace("(", ",[").replace(")", "]").replace("'", ""))

with open("Output.html", "a") as text_file:
    text_file.write("    ]);\n")

    text_file.write("   var options = {\n")
    text_file.write("        title: 'Hemoglobin (10.6-18)',\n")
    text_file.write("        curveType: 'function',\n")
    text_file.write("       legend: { position: 'right' }\n")
    text_file.write("    };\n")

    text_file.write("   var chart = new google.visualization.LineChart(document.getElementById('hemoglobin_chart'));\n")

    text_file.write("    chart.draw(data, options);\n")
    text_file.write("   }\n")
    # Function end Hemoglobin#

    ##############################################
    # Function begin Pulse Oximetry#
    text_file.write("    function drawO2satChart() { \n")
    text_file.write("      var data = google.visualization.arrayToDataTable([ \n")
    text_file.write("        ['Count', '%'] \n")

# SQL insert
cursor = connection.cursor()
query_string = "select cast(pulse_ox as char), count(pulse_ox) \
from [dbo].[hlthscore] \
where pulse_ox is not null \
and valid = 1                \
group by pulse_ox \
order by pulse_ox asc"
cursor.execute(query_string)

with open('Output.html', 'a') as file:
    for row in cursor:
        file.write(str(row).replace("(", ",[").replace(")", "]").replace("'", ""))

with open("Output.html", "a") as text_file:
    text_file.write("    ]);\n")

    text_file.write("   var options = {\n")
    text_file.write("        title: 'Pulse Oximetry (93-100)',\n")
    text_file.write("        curveType: 'function',\n")
    text_file.write("       legend: { position: 'right' }\n")
    text_file.write("    };\n")

    text_file.write("   var chart = new google.visualization.LineChart(document.getElementById('O2sat_chart'));\n")

    text_file.write("    chart.draw(data, options);\n")
    text_file.write("   }\n")
    # Function end Pulse Oximetry#


    ##############################################
    # Function begin Potassium #
    text_file.write("    function drawPotassiumChart() { \n")
    text_file.write("      var data = google.visualization.arrayToDataTable([ \n")
    text_file.write("        ['Count', 'mEq/L'] \n")

# SQL insert
cursor = connection.cursor()
query_string = "select cast(potassium as char), count(potassium) \
from [dbo].[hlthscore] \
where potassium is not null \
and valid = 1                \
group by potassium \
order by potassium asc"
cursor.execute(query_string)

with open('Output.html', 'a') as file:
    for row in cursor:
        file.write(str(row).replace("(", ",[").replace(")", "]").replace("'", ""))

with open("Output.html", "a") as text_file:
    text_file.write("    ]);\n")

    text_file.write("   var options = {\n")
    text_file.write("        title: 'Potassium (3.0-4.7)',\n")
    text_file.write("        curveType: 'function',\n")
    text_file.write("       legend: { position: 'right' }\n")
    text_file.write("    };\n")

    text_file.write("   var chart = new google.visualization.LineChart(document.getElementById('potassium_chart'));\n")

    text_file.write("    chart.draw(data, options);\n")
    text_file.write("   }\n")
    # Function end Potassium#

    ##############################################
    # Function begin Respiratory Rate #
    text_file.write("    function drawRespChart() { \n")
    text_file.write("      var data = google.visualization.arrayToDataTable([ \n")
    text_file.write("        ['Count', 'breaths/min'] \n")

# SQL insert
cursor = connection.cursor()
query_string = "select cast(resp_rt as char), count(resp_rt) \
from [dbo].[hlthscore] \
where resp_rt is not null \
and valid = 1                \
group by resp_rt \
order by resp_rt asc"
cursor.execute(query_string)

with open('Output.html', 'a') as file:
    for row in cursor:
        file.write(str(row).replace("(", ",[").replace(")", "]").replace("'", ""))

with open("Output.html", "a") as text_file:
    text_file.write("    ]);\n")

    text_file.write("   var options = {\n")
    text_file.write("        title: 'Respiratory Rate (15-19)',\n")
    text_file.write("        curveType: 'function',\n")
    text_file.write("       legend: { position: 'right' }\n")
    text_file.write("    };\n")

    text_file.write("   var chart = new google.visualization.LineChart(document.getElementById('resp_chart'));\n")

    text_file.write("    chart.draw(data, options);\n")
    text_file.write("   }\n")
    # Function end Respiratory Rate #


    ##############################################
    # Function begin Sodium #
    text_file.write("    function drawSodiumChart() { \n")
    text_file.write("      var data = google.visualization.arrayToDataTable([ \n")
    text_file.write("        ['Count', 'mEq/L'] \n")

# SQL insert
cursor = connection.cursor()
query_string = "select cast(sodium as char), count(sodium) \
from [dbo].[hlthscore] \
where sodium is not null \
and valid = 1                \
group by sodium \
order by sodium asc"
cursor.execute(query_string)

with open('Output.html', 'a') as file:
    for row in cursor:
        file.write(str(row).replace("(", ",[").replace(")", "]").replace("'", ""))

with open("Output.html", "a") as text_file:
    text_file.write("    ]);\n")

    text_file.write("   var options = {\n")
    text_file.write("        title: 'Sodium (135-145)',\n")
    text_file.write("        curveType: 'function',\n")
    text_file.write("       legend: { position: 'right' }\n")
    text_file.write("    };\n")

    text_file.write("   var chart = new google.visualization.LineChart(document.getElementById('sodium_chart'));\n")

    text_file.write("    chart.draw(data, options);\n")
    text_file.write("   }\n")
    # Function end Sodium#

    ##############################################
    # Function begin Temperature #
    text_file.write("    function drawTempChart() { \n")
    text_file.write("      var data = google.visualization.arrayToDataTable([ \n")
    text_file.write("        ['Count', '°F'] \n")

# SQL insert
cursor = connection.cursor()
query_string = "select cast(temp as char), count(temp) \
from [dbo].[hlthscore] \
where temp is not null \
and valid = 1                \
group by temp \
order by temp asc"
cursor.execute(query_string)

with open('Output.html', 'a') as file:
    for row in cursor:
        file.write(str(row).replace("(", ",[").replace(")", "]").replace("'", ""))

with open("Output.html", "a") as text_file:
    text_file.write("    ]);\n")

    text_file.write("   var options = {\n")
    text_file.write("        title: 'Temperature (97-100.1)',\n")
    text_file.write("        curveType: 'function',\n")
    text_file.write("       legend: { position: 'right' }\n")
    text_file.write("    };\n")

    text_file.write("   var chart = new google.visualization.LineChart(document.getElementById('temp_chart'));\n")

    text_file.write("    chart.draw(data, options);\n")
    text_file.write("   }\n")
    # Function end Temperature #
    ##############################################
    # Function begin WBC #
    text_file.write("    function drawWBCChart() { \n")
    text_file.write("      var data = google.visualization.arrayToDataTable([ \n")
    text_file.write("        ['Count', 'mcL'] \n")

# SQL insert
cursor = connection.cursor()
query_string = "select cast(wbc as char), count(wbc) \
from [dbo].[hlthscore] \
where wbc is not null \
and valid = 1                \
group by wbc \
order by wbc asc"
cursor.execute(query_string)

with open('Output.html', 'a') as file:
    for row in cursor:
        file.write(str(row).replace("(", ",[").replace(")", "]").replace("'", ""))

with open("Output.html", "a") as text_file:
    text_file.write("    ]);\n")

    text_file.write("   var options = {\n")
    text_file.write("        title: 'White Blood Count (4.18-14.92)',\n")
    text_file.write("        curveType: 'function',\n")
    text_file.write("       legend: { position: 'right' }\n")
    text_file.write("    };\n")

    text_file.write("   var chart = new google.visualization.LineChart(document.getElementById('wbc_chart'));\n")

    text_file.write("    chart.draw(data, options);\n")
    text_file.write("   }\n")
    # Function end WBC#

    ##############################################
    # Function begin Heart Rhythm #
    text_file.write("    function drawHeartRhythmChart() { \n")
    text_file.write("      var data = google.visualization.arrayToDataTable([ \n")
    text_file.write("        ['Count', 'Score'] \n")

# SQL insert
cursor = connection.cursor()
query_string = "select ''''+heart_rhy+'''', count(heart_rhy) \
from [dbo].[hlthscore] \
where heart_rhy is not null \
and valid = 1                \
group by heart_rhy \
order by heart_rhy asc"
cursor.execute(query_string)

with open('Output.html', 'a') as file:
    for row in cursor:
        file.write(str(row).replace("(", ",[").replace(")", "]").replace("'", ""))

with open("Output.html", "a") as text_file:
    text_file.write("    ]);\n")

    text_file.write("   var options = {\n")
    text_file.write("        title: 'Heart Rhythm',\n")
    text_file.write("        is3D: true,\n")
    text_file.write("       legend: { position: 'right' }\n")
    text_file.write("    };\n")

    text_file.write("   var chart = new google.visualization.PieChart(document.getElementById('heartrhythm_chart'));\n")

    text_file.write("    chart.draw(data, options);\n")
    text_file.write("   }\n")
    # Function end Heart Rhythm #

    ##############################################
    ##############################################
    # Function begin Cardiac Pass/Fail #
    text_file.write("    function drawCardiacPassFail() { \n")
    text_file.write("      var data = google.visualization.arrayToDataTable([ \n")
    text_file.write("        ['Count', 'Score'] \n")

# SQL insert
cursor = connection.cursor()
query_string = "select ''''+'Pass'+'''',count(cardiac) \
from hlthscore \
where cardiac = 0 \
and valid = 1 \
UNION ALL \
select ''''+'Fail'+'''',count(cardiac) \
from hlthscore \
where cardiac = 1 \
and valid = 1 \
"
cursor.execute(query_string)

with open('Output.html', 'a') as file:
    for row in cursor:
        file.write(str(row).replace("(", ",[").replace(")", "]").replace("'", ""))

with open("Output.html", "a") as text_file:
    text_file.write("    ]);\n")

    text_file.write("   var options = {\n")
    text_file.write("        title: 'Cardiac Pass / Fail',\n")
    text_file.write("        is3D: true,\n")
    text_file.write("       slices: { 0:{color:'#26806E'}, 1:{color:'#dc1b06'} },\n")
    text_file.write("       legend: { position: 'right' }\n")
    text_file.write("    };\n")

    text_file.write("   var chart = new google.visualization.PieChart(document.getElementById('cardiac_passfail'));\n")

    text_file.write("    chart.draw(data, options);\n")
    text_file.write("   }\n")
# Function end Cardiac Pass / Fail #
    ##############################################
    # Function begin Nutrition Pass/Fail #
    text_file.write("    function drawNutritionPassFail() { \n")
    text_file.write("      var data = google.visualization.arrayToDataTable([ \n")
    text_file.write("        ['Count', 'Score'] \n")

# SQL insert
cursor = connection.cursor()
query_string = "select ''''+'Pass'+'''',count(food) \
from hlthscore \
where food = 0 \
and valid = 1 \
UNION ALL \
select ''''+'Fail'+'''',count(food) \
from hlthscore \
where food = 1 \
and valid = 1 \
"
cursor.execute(query_string)

with open('Output.html', 'a') as file:
    for row in cursor:
        file.write(str(row).replace("(", ",[").replace(")", "]").replace("'", ""))

with open("Output.html", "a") as text_file:
    text_file.write("    ]);\n")

    text_file.write("   var options = {\n")
    text_file.write("        title: 'Nutrition Pass / Fail',\n")
    text_file.write("        is3D: true,\n")
    text_file.write("       slices: { 0:{color:'#26806E'}, 1:{color:'#dc1b06'} },\n")
    text_file.write("       legend: { position: 'right' }\n")
    text_file.write("    };\n")

    text_file.write("   var chart = new google.visualization.PieChart(document.getElementById('nutrition_passfail'));\n")

    text_file.write("    chart.draw(data, options);\n")
    text_file.write("   }\n")
# Function end Nutrition Pass / Fail #
##############################################
# Function begin Gastro Pass/Fail #
    text_file.write("    function drawGastroPassFail() { \n")
    text_file.write("      var data = google.visualization.arrayToDataTable([ \n")
    text_file.write("        ['Count', 'Score'] \n")

# SQL insert
cursor = connection.cursor()
query_string = "select ''''+'Pass'+'''',count(gastro) \
from hlthscore \
where gastro = 0 \
and valid = 1 \
UNION ALL \
select ''''+'Fail'+'''',count(gastro) \
from hlthscore \
where gastro = 1 \
and valid = 1 \
"
cursor.execute(query_string)

with open('Output.html', 'a') as file:
    for row in cursor:
        file.write(str(row).replace("(", ",[").replace(")", "]").replace("'", ""))

with open("Output.html", "a") as text_file:
    text_file.write("    ]);\n")

    text_file.write("   var options = {\n")
    text_file.write("        title: 'Gastro Pass / Fail',\n")
    text_file.write("        is3D: true,\n")
    text_file.write("       slices: { 0:{color:'#26806E'}, 1:{color:'#dc1b06'} },\n")
    text_file.write("       legend: { position: 'right' }\n")
    text_file.write("    };\n")

    text_file.write("   var chart = new google.visualization.PieChart(document.getElementById('gastro_passfail'));\n")

    text_file.write("    chart.draw(data, options);\n")
    text_file.write("   }\n")
# Function end Gastro Pass / Fail #
    ##############################################
    # Function begin Genito Pass/Fail #
    text_file.write("    function drawGenitoPassFail() { \n")
    text_file.write("      var data = google.visualization.arrayToDataTable([ \n")
    text_file.write("        ['Count', 'Score'] \n")

# SQL insert
cursor = connection.cursor()
query_string = "select ''''+'Pass'+'''',count(genito) \
from hlthscore \
where genito = 0 \
and valid = 1 \
UNION ALL \
select ''''+'Fail'+'''',count(genito) \
from hlthscore \
where genito = 1 \
and valid = 1 \
"
cursor.execute(query_string)

with open('Output.html', 'a') as file:
    for row in cursor:
        file.write(str(row).replace("(", ",[").replace(")", "]").replace("'", ""))

with open("Output.html", "a") as text_file:
    text_file.write("    ]);\n")

    text_file.write("   var options = {\n")
    text_file.write("        title: 'Genito Pass / Fail',\n")
    text_file.write("        is3D: true,\n")
    text_file.write("       slices: { 0:{color:'#26806E'}, 1:{color:'#dc1b06'} },\n")
    text_file.write("       legend: { position: 'right' }\n")
    text_file.write("    };\n")

    text_file.write("   var chart = new google.visualization.PieChart(document.getElementById('genito_passfail'));\n")

    text_file.write("    chart.draw(data, options);\n")
    text_file.write("   }\n")
# Function end Gentio Pass / Fail #
    ##############################################
    # Function begin Musculo Pass/Fail #
    text_file.write("    function drawMusculoPassFail() { \n")
    text_file.write("      var data = google.visualization.arrayToDataTable([ \n")
    text_file.write("        ['Count', 'Score'] \n")

# SQL insert
cursor = connection.cursor()
query_string = "select ''''+'Pass'+'''',count(musculo) \
from hlthscore \
where musculo = 0 \
and valid = 1 \
UNION ALL \
select ''''+'Fail'+'''',count(musculo) \
from hlthscore \
where musculo = 1 \
and valid = 1 \
"
cursor.execute(query_string)

with open('Output.html', 'a') as file:
    for row in cursor:
        file.write(str(row).replace("(", ",[").replace(")", "]").replace("'", ""))

with open("Output.html", "a") as text_file:
    text_file.write("    ]);\n")

    text_file.write("   var options = {\n")
    text_file.write("        title: 'Musculo Pass / Fail',\n")
    text_file.write("        is3D: true,\n")
    text_file.write("       slices: { 0:{color:'#26806E'}, 1:{color:'#dc1b06'} },\n")
    text_file.write("       legend: { position: 'right' }\n")
    text_file.write("    };\n")

    text_file.write("   var chart = new google.visualization.PieChart(document.getElementById('musculo_passfail'));\n")

    text_file.write("    chart.draw(data, options);\n")
    text_file.write("   }\n")
# Function end Musculo Pass / Fail #
    ##############################################
    # Function begin Neuro Pass/Fail #
    text_file.write("    function drawNeuroPassFail() { \n")
    text_file.write("      var data = google.visualization.arrayToDataTable([ \n")
    text_file.write("        ['Count', 'Score'] \n")

# SQL insert
cursor = connection.cursor()
query_string = "select ''''+'Pass'+'''',count(neuro) \
from hlthscore \
where neuro = 0 \
and valid = 1 \
UNION ALL \
select ''''+'Fail'+'''',count(neuro) \
from hlthscore \
where neuro = 1 \
and valid = 1 \
"
cursor.execute(query_string)

with open('Output.html', 'a') as file:
    for row in cursor:
        file.write(str(row).replace("(", ",[").replace(")", "]").replace("'", ""))

with open("Output.html", "a") as text_file:
    text_file.write("    ]);\n")

    text_file.write("   var options = {\n")
    text_file.write("        title: 'Neuro Pass / Fail',\n")
    text_file.write("        is3D: true,\n")
    text_file.write("       slices: { 0:{color:'#26806E'}, 1:{color:'#dc1b06'} },\n")
    text_file.write("       legend: { position: 'right' }\n")
    text_file.write("    };\n")

    text_file.write("   var chart = new google.visualization.PieChart(document.getElementById('neuro_passfail'));\n")

    text_file.write("    chart.draw(data, options);\n")
    text_file.write("   }\n")
# Function end neuro Pass / Fail #
    ##############################################
    # Function begin Perivasc Pass/Fail #
    text_file.write("    function drawPerivascPassFail() { \n")
    text_file.write("      var data = google.visualization.arrayToDataTable([ \n")
    text_file.write("        ['Count', 'Score'] \n")

# SQL insert
cursor = connection.cursor()
query_string = "select ''''+'Pass'+'''',count(perivasc) \
from hlthscore \
where perivasc = 0 \
and valid = 1 \
UNION ALL \
select ''''+'Fail'+'''',count(perivasc) \
from hlthscore \
where perivasc = 1 \
and valid = 1 \
"
cursor.execute(query_string)

with open('Output.html', 'a') as file:
    for row in cursor:
        file.write(str(row).replace("(", ",[").replace(")", "]").replace("'", ""))

with open("Output.html", "a") as text_file:
    text_file.write("    ]);\n")

    text_file.write("   var options = {\n")
    text_file.write("        title: 'Perivasc Pass / Fail',\n")
    text_file.write("        is3D: true,\n")
    text_file.write("       slices: { 0:{color:'#26806E'}, 1:{color:'#dc1b06'} },\n")
    text_file.write("       legend: { position: 'right' }\n")
    text_file.write("    };\n")

    text_file.write("   var chart = new google.visualization.PieChart(document.getElementById('perivasc_passfail'));\n")

    text_file.write("    chart.draw(data, options);\n")
    text_file.write("   }\n")
# Function end psycho Pass / Fail #
    ##############################################
    # Function begin Psycho Pass/Fail #
    text_file.write("    function drawPsychoPassFail() { \n")
    text_file.write("      var data = google.visualization.arrayToDataTable([ \n")
    text_file.write("        ['Count', 'Score'] \n")

# SQL insert
cursor = connection.cursor()
query_string = "select ''''+'Pass'+'''',count(psycho) \
from hlthscore \
where psycho = 0 \
and valid = 1 \
UNION ALL \
select ''''+'Fail'+'''',count(psycho) \
from hlthscore \
where psycho = 1 \
and valid = 1 \
"
cursor.execute(query_string)

with open('Output.html', 'a') as file:
    for row in cursor:
        file.write(str(row).replace("(", ",[").replace(")", "]").replace("'", ""))

with open("Output.html", "a") as text_file:
    text_file.write("    ]);\n")

    text_file.write("   var options = {\n")
    text_file.write("        title: 'Psycho Pass / Fail',\n")
    text_file.write("        is3D: true,\n")
    text_file.write("       slices: { 0:{color:'#26806E'}, 1:{color:'#dc1b06'} },\n")
    text_file.write("       legend: { position: 'right' }\n")
    text_file.write("    };\n")

    text_file.write("   var chart = new google.visualization.PieChart(document.getElementById('psycho_passfail'));\n")

    text_file.write("    chart.draw(data, options);\n")
    text_file.write("   }\n")
# Function end Resp Pass / Fail #
    ##############################################
    # Function begin Resp Pass/Fail #
    text_file.write("    function drawRespPassFail() { \n")
    text_file.write("      var data = google.visualization.arrayToDataTable([ \n")
    text_file.write("        ['Count', 'Score'] \n")

# SQL insert
cursor = connection.cursor()
query_string = "select ''''+'Pass'+'''',count(resp) \
from hlthscore \
where resp = 0 \
and valid = 1 \
UNION ALL \
select ''''+'Fail'+'''',count(resp) \
from hlthscore \
where resp = 1 \
and valid = 1 \
"
cursor.execute(query_string)

with open('Output.html', 'a') as file:
    for row in cursor:
        file.write(str(row).replace("(", ",[").replace(")", "]").replace("'", ""))

with open("Output.html", "a") as text_file:
    text_file.write("    ]);\n")

    text_file.write("   var options = {\n")
    text_file.write("        title: 'Resp Pass / Fail',\n")
    text_file.write("        is3D: true,\n")
    text_file.write("       slices: { 0:{color:'#26806E'}, 1:{color:'#dc1b06'} },\n")
    text_file.write("       legend: { position: 'right' }\n")
    text_file.write("    };\n")

    text_file.write("   var chart = new google.visualization.PieChart(document.getElementById('resp_passfail'));\n")

    text_file.write("    chart.draw(data, options);\n")
    text_file.write("   }\n")
# Function end Resp Pass / Fail #
    # Function end Resp Pass / Fail #
    ##############################################
    # Function begin Safe Pass/Fail #
    text_file.write("    function drawSafePassFail() { \n")
    text_file.write("      var data = google.visualization.arrayToDataTable([ \n")
    text_file.write("        ['Count', 'Score'] \n")

# SQL insert
cursor = connection.cursor()
query_string = "select ''''+'Pass'+'''',count(safe) \
from hlthscore \
where safe = 0 \
and valid = 1 \
UNION ALL \
select ''''+'Fail'+'''',count(safe) \
from hlthscore \
where safe = 1 \
and valid = 1 \
"
cursor.execute(query_string)

with open('Output.html', 'a') as file:
    for row in cursor:
        file.write(str(row).replace("(", ",[").replace(")", "]").replace("'", ""))

with open("Output.html", "a") as text_file:
    text_file.write("    ]);\n")

    text_file.write("   var options = {\n")
    text_file.write("        title: 'Safe Pass / Fail',\n")
    text_file.write("        is3D: true,\n")
    text_file.write("       slices: { 0:{color:'#26806E'}, 1:{color:'#dc1b06'} },\n")
    text_file.write("       legend: { position: 'right' }\n")
    text_file.write("    };\n")

    text_file.write("   var chart = new google.visualization.PieChart(document.getElementById('safe_passfail'));\n")

    text_file.write("    chart.draw(data, options);\n")
    text_file.write("   }\n")
# Function end Safe Pass / Fail #
    # Function end Safe Pass / Fail #
    ##############################################
    # Function begin Skin Pass/Fail #
    text_file.write("    function drawSkinPassFail() { \n")
    text_file.write("      var data = google.visualization.arrayToDataTable([ \n")
    text_file.write("        ['Count', 'Score'] \n")

# SQL insert
cursor = connection.cursor()
query_string = "select ''''+'Pass'+'''',count(skin) \
from hlthscore \
where skin = 0 \
and valid = 1 \
UNION ALL \
select ''''+'Fail'+'''',count(skin) \
from hlthscore \
where skin = 1 \
and valid = 1 \
"
cursor.execute(query_string)

with open('Output.html', 'a') as file:
    for row in cursor:
        file.write(str(row).replace("(", ",[").replace(")", "]").replace("'", ""))

with open("Output.html", "a") as text_file:
    text_file.write("    ]);\n")

    text_file.write("   var options = {\n")
    text_file.write("        title: 'Skin Pass / Fail',\n")
    text_file.write("        is3D: true,\n")
    text_file.write("       slices: { 0:{color:'#26806E'}, 1:{color:'#dc1b06'} },\n")
    text_file.write("       legend: { position: 'right' }\n")
    text_file.write("    };\n")

    text_file.write("   var chart = new google.visualization.PieChart(document.getElementById('skin_passfail'));\n")

    text_file.write("    chart.draw(data, options);\n")
    text_file.write("   }\n")
# Function end Skin Pass / Fail #






with open("Output.html", "a") as text_file:
    text_file.write("    </script>\n")
    text_file.write("</head>\n")
    text_file.write("<body>\n")
    text_file.write("  <h3>Lab Checks</h3><br>\n")
    text_file.write("    <div class=\"row\">\n")
    text_file.write("      <div class=\"col-sm-4 panel panel-success\" id=\"bun_chart\" style=\"height: 70%\"></div>\n")
    text_file.write(
        "      <div class=\"col-sm-4 panel panel-success\" id=\"chloride_chart\" style=\"height: 70%\"></div>\n")
    text_file.write(
        "      <div class=\"col-sm-4 panel panel-success\" id=\"creatinine_chart\" style=\"height: 70%\"></div>\n")
    text_file.write("    </div>\n")
    text_file.write("    <div class=\"row\">\n")
    text_file.write(
        "      <div class=\"col-sm-4 panel panel-success\" id=\"hemoglobin_chart\" style=\"height: 70%\"></div>\n")
    text_file.write(
        "      <div class=\"col-sm-4 panel panel-success\" id=\"potassium_chart\" style=\"height: 70%\"></div>\n")
    text_file.write(
        "      <div class=\"col-sm-4 panel panel-success\" id=\"sodium_chart\" style=\"height: 70%\"></div>\n")
    text_file.write("    </div>\n")
    text_file.write("    <div class=\"row\">\n")
    text_file.write("      <div class=\"col-sm-4 panel panel-success\" id=\"wbc_chart\" style=\"height: 70%\"></div>\n")
    text_file.write("    </div>\n")
    text_file.write("  <h3>Vital and Nurse Checks</h3><br>\n")
    text_file.write("    <div class=\"row\">\n")
    text_file.write(
        "      <div class=\"col-sm-4 panel panel-success\" id=\"systol_chart\" style=\"height: 70%\"></div>\n")
    text_file.write(
        "      <div class=\"col-sm-4 panel panel-success\" id=\"diastolic_chart\" style=\"height: 70%\"></div>\n")
    text_file.write(
        "      <div class=\"col-sm-4 panel panel-success\" id=\"braden_chart\" style=\"height: 70%\"></div>\n")
    text_file.write("    </div>\n")
    text_file.write("    <div class=\"row\">\n")
    text_file.write(
        "      <div class=\"col-sm-4 panel panel-success\" id=\"heartrate_chart\" style=\"height: 70%\"></div>\n")
    text_file.write(
        "      <div class=\"col-sm-4 panel panel-success\" id=\"O2sat_chart\" style=\"height: 70%\"></div>\n")
    text_file.write(
        "      <div class=\"col-sm-4 panel panel-success\" id=\"resp_chart\" style=\"height: 70%\"></div>\n")
    text_file.write("    </div>\n")
    text_file.write("    <div class=\"row\">\n")
    text_file.write(
        "      <div class=\"col-sm-4 panel panel-success\" id=\"temp_chart\"     style=\"height: 70%\"></div>\n")
    text_file.write(
        "      <div class=\"col-sm-4 panel panel-success\" id=\"heartrhythm_chart\"     style=\"height: 70%\"></div>\n")
    text_file.write("    </div>\n")
    text_file.write("  <h3>Pass Fail Checks</h3><br>\n")
    text_file.write("    <div class=\"row\">\n")
    text_file.write(
        "      <div class=\"col-sm-4 panel panel-success\" id=\"cardiac_passfail\"      style=\"height: 70%\"></div>\n")
    text_file.write(
        "      <div class=\"col-sm-4 panel panel-success\" id=\"nutrition_passfail\" style=\"height: 70%\"></div>\n")
    text_file.write(
        "      <div class=\"col-sm-4 panel panel-success\" id=\"gastro_passfail\" style=\"height: 70%\"></div>\n")
    text_file.write("    </div>\n")
    text_file.write("    <div class=\"row\">\n")
    text_file.write(
        "      <div class=\"col-sm-4 panel panel-success\" id=\"genito_passfail\"  style=\"height: 70%\"></div>\n")
    text_file.write(
        "      <div class=\"col-sm-4 panel panel-success\" id=\"musculo_passfail\"   style=\"height: 70%\"></div>\n")
    text_file.write(
        "      <div class=\"col-sm-4 panel panel-success\" id=\"neuro_passfail\" style=\"height: 70%\"></div>\n")
    text_file.write("    </div>\n")

    text_file.write("    <div class=\"row\">\n")

    text_file.write(
    "      <div class=\"col-sm-4 panel panel-success\" id=\"perivasc_passfail\"  style=\"height: 70%\"></div>\n")
    text_file.write(
    "      <div class=\"col-sm-4 panel panel-success\" id=\"psycho_passfail\"   style=\"height: 70%\"></div>\n")
    text_file.write(
    "      <div class=\"col-sm-4 panel panel-success\" id=\"resp_passfail\" style=\"height: 70%\"></div>\n")
    text_file.write("    </div>\n")

    text_file.write("    <div class=\"row\">\n")
    text_file.write(
        "      <div class=\"col-sm-4 panel panel-success\" id=\"safe_passfail\" style=\"height: 100%\"></div>\n")
    text_file.write(
        "      <div class=\"col-sm-4 panel panel-success\" id=\"skin_passfail\" style=\"height: 100%\"></div>\n")
    text_file.write("    </div>\n")

    text_file.write("  </body>\n")
    text_file.write("</html>\n")
