
-- Use the script below to remove any non-numeric data from the labs and vitals, and delete blank or NULL
-- message values.


-- Drop function if it exists
IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'dbo.fnStrip') 
    AND xtype IN (N'FN', N'IF', N'TF')
)
    DROP FUNCTION dbo.fnStrip
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- Create fnStrip function

CREATE FUNCTION [dbo].[fnStrip]
(@String        VARCHAR(8000)
)
RETURNS VARCHAR(8000)
AS
     BEGIN
         DECLARE @i INT;
         DECLARE @c VARCHAR(1);
	    DECLARE @PatternToKeep varchar(100);
         DECLARE @Result VARCHAR(8000);

         SET @Result = '';
	    SET @PatternToKeep='[0-9]'
         SET @i = 1;
         WHILE(@i <= LEN(@String))
             SELECT @c = SUBSTRING(@String, @i, 1),
                    @Result = @Result+CASE
                                          WHEN @c LIKE @PatternToKeep
                                          THEN @c
										  WHEN @c like '.'
										  THEN @c
                                          ELSE ''
                                      END,
                    @i = @i + 1;
         RETURN(@Result);
     END;

GO

UPDATE __lab_final
  SET
      msgval = dbo.fnStrip(msgval);

UPDATE __vital_final
  SET
      msgval = dbo.fnStrip(msgval);

-- This script will delete any NULL rows from the lab qa view
DELETE FROM __vital_final
WHERE msgVal = 'NULL';

-- This script will delete any blank values from the vital qa view
DELETE FROM __vital_final
WHERE msgVal = '';

-- This script will delete any NULL rows from the lab qa view
DELETE FROM __lab_final
WHERE msgVal = 'NULL';

-- This script will delete any blank values from the lab qa view
DELETE FROM __lab_final
WHERE msgVal = '';

-- This script will delete any NULL rows from the lab qa view
DELETE FROM __vital_final
WHERE msgVal = '.';

-- This script will delete any blank values from the lab qa view
DELETE FROM __lab_final
WHERE msgVal = '.';