-- This script will delete any visits with discharge dates before the admit dates

DECLARE @negativeVisits TABLE(visitNum VARCHAR(MAX));
INSERT INTO @negativeVisits
       SELECT visitNum
       FROM __adt_final
       WHERE admitdt > dischdt;
DELETE FROM __adt_final
WHERE visitnum IN
(
    SELECT visitnum
    FROM @negativeVisits
);
DELETE FROM __lab_final
WHERE visitnum IN
(
    SELECT visitnum
    FROM @negativeVisits
);
DELETE FROM __nurse_final
WHERE visitnum IN
(
    SELECT visitnum
    FROM @negativeVisits
);
DELETE FROM __vital_final
WHERE visitnum IN
(
    SELECT visitnum
    FROM @negativeVisits
);