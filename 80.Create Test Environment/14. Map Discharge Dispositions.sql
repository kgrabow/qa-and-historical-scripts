/*

If the customer is sending discharge dispositions to us in a very verbose manner OR just a bunch of codes,
the graphs in Analytics will look ugly and/or we may not calculate mortality correctly
so look at the Peratrend data and run some of the bits below

If discharge disp data not provided--  may have to use some variety of sql from below

*/

/*
 
with cteGetDisps as (select * from x.dbo.map_discharge_disp_descr)
merge into map_discharge_disp_descr as target
using cteGetDisps as source
on source.code = target.code
when not matched then
insert (code, descr)
values (source.code, source.descr);

select * from map_discharge_disp_descr
 */

with cteGetDisps as (
select distinct disch_disp from perahealthdata.dbo.visit where datalength(rtrim(ltrim(disch_disp)))>0 
)
,cteRow as (select row_number() over (order by disch_disp) as rownum, disch_disp from cteGetDisps)


merge into map_discharge_disp_descr as target
using cteRow as source
on source.disch_disp = target.code
when not matched then
insert (code, descr)
values (source.disch_disp, source.disch_disp);


with cteX as (select * from map_discharge_disp_descr where code like '%Hospice%' and code = descr)
--select * from ctex;
merge into map_discharge_disp_descr as target
using ctex as source 
on source.row_id = target.row_id
when matched then update 
set descr = 'Hospice';

with cteX as (select * from map_discharge_disp_descr where code like '%PSYCHIATRIC%' and code = descr)
--select * from ctex;

merge into map_discharge_disp_descr as target
using ctex as source 
on source.row_id = target.row_id
when matched then update 
set descr = 'Psychiatric';


select * from map_discharge_disp_descr;


with cteX as (select * from map_discharge_disp_descr where code like 'Disch/Trans %' and code = descr)
merge into map_discharge_disp_descr as target
using ctex as source 
on source.row_id = target.row_id
when matched then update 
set descr = REPLACE(source.descr,'Disch/Trans ','');

with cteX as (select * from map_discharge_disp_descr where code like 'Disch/Trans %' and code = descr)
merge into map_discharge_disp_descr as target
using ctex as source 
on source.row_id = target.row_id
when matched then update 
set descr = REPLACE(source.descr,'Disch/Trans to','');


with cteX as (select * from map_discharge_disp_descr where code like 'Discharge to %' and code = descr)
merge into map_discharge_disp_descr as target
using ctex as source 
on source.row_id = target.row_id
when matched then update 
set descr = REPLACE(source.descr,'Discharge to ','');

with cteX as (select * from map_discharge_disp_descr where code like 'D/C %' and code = descr)
merge into map_discharge_disp_descr as target
using ctex as source 
on source.row_id = target.row_id
when matched then update 
set descr = REPLACE(source.descr,'D/C ','');


with cteX as (select * from map_discharge_disp_descr where code like '%Home%' and code = descr)
--select * from ctex;

merge into map_discharge_disp_descr as target
using ctex as source 
on source.row_id = target.row_id
when matched then update 
set descr = 'Home';

with cteX as (select * from map_discharge_disp_descr where code like '%SNF%' and code = descr)
--select * from ctex;

merge into map_discharge_disp_descr as target
using ctex as source 
on source.row_id = target.row_id
when matched then update 
set descr = 'SNF';

with cteX as (select * from map_discharge_disp_descr where code like '%Expired%' and code = descr)
--select * from ctex;

merge into map_discharge_disp_descr as target
using ctex as source 
on source.row_id = target.row_id
when matched then update 
set descr = 'Expired';


with cteX as (select * from map_discharge_disp_descr where code like '%Coroner%' and code = descr)
--select * from ctex;

merge into map_discharge_disp_descr as target
using ctex as source 
on source.row_id = target.row_id
when matched then update 
set descr = 'Expired';

with cteX as (select * from map_discharge_disp_descr where code like '%Deceased%' and code = descr)
--select * from ctex;

merge into map_discharge_disp_descr as target
using ctex as source 
on source.row_id = target.row_id
when matched then update 
set descr = 'Expired';


--handful of codes from customer
with cteX as (
select '81' as code, 'HOME' as descr union all
select 'I51' as code, 'HOSPICE' as descr union all
select 'I62' as code, 'REHAB' as descr union all
select 'IE' as code, 'SNF' as descr union all
select 'IH' as code, 'HOME' as descr union all
select 'IL' as code, 'AMA' as descr union all
select 'IX' as code, 'EXPIRED' as descr 
)
--select * from ctex;

merge into map_discharge_disp_descr as target
using ctex as source 
on source.code = target.code
when matched then update 
set target.descr = source.descr;


--------------------------------

