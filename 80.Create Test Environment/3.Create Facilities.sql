USE [perahealthdata]
GO
/****** Object:  Table [dbo].[facility]    Script Date: 1/16/2017 10:11:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[facility](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[facility_num] [varchar](45) NOT NULL,
	[descr] [varchar](50) NOT NULL,
	[created_dt] [datetime] NOT NULL CONSTRAINT [DF_facility_created_dt]  DEFAULT (getutcdate()),
	[updated_dt] [datetime] NOT NULL CONSTRAINT [DF_facility_updated_dt]  DEFAULT (getutcdate()),
	[display_order] [int] NULL,
	[primary_type] [smallint] NULL,
	[secondary_type] [smallint] NULL,
	[utc_time_zone_name] [nvarchar](99) NULL,
	[created_dt_back] [datetime] NULL,
	[updated_dt_back] [datetime] NULL,
 CONSTRAINT [facility_PK] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[facility] ON 

GO
INSERT [dbo].[facility] ([id], [facility_num], [descr], [created_dt], [updated_dt], [display_order], [primary_type], [secondary_type], [utc_time_zone_name], [created_dt_back], [updated_dt_back]) VALUES (1, N'Roswell', N'Roswell', CAST(N'2016-03-23 10:23:13.710' AS DateTime), CAST(N'2016-05-12 08:49:15.307' AS DateTime), 10, NULL, NULL, N'America/New_York', CAST(N'2016-03-23 10:23:13.710' AS DateTime), CAST(N'2016-05-12 08:49:15.307' AS DateTime))
GO
INSERT [dbo].[facility] ([id], [facility_num], [descr], [created_dt], [updated_dt], [display_order], [primary_type], [secondary_type], [utc_time_zone_name], [created_dt_back], [updated_dt_back]) VALUES (2, N'Los Angeles', N'Los Angeles', CAST(N'2016-04-07 14:39:07.990' AS DateTime), CAST(N'2016-04-14 10:35:45.013' AS DateTime), 2, NULL, NULL, N'America/Los_Angeles', CAST(N'2016-04-07 14:39:07.990' AS DateTime), CAST(N'2016-04-14 10:35:45.013' AS DateTime))
GO
INSERT [dbo].[facility] ([id], [facility_num], [descr], [created_dt], [updated_dt], [display_order], [primary_type], [secondary_type], [utc_time_zone_name], [created_dt_back], [updated_dt_back]) VALUES (3, N'Greenville', N'Greenville', CAST(N'2016-04-07 14:39:21.617' AS DateTime), CAST(N'2016-05-25 11:14:38.460' AS DateTime), 1, NULL, NULL, N'America/New_York', CAST(N'2016-04-07 14:39:21.617' AS DateTime), CAST(N'2016-05-25 11:14:38.460' AS DateTime))
GO
INSERT [dbo].[facility] ([id], [facility_num], [descr], [created_dt], [updated_dt], [display_order], [primary_type], [secondary_type], [utc_time_zone_name], [created_dt_back], [updated_dt_back]) VALUES (4, N'Anderson', N'Anderson', CAST(N'2016-04-08 08:55:36.777' AS DateTime), CAST(N'2016-04-14 10:35:56.400' AS DateTime), 3, NULL, NULL, N'America/New_York', CAST(N'2016-04-08 08:55:36.777' AS DateTime), CAST(N'2016-04-14 10:35:56.400' AS DateTime))
GO
INSERT [dbo].[facility] ([id], [facility_num], [descr], [created_dt], [updated_dt], [display_order], [primary_type], [secondary_type], [utc_time_zone_name], [created_dt_back], [updated_dt_back]) VALUES (5, N'Tampa', N'Tampa', CAST(N'2016-04-08 08:56:15.127' AS DateTime), CAST(N'2016-04-14 10:36:07.040' AS DateTime), 4, NULL, NULL, N'America/New_York', CAST(N'2016-04-08 08:56:15.127' AS DateTime), CAST(N'2016-04-14 10:36:07.040' AS DateTime))
GO
INSERT [dbo].[facility] ([id], [facility_num], [descr], [created_dt], [updated_dt], [display_order], [primary_type], [secondary_type], [utc_time_zone_name], [created_dt_back], [updated_dt_back]) VALUES (6, N'Pequannock', N'Pequannock', CAST(N'2016-04-08 08:56:46.370' AS DateTime), CAST(N'2016-04-14 10:36:30.493' AS DateTime), 5, NULL, NULL, N'America/Phoenix', CAST(N'2016-04-08 08:56:46.370' AS DateTime), CAST(N'2016-04-14 10:36:30.493' AS DateTime))
GO
INSERT [dbo].[facility] ([id], [facility_num], [descr], [created_dt], [updated_dt], [display_order], [primary_type], [secondary_type], [utc_time_zone_name], [created_dt_back], [updated_dt_back]) VALUES (7, N'Central', N'Central', CAST(N'2016-04-08 08:57:35.770' AS DateTime), CAST(N'2016-04-14 10:36:42.843' AS DateTime), 6, NULL, NULL, N'America/Denver', CAST(N'2016-04-08 08:57:35.770' AS DateTime), CAST(N'2016-04-14 10:36:42.843' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[facility] OFF
GO
ALTER TABLE [dbo].[facility]  WITH CHECK ADD  CONSTRAINT [facility_HOSPITAL_TYPE_FK] FOREIGN KEY([primary_type])
REFERENCES [dbo].[HOSPITAL_TYPE] ([ht_code])
GO
ALTER TABLE [dbo].[facility] CHECK CONSTRAINT [facility_HOSPITAL_TYPE_FK]
GO
ALTER TABLE [dbo].[facility]  WITH CHECK ADD  CONSTRAINT [facility_HOSPITAL_TYPE2_FK] FOREIGN KEY([secondary_type])
REFERENCES [dbo].[HOSPITAL_TYPE] ([ht_code])
GO
ALTER TABLE [dbo].[facility] CHECK CONSTRAINT [facility_HOSPITAL_TYPE2_FK]
GO
