/***
this is another "it depends" script
run the pieces depending on the source data
**/

--map_ptrule_warninglevel
--this is to map the various customer rules to the 3 analytics severity levels
--sample customer data from ptrule:
--SEVERITY     DESC                        PTRULE_ID
--1            Adult Somethingish          7
--4            Score Below 30              8
--14           Adult Very High             9

--sample mapping data
--row_id    ptrule_id    warninglevel
--1         7            10
--2         8            30
--3         9            30

--running the check at the end to make sure it is all mapped gives you
--PTRuleDescr           pa_warning_level  pa_warning_descr
--Adult Somethingish    10                     Medium
--Score Below 30        30                     Very High
--Adult Very High       30                     Very High

--per Stacey:
--score below x is very high
--score drop in 12 hours is high
--score drop in 24 hours is medium


DECLARE @medium_value INT;
SET @medium_value=(select pa_warning_level from warning_levels where pa_warning_descr = 'Medium');

DECLARE @high_value INT;
SET @high_value=(select pa_warning_level from warning_levels where pa_warning_descr = 'High');

DECLARE @veryhigh_value INT;
SET @veryhigh_value=(select pa_warning_level from warning_levels where pa_warning_descr = 'Very High');



with cteMedium as (
  select r.id as ptrule_id, @medium_value as pa_warning_level from ptrule r where descr like '%24 hour%'
)
merge into map_ptrule_warninglevel as target
using cteMedium as source
on source.ptrule_id = target.ptrule_id
when matched then update
 set pa_warninglevel = source.pa_warning_level
when not matched then insert
 (ptrule_id, pa_warninglevel) values (source.ptrule_id, pa_warning_level);

with cteHigh as (
  select r.id as ptrule_id, @high_value as pa_warning_level from ptrule r where descr like '%12 hour%'
)
merge into map_ptrule_warninglevel as target
using cteHigh as source
on source.ptrule_id = target.ptrule_id
when matched then update
 set pa_warninglevel = source.pa_warning_level
when not matched then insert
 (ptrule_id, pa_warninglevel) values (source.ptrule_id, pa_warning_level);


with cteHigh as (
  select r.id as ptrule_id, @high_value as pa_warning_level from ptrule r where descr like '%High%'
)
merge into map_ptrule_warninglevel as target
using cteHigh as source
on source.ptrule_id = target.ptrule_id
when matched then update
 set pa_warninglevel = source.pa_warning_level
when not matched then insert
 (ptrule_id, pa_warninglevel) values (source.ptrule_id, pa_warning_level);


with cteMedium as (
  select r.id as ptrule_id, @medium_value as pa_warning_level from ptrule r where descr like '%Medium%'
)
merge into map_ptrule_warninglevel as target
using cteMedium as source
on source.ptrule_id = target.ptrule_id
when matched then update
 set pa_warninglevel = source.pa_warning_level
when not matched then insert
 (ptrule_id, pa_warninglevel) values (source.ptrule_id, pa_warning_level);
 
--this should override any very highs that were caught up in the initial high selection 
with cteVeryHigh as (
  select r.id as ptrule_id, @veryhigh_value as pa_warning_level from ptrule r where descr like '%Very High%'
)
merge into map_ptrule_warninglevel as target
using cteVeryHigh as source
on source.ptrule_id = target.ptrule_id
when matched then update
 set pa_warninglevel = source.pa_warning_level
when not matched then insert
 (ptrule_id, pa_warninglevel) values (source.ptrule_id, pa_warning_level);
 
--now get the less thans/ below
 with cteVeryHigh as (
  select r.id as ptrule_id, @veryhigh_value as pa_warning_level from ptrule r where 
    descr like '%less than%' or descr like '%below%'
)
merge into map_ptrule_warninglevel as target
using cteVeryHigh as source
on source.ptrule_id = target.ptrule_id
when matched then update
 set pa_warninglevel = source.pa_warning_level
when not matched then insert
 (ptrule_id, pa_warninglevel) values (source.ptrule_id, pa_warning_level);
 
  --if any null values for warning level and warning descr, manually correct the data
 
select
 r.id as ptrule_id, r.descr as ptrule_descr, pa_warning_level ,
 wl.pa_warning_descr from 
 ptrule r left join
 map_ptrule_warninglevel mpw on mpw.ptrule_id = r.id
left join warning_levels wl on mpw.pa_warninglevel = wl.pa_warning_level
order by pa_warning_level desc


 