USE perahealthdata;
GO

ALTER TABLE hlthscorexlatedata ALTER COLUMN heart_rhy VARCHAR(MAX);

ALTER TABLE hlthscorexlatedata ALTER COLUMN cardiac VARCHAR(MAX);

ALTER TABLE hlthscorexlatedata ALTER COLUMN food VARCHAR(MAX);

ALTER TABLE hlthscorexlatedata ALTER COLUMN gastro VARCHAR(MAX);

ALTER TABLE hlthscorexlatedata ALTER COLUMN genito VARCHAR(MAX);

ALTER TABLE hlthscorexlatedata ALTER COLUMN musculo VARCHAR(MAX);

ALTER TABLE hlthscorexlatedata ALTER COLUMN neuro VARCHAR(MAX);

ALTER TABLE hlthscorexlatedata ALTER COLUMN perivasc VARCHAR(MAX);

ALTER TABLE hlthscorexlatedata ALTER COLUMN psycho VARCHAR(MAX);

ALTER TABLE hlthscorexlatedata ALTER COLUMN resp VARCHAR(MAX);

ALTER TABLE hlthscorexlatedata ALTER COLUMN safe VARCHAR(MAX);

ALTER TABLE hlthscorexlatedata ALTER COLUMN skin VARCHAR(MAX);
GO