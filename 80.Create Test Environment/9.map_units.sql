use perahealthdata

update unit
set utc_code = 'E'
where descr like 'ED%'

update unit
set utc_code = 'R'
where descr like 'Med%'

update unit
set utc_code = 'N'
where descr like 'ICU%'

update unit
set utc_code = 'P'
where descr like 'Unit%'

update unit
set active_ind=1

