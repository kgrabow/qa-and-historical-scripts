SET IDENTITY_INSERT [dbo].[ptrule] ON

GO
INSERT [dbo].[ptrule] ([id], [descr], [active_ind], [alert_duration], [severity], [display_order], [created_dt], [updated_dt], [ptrulecategory_id]) VALUES (4, N'Adult Drop 30% in 12 hours', 1, 24, 4, NULL, CAST(0x0000A5CC00FC5001 AS DateTime), CAST(0x0000A5CC00FC5001 AS DateTime), 1)
GO
INSERT [dbo].[ptrule] ([id], [descr], [active_ind], [alert_duration], [severity], [display_order], [created_dt], [updated_dt], [ptrulecategory_id]) VALUES (5, N'Ped Drop 30% in 24 hours', 1, 24, 4, NULL, CAST(0x0000A5CC00FE5239 AS DateTime), CAST(0x0000A5CC00FE5239 AS DateTime), 1)
GO
INSERT [dbo].[ptrule] ([id], [descr], [active_ind], [alert_duration], [severity], [display_order], [created_dt], [updated_dt], [ptrulecategory_id]) VALUES (6, N'Adult Drop 35% in 12 hours', 1, 24, 4, NULL, CAST(0x0000A5CC00FFD246 AS DateTime), CAST(0x0000A5CC00FFD246 AS DateTime), 1)
GO
INSERT [dbo].[ptrule] ([id], [descr], [active_ind], [alert_duration], [severity], [display_order], [created_dt], [updated_dt], [ptrulecategory_id]) VALUES (7, N'Adult Drop 40% in 12 hours', 1, 24, 4, NULL, CAST(0x0000A5CC01018025 AS DateTime), CAST(0x0000A5CC01018025 AS DateTime), 1)
GO
INSERT [dbo].[ptrule] ([id], [descr], [active_ind], [alert_duration], [severity], [display_order], [created_dt], [updated_dt], [ptrulecategory_id]) VALUES (8, N'Adult Drop 50% in 12 hours', 1, 24, 4, NULL, CAST(0x0000A5CC0101AE5B AS DateTime), CAST(0x0000A5CC0101AE5B AS DateTime), 1)
GO
INSERT [dbo].[ptrule] ([id], [descr], [active_ind], [alert_duration], [severity], [display_order], [created_dt], [updated_dt], [ptrulecategory_id]) VALUES (9, N'Ped Drop 35% in 24 hours', 1, 24, 4, NULL, CAST(0x0000A5CC0102586A AS DateTime), CAST(0x0000A5CC0102586A AS DateTime), 1)
GO
INSERT [dbo].[ptrule] ([id], [descr], [active_ind], [alert_duration], [severity], [display_order], [created_dt], [updated_dt], [ptrulecategory_id]) VALUES (10, N'Ped Drop 40% in 24 hours', 1, 24, 4, NULL, CAST(0x0000A5CC010281AE AS DateTime), CAST(0x0000A5CC010281AE AS DateTime), 1)
GO
INSERT [dbo].[ptrule] ([id], [descr], [active_ind], [alert_duration], [severity], [display_order], [created_dt], [updated_dt], [ptrulecategory_id]) VALUES (11, N'Ped Drop 45% in 24 hours', 1, 24, 4, NULL, CAST(0x0000A5CC01029A76 AS DateTime), CAST(0x0000A5CC01029A76 AS DateTime), 1)
GO
INSERT [dbo].[ptrule] ([id], [descr], [active_ind], [alert_duration], [severity], [display_order], [created_dt], [updated_dt], [ptrulecategory_id]) VALUES (12, N'Ped Drop 50% in 24 hours', 1, 24, 4, NULL, CAST(0x0000A5CC0102AE27 AS DateTime), CAST(0x0000A5CC0102AE27 AS DateTime), 1)
GO
INSERT [dbo].[ptrule] ([id], [descr], [active_ind], [alert_duration], [severity], [display_order], [created_dt], [updated_dt], [ptrulecategory_id]) VALUES (13, N'Adult Score less than 15', 1, 24, 4, NULL, CAST(0x0000A5CC0106FE7A AS DateTime), CAST(0x0000A5CC0106FE7A AS DateTime), 1)
GO
INSERT [dbo].[ptrule] ([id], [descr], [active_ind], [alert_duration], [severity], [display_order], [created_dt], [updated_dt], [ptrulecategory_id]) VALUES (14, N'Adult Score less than 20', 1, 24, 4, NULL, CAST(0x0000A5CC01072D31 AS DateTime), CAST(0x0000A5CC01072D31 AS DateTime), 1)
GO
INSERT [dbo].[ptrule] ([id], [descr], [active_ind], [alert_duration], [severity], [display_order], [created_dt], [updated_dt], [ptrulecategory_id]) VALUES (15, N'Adult Score less than 30', 1, 24, 4, NULL, CAST(0x0000A5CC01077AE2 AS DateTime), CAST(0x0000A5CC01077AE2 AS DateTime), 1)
GO
INSERT [dbo].[ptrule] ([id], [descr], [active_ind], [alert_duration], [severity], [display_order], [created_dt], [updated_dt], [ptrulecategory_id]) VALUES (16, N'Adult Score less than 35', 1, 24, 4, NULL, CAST(0x0000A5CC0107C769 AS DateTime), CAST(0x0000A5CC0107C769 AS DateTime), 1)
GO
INSERT [dbo].[ptrule] ([id], [descr], [active_ind], [alert_duration], [severity], [display_order], [created_dt], [updated_dt], [ptrulecategory_id]) VALUES (17, N'Adult Drop 30% in 24 hours', 1, 24, 4, NULL, CAST(0x0000A5CC0109D515 AS DateTime), CAST(0x0000A5CC0109D515 AS DateTime), 1)
GO
INSERT [dbo].[ptrule] ([id], [descr], [active_ind], [alert_duration], [severity], [display_order], [created_dt], [updated_dt], [ptrulecategory_id]) VALUES (18, N'Adult Drop 35% in 24 hours', 1, 24, 4, NULL, CAST(0x0000A5CC0109E758 AS DateTime), CAST(0x0000A5CC0109E758 AS DateTime), 1)
GO
INSERT [dbo].[ptrule] ([id], [descr], [active_ind], [alert_duration], [severity], [display_order], [created_dt], [updated_dt], [ptrulecategory_id]) VALUES (19, N'Adult Drop 40% in 24 hours', 1, 24, 4, NULL, CAST(0x0000A5CC010A003A AS DateTime), CAST(0x0000A5CC010A003A AS DateTime), 1)
GO
INSERT [dbo].[ptrule] ([id], [descr], [active_ind], [alert_duration], [severity], [display_order], [created_dt], [updated_dt], [ptrulecategory_id]) VALUES (20, N'Adult Drop 45% in 24 hours', 1, 24, 4, NULL, CAST(0x0000A5CC010A1185 AS DateTime), CAST(0x0000A5CC010A1185 AS DateTime), 1)
GO
INSERT [dbo].[ptrule] ([id], [descr], [active_ind], [alert_duration], [severity], [display_order], [created_dt], [updated_dt], [ptrulecategory_id]) VALUES (21, N'Adult Drop 50% in 24 hours', 1, 24, 4, NULL, CAST(0x0000A5CC010A22B4 AS DateTime), CAST(0x0000A5CC010A22B4 AS DateTime), 1)
GO
INSERT [dbo].[ptrule] ([id], [descr], [active_ind], [alert_duration], [severity], [display_order], [created_dt], [updated_dt], [ptrulecategory_id]) VALUES (22, N'Ped Drop 30% in 12 hours', 1, 24, 4, NULL, CAST(0x0000A5CC010BC3E5 AS DateTime), CAST(0x0000A5CC010BC3E5 AS DateTime), 1)
GO
INSERT [dbo].[ptrule] ([id], [descr], [active_ind], [alert_duration], [severity], [display_order], [created_dt], [updated_dt], [ptrulecategory_id]) VALUES (23, N'Ped Drop 35% in 12 hours', 1, 24, 4, NULL, CAST(0x0000A5CC010BE2A0 AS DateTime), CAST(0x0000A5CC010BE2A0 AS DateTime), 1)
GO
INSERT [dbo].[ptrule] ([id], [descr], [active_ind], [alert_duration], [severity], [display_order], [created_dt], [updated_dt], [ptrulecategory_id]) VALUES (24, N'Ped Drop 40% in 12 hours', 1, 24, 4, NULL, CAST(0x0000A5CC010BF579 AS DateTime), CAST(0x0000A5CC010BF579 AS DateTime), 1)
GO
INSERT [dbo].[ptrule] ([id], [descr], [active_ind], [alert_duration], [severity], [display_order], [created_dt], [updated_dt], [ptrulecategory_id]) VALUES (25, N'Ped Drop 45% in 12 hours', 1, 24, 4, NULL, CAST(0x0000A5CC010D2DDE AS DateTime), CAST(0x0000A5CC010D2DDE AS DateTime), 1)
GO
INSERT [dbo].[ptrule] ([id], [descr], [active_ind], [alert_duration], [severity], [display_order], [created_dt], [updated_dt], [ptrulecategory_id]) VALUES (26, N'Ped Drop 50% in 12 hours', 1, 24, 4, NULL, CAST(0x0000A5CC010D42C0 AS DateTime), CAST(0x0000A5CC010D42C0 AS DateTime), 1)
GO
INSERT [dbo].[ptrule] ([id], [descr], [active_ind], [alert_duration], [severity], [display_order], [created_dt], [updated_dt], [ptrulecategory_id]) VALUES (27, N'Ped Score less than 15', 1, 24, 4, NULL, CAST(0x0000A5CC010D6002 AS DateTime), CAST(0x0000A5CC010D6002 AS DateTime), 1)
GO
INSERT [dbo].[ptrule] ([id], [descr], [active_ind], [alert_duration], [severity], [display_order], [created_dt], [updated_dt], [ptrulecategory_id]) VALUES (28, N'Ped Score less than 20', 1, 24, 4, NULL, CAST(0x0000A5CC010D70DE AS DateTime), CAST(0x0000A5CC010D70DE AS DateTime), 1)
GO
INSERT [dbo].[ptrule] ([id], [descr], [active_ind], [alert_duration], [severity], [display_order], [created_dt], [updated_dt], [ptrulecategory_id]) VALUES (29, N'Ped Score less than 30', 1, 24, 4, NULL, CAST(0x0000A5CC010D8599 AS DateTime), CAST(0x0000A5CC010D8599 AS DateTime), 1)
GO
INSERT [dbo].[ptrule] ([id], [descr], [active_ind], [alert_duration], [severity], [display_order], [created_dt], [updated_dt], [ptrulecategory_id]) VALUES (30, N'Ped Score less than 35', 1, 24, 4, NULL, CAST(0x0000A5CC010D984C AS DateTime), CAST(0x0000A5CC010D984C AS DateTime), 1)
GO
INSERT [dbo].[ptrule] ([id], [descr], [active_ind], [alert_duration], [severity], [display_order], [created_dt], [updated_dt], [ptrulecategory_id]) VALUES (31, N'Adult Drop 45% in 12 hours', 1, 24, 4, NULL, CAST(0x0000A5CC0111F1B9 AS DateTime), CAST(0x0000A5CC0111F1B9 AS DateTime), 1)
GO
SET IDENTITY_INSERT [dbo].[ptrule] OFF
GO
