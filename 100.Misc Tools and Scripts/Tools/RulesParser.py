# This script will take the Rules.txt file generated from peraserver.rules and strip out the
# modes that the historical processing tool doesn't like.
import json

with open('RulesJohn.txt') as rules_file:
    data = json.load(rules_file)
    for element in data:
        del element['ruleSetToDeleteInServer']
        del element['populationSetToDeleteInServer']
        del element['rulesSet']['rulestmt'][0]['listRuleGroupWithThisStm']
        del element['ruleCategory']

with open('RulesJohnOut.txt','w') as data_file:
    json.dump(data, data_file)