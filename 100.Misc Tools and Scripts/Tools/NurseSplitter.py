__author__ = 'Kevin'

import pypyodbc
import csv

connection = pypyodbc.connect('Driver={Sql Server};'
                              'Server=localhost;'
                              'Database=perahealthinput;'
                              'Trusted_Connection=True'
                              )

cursor = connection.cursor()

query_string = "SELECT * FROM nurse_historical_extract_qa order by mrn,msgDt"
cursor.execute(query_string)

with open('nurse.txt', 'w', newline='') as tsv:
    tsv.write("fac\tmrn\tVisitNum\tmsgVar\tmsgVal\tmsgOrigVal\trecordedDt\tmsgDt\r\n")

    for row in cursor:
        fac = str(row[0])
        mrn = str(row[1])
        visitNum = str(row[2])
        msgVar = str(row[3])
        msgVal = str(row[4])
        msgOrigVal = str(row[5])
        recordedDt = str(row[6])
        msgDt = str(row[7])

        if msgVal.count(';') == 0:
            tsv.write(
                fac + '\t' + mrn + '\t' + visitNum + '\t' + msgVar + '\t' + msgVal + '\t' + msgOrigVal + '\t' + recordedDt + '\t' + msgDt + '\r\n')
        else:
            b = msgVal.split(";")
            for c in range(0, msgVal.count(';')+1):
                tsv.write(fac + '\t' + mrn + '\t' + visitNum + '\t' + msgVar + '\t' + b[
                    c] + '\t' + msgOrigVal + '\t' + recordedDt + '\t' + msgDt + '\r\n')

connection.close()
