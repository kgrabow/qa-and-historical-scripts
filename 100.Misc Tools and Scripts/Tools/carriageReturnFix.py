# This script will take the Rules.txt file generated from peraserver.rules and strip out the
# modes that the historical processing tool doesn't like.

with open('input.txt') as infile:
    with open('output.txt',"w") as outfile:
        for line in infile:
            line=line.replace(" ","\n\r")
            outfile.write(line)

