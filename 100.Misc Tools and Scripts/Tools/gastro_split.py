__author__ = 'Kevin'

import pypyodbc
import csv

connection = pypyodbc.connect('Driver={Sql Server};'
                              'Server=localhost;'
                              'Database=perahealthdata;'
                              'Trusted_Connection=True'
                              )

cursor = connection.cursor()

query_string = "SELECT h.score_dt,hx.gastro \
FROM hlthscorexlatedata hx\
 join hlthscore h on h.id = hx.hlthscore_id \
 where h.valid = 1 \
 and hx.gastro is not null "

cursor.execute(query_string)

with open('gastro.txt', 'w', newline='') as tsv:
    tsv.write("score_dt\gastro\r\n")

    for row in cursor:
        score_dt = str(row[0])
        gastro = str(row[1])


        if gastro.count(';') == 0:
            tsv.write(
                score_dt + '\t' + gastro + '\r\n')
        else:
            b = gastro.split(";")
            for c in range(0, gastro.count(';')+1):
                tsv.write(score_dt + '\t' + gastro +'\r\n')

connection.close()
