import pypyodbc


bun = "BUN"
chloride = "Chloride"
creatinine = "CREATININE"
hgb = "HGB"
potassium = "K"
sodium = "NA"
wbc = "WBC Count"

braden = "braden score"
diastolic = "Diastolic BP"
systolic = "Systolic BP"
resp = "RESPIRATIONS"
temp = "Temperature"
heartrate = "PULSE"
pulseox = "PULSE OXIMETRY"


connection = pypyodbc.connect('Driver={Sql Server};'
                              'Server=localhost\SQLEXPRESS01;'
                              'Database=Yale;'
                     

                                'Trusted_Connection=True'
                              )

with open("Output.html", "w") as text_file:
    text_file.write("<html>\n")
    text_file.write("  <head>\n")
    text_file.write("  <h2>Historical Extract Files QA Checks</h2><br>\n")

    text_file.write("	<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\" integrity=\"sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u\" crossorigin=\"anonymous\"> \n")
    text_file.write("	<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css\" integrity=\"sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp\" crossorigin=\"anonymous\"> \n")
    text_file.write("	<script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\" integrity=\"sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa\" crossorigin=\"anonymous\"></script> \n")

    
    text_file.write("    <script type=\"text/javascript\" src=\"https://www.gstatic.com/charts/loader.js\"></script>\n")
    text_file.write("    <script type=\"text/javascript\">\n")
    text_file.write("      google.charts.load('current', {'packages':['corechart','table']});\n")
    text_file.write("     google.charts.setOnLoadCallback(drawBradenChart);\n")
    text_file.write("     google.charts.setOnLoadCallback(drawSystolicChart);\n")
    text_file.write("     google.charts.setOnLoadCallback(drawDiastolicChart);\n")

    text_file.write("      google.charts.setOnLoadCallback(drawBUNChart);\n")
    text_file.write("      google.charts.setOnLoadCallback(drawChlorideChart);\n")
    text_file.write("      google.charts.setOnLoadCallback(drawCreatinineChart);\n")

    text_file.write("      google.charts.setOnLoadCallback(drawHeartRateChart);\n")
    text_file.write("      google.charts.setOnLoadCallback(drawHemoglobinChart);\n")
    text_file.write("      google.charts.setOnLoadCallback(drawO2satChart);\n")

    text_file.write("      google.charts.setOnLoadCallback(drawPotassiumChart);\n")
    text_file.write("      google.charts.setOnLoadCallback(drawRespChart);\n")
    text_file.write("      google.charts.setOnLoadCallback(drawSodiumChart);\n")

    text_file.write("      google.charts.setOnLoadCallback(drawTempChart);\n")
    text_file.write("      google.charts.setOnLoadCallback(drawWBCChart);\n")


    text_file.write("      google.charts.setOnLoadCallback(drawVitalCount);\n")
    text_file.write("      google.charts.setOnLoadCallback(drawLabCount);\n")
    text_file.write("      google.charts.setOnLoadCallback(drawBPCount);\n")

    text_file.write("      google.charts.setOnLoadCallback(drawBedMoveCount);\n")
    text_file.write("      google.charts.setOnLoadCallback(drawRoomMoveCount);\n")
    text_file.write("      google.charts.setOnLoadCallback(drawUnitMoveCount);\n")


    text_file.write("      google.charts.setOnLoadCallback(drawMRNCount);\n")
    text_file.write("      google.charts.setOnLoadCallback(drawVisitNumCount);\n")
    text_file.write("      google.charts.setOnLoadCallback(drawPatClassCount);\n")

    text_file.write("      google.charts.setOnLoadCallback(drawMaritalStCount);\n")
    text_file.write("      google.charts.setOnLoadCallback(drawGenderCount);\n")
    text_file.write("      google.charts.setOnLoadCallback(drawFacilityCount);\n")
    
    text_file.write("      google.charts.setOnLoadCallback(drawDischDispCount);\n")
    text_file.write("      google.charts.setOnLoadCallback(drawMortalityCount);\n")
    text_file.write("      google.charts.setOnLoadCallback(drawAdmitDischChart);\n")

    text_file.write("      google.charts.setOnLoadCallback(drawVitalCountChart);\n")
    text_file.write("      google.charts.setOnLoadCallback(drawLabCountChart);\n")

############################################## 
# Function begin Braden #
    text_file.write("    function drawBradenChart() { \n")
    text_file.write("      var data = google.visualization.arrayToDataTable([ \n")
    text_file.write("        ['Count', 'Score'] \n")

# SQL insert
cursor = connection.cursor()
query_string = "select msgVal, count(msgVal) \
from [dbo].[nurse_final] \
where msgVar = '"+braden+"' \
group by msgVal \
order by cast(msgVal as float) asc"
cursor.execute(query_string)

with open('Output.html', 'a') as file:
    for row in cursor:
        file.write(str(row).replace("(",",[").replace(")","]").replace("'",""))


with open("Output.html", "a") as text_file:
    text_file.write("    ]);\n")

    text_file.write("   var options = {\n")
    text_file.write("        title: 'Braden Scale Score (19-23)',\n")
    text_file.write("        curveType: 'function',\n")
    text_file.write("        colors: ['#FFC805','#565B6C'],\n")
    text_file.write("        crosshair: {trigger: 'both'},\n")
    text_file.write("        legend: { position: 'right' }\n")
    text_file.write("    };\n")


    text_file.write("   var chart = new google.visualization.LineChart(document.getElementById('braden_chart'));\n")

    text_file.write("    chart.draw(data, options);\n")
    text_file.write("   }\n")
# Function end Braden #
##############################################    
# Function begin Systolic#
    text_file.write("    function drawSystolicChart() { \n")
    text_file.write("      var data = google.visualization.arrayToDataTable([ \n")
    text_file.write("        ['Count', 'mmHg'] \n")

# SQL insert
cursor = connection.cursor()
query_string = "select msgVal, count(msgVal) \
from [dbo].[vital_final] \
where msgVar = '"+systolic+"' \
group by msgVal \
order by cast(msgVal as float) asc"
cursor.execute(query_string)

with open('Output.html', 'a') as file:
    for row in cursor:
        file.write(str(row).replace("(",",[").replace(")","]").replace("'",""))


with open("Output.html", "a") as text_file:
    text_file.write("    ]);\n")

    text_file.write("   var options = {\n")
    text_file.write("        title: 'Systolic (101-189)',\n")
    text_file.write("        curveType: 'function',\n")
    text_file.write("        crosshair: {trigger: 'both'},\n")
    text_file.write("        colors: ['#FFC805','#565B6C'],\n")
    text_file.write("       legend: { position: 'right' }\n")
    text_file.write("    };\n")


    text_file.write("   var chart = new google.visualization.LineChart(document.getElementById('systolic_chart'));\n")

    text_file.write("    chart.draw(data, options);\n")
    text_file.write("   }\n")
# Function end Systolic#
##############################################
# Function begin Diastolic#
    text_file.write("    function drawDiastolicChart() { \n")
    text_file.write("      var data = google.visualization.arrayToDataTable([ \n")
    text_file.write("        ['Count', 'mmHg'] \n")

# SQL insert
cursor = connection.cursor()
query_string = "select msgVal, count(msgVal) \
from [dbo].[vital_final] \
where msgVar = '"+diastolic+"' \
group by msgVal \
order by cast(coalesce(msgVal,0) as float) asc"
cursor.execute(query_string)

with open('Output.html', 'a') as file:
    for row in cursor:
        file.write(str(row).replace("(",",[").replace(")","]").replace("'",""))


with open("Output.html", "a") as text_file:
    text_file.write("    ]);\n")

    text_file.write("   var options = {\n")
    text_file.write("        title: 'Diastolic (49-105)',\n")
    text_file.write("        curveType: 'function',\n")
    text_file.write("        crosshair: {trigger: 'both'},\n")
    text_file.write("        colors: ['#FFC805','#565B6C'],\n")
    text_file.write("       legend: { position: 'right' }\n")
    text_file.write("    };\n")


    text_file.write("   var chart = new google.visualization.LineChart(document.getElementById('diastolic_chart'));\n")

    text_file.write("    chart.draw(data, options);\n")
    text_file.write("   }\n")
# Function end Diastolic#
##############################################
# Function begin BUN #
    text_file.write("    function drawBUNChart() { \n")
    text_file.write("      var data = google.visualization.arrayToDataTable([ \n")
    text_file.write("        ['Count', 'mg/dL'] \n")

# SQL insert
cursor = connection.cursor()
query_string = "select msgVal, count(msgVal) \
from [dbo].[lab_final] \
where msgVar = '"+bun+"' \
group by msgVal \
order by cast(msgVal as float) asc"
cursor.execute(query_string)

with open('Output.html', 'a') as file:
    for row in cursor:
        file.write(str(row).replace("(",",[").replace(")","]").replace("'",""))


with open("Output.html", "a") as text_file:
    text_file.write("    ]);\n")

    text_file.write("   var options = {\n")
    text_file.write("        title: 'BUN (0-22)',\n")
    text_file.write("        curveType: 'function',\n")
    text_file.write("        crosshair: {trigger: 'both'},\n")
    text_file.write("        colors: ['#FFC805','#565B6C'],\n")
    text_file.write("       legend: { position: 'right' }\n")
    text_file.write("    };\n")


    text_file.write("   var chart = new google.visualization.LineChart(document.getElementById('bun_chart'));\n")

    text_file.write("    chart.draw(data, options);\n")
    text_file.write("   }\n")
# Function end BUN#
##############################################
# Function begin Chloride #
    text_file.write("    function drawChlorideChart() { \n")
    text_file.write("      var data = google.visualization.arrayToDataTable([ \n")
    text_file.write("        ['Count', 'mEq/L'] \n")

# SQL insert
cursor = connection.cursor()
query_string = "select msgVal, count(msgVal) \
from [dbo].[lab_final] \
where msgVar = '"+chloride+"' \
group by msgVal \
order by cast(msgVal as float) asc"
cursor.execute(query_string)

with open('Output.html', 'a') as file:
    for row in cursor:
        file.write(str(row).replace("(",",[").replace(")","]").replace("'",""))


with open("Output.html", "a") as text_file:
    text_file.write("    ]);\n")

    text_file.write("   var options = {\n")
    text_file.write("        title: 'Chloride (100-109)',\n")
    text_file.write("        curveType: 'function',\n")
    text_file.write("        crosshair: {trigger: 'both'},\n")
    text_file.write("        colors: ['#FFC805','#565B6C'],\n")
    text_file.write("       legend: { position: 'right' }\n")
    text_file.write("    };\n")


    text_file.write("   var chart = new google.visualization.LineChart(document.getElementById('chloride_chart'));\n")

    text_file.write("    chart.draw(data, options);\n")
    text_file.write("   }\n")
# Function end Chloride#
##############################################
# Function begin Creatinine #
    text_file.write("    function drawCreatinineChart() { \n")
    text_file.write("      var data = google.visualization.arrayToDataTable([ \n")
    text_file.write("        ['Count', 'mg/dl'] \n")

# SQL insert
cursor = connection.cursor()
query_string = "select msgVal, count(msgVal) \
from [dbo].[lab_final] \
where msgVar = '"+creatinine+"' \
group by msgVal \
order by cast(msgVal as float) asc"
cursor.execute(query_string)

with open('Output.html', 'a') as file:
    for row in cursor:
        file.write(str(row).replace("(",",[").replace(")","]").replace("'",""))


with open("Output.html", "a") as text_file:
    text_file.write("    ]);\n")

    text_file.write("   var options = {\n")
    text_file.write("        title: 'Creatinine (0.5-1.4)',\n")
    text_file.write("        curveType: 'function',\n")
    text_file.write("        crosshair: {trigger: 'both'},\n")
    text_file.write("        colors: ['#FFC805','#565B6C'],\n")
    text_file.write("       legend: { position: 'right' }\n")
    text_file.write("    };\n")


    text_file.write("   var chart = new google.visualization.LineChart(document.getElementById('creatinine_chart'));\n")

    text_file.write("    chart.draw(data, options);\n")
    text_file.write("   }\n")
# Function end Creatinine#
##############################################
# Function begin Heart Rate #
    text_file.write("    function drawHeartRateChart() { \n")
    text_file.write("      var data = google.visualization.arrayToDataTable([ \n")
    text_file.write("        ['Count', 'bpm'] \n")

# SQL insert
cursor = connection.cursor()
query_string = "select msgVal, count(msgVal) \
from [dbo].[vital_final] \
where msgVar = '"+heartrate+"' \
group by msgVal \
order by cast(msgVal as float) asc"
cursor.execute(query_string)

with open('Output.html', 'a') as file:
    for row in cursor:
        file.write(str(row).replace("(",",[").replace(")","]").replace("'",""))


with open("Output.html", "a") as text_file:
    text_file.write("    ]);\n")

    text_file.write("   var options = {\n")
    text_file.write("        title: 'Heart Rate (43-92)',\n")
    text_file.write("        curveType: 'function',\n")
    text_file.write("        crosshair: {trigger: 'both'},\n")
    text_file.write("        colors: ['#FFC805','#565B6C'],\n")
    text_file.write("       legend: { position: 'right' }\n")
    text_file.write("    };\n")


    text_file.write("   var chart = new google.visualization.LineChart(document.getElementById('heartrate_chart'));\n")

    text_file.write("    chart.draw(data, options);\n")
    text_file.write("   }\n")
# Function end Heart Rate#
##############################################
# Function begin Hemoglobin #
    text_file.write("    function drawHemoglobinChart() { \n")
    text_file.write("      var data = google.visualization.arrayToDataTable([ \n")
    text_file.write("        ['Count', 'mg/L'] \n")

# SQL insert
cursor = connection.cursor()
query_string = "select msgVal, count(msgVal) \
from [dbo].[lab_final] \
where msgVar = '"+hgb+"' \
group by msgVal \
order by cast(msgVal as float) asc"
cursor.execute(query_string)

with open('Output.html', 'a') as file:
    for row in cursor:
        file.write(str(row).replace("(",",[").replace(")","]").replace("'",""))


with open("Output.html", "a") as text_file:
    text_file.write("    ]);\n")

    text_file.write("   var options = {\n")
    text_file.write("        title: 'Hemoglobin (10.6-18.0)',\n")
    text_file.write("        curveType: 'function',\n")
    text_file.write("        crosshair: {trigger: 'both'},\n")
    text_file.write("        colors: ['#FFC805','#565B6C'],\n")
    text_file.write("       legend: { position: 'right' }\n")
    text_file.write("    };\n")


    text_file.write("   var chart = new google.visualization.LineChart(document.getElementById('hemoglobin_chart'));\n")

    text_file.write("    chart.draw(data, options);\n")
    text_file.write("   }\n")
# Function end Hemoglobin#
##############################################
# Function begin Pulse Oximetry#
    text_file.write("    function drawO2satChart() { \n")
    text_file.write("      var data = google.visualization.arrayToDataTable([ \n")
    text_file.write("        ['Count', '%'] \n")

# SQL insert
cursor = connection.cursor()
query_string = "select msgVal, count(msgVal) \
from [dbo].[vital_final] \
where msgVar = '"+pulseox+"' \
group by msgVal \
order by cast(msgVal as float) asc"
cursor.execute(query_string)

with open('Output.html', 'a') as file:
    for row in cursor:
        file.write(str(row).replace("(",",[").replace(")","]").replace("'",""))


with open("Output.html", "a") as text_file:
    text_file.write("    ]);\n")

    text_file.write("   var options = {\n")
    text_file.write("        title: 'Pulse Oximetry (93-100)',\n")
    text_file.write("        curveType: 'function',\n")
    text_file.write("        crosshair: {trigger: 'both'},\n")
    text_file.write("        colors: ['#FFC805','#565B6C'],\n")
    text_file.write("       legend: { position: 'right' }\n")
    text_file.write("    };\n")


    text_file.write("   var chart = new google.visualization.LineChart(document.getElementById('O2sat_chart'));\n")

    text_file.write("    chart.draw(data, options);\n")
    text_file.write("   }\n")
# Function end Pulse Oximetry#
##############################################
# Function begin Potassium #
    text_file.write("    function drawPotassiumChart() { \n")
    text_file.write("      var data = google.visualization.arrayToDataTable([ \n")
    text_file.write("        ['Count', 'mEq/L'] \n")

# SQL insert
cursor = connection.cursor()
query_string = "select msgVal, count(msgVal) \
from [dbo].[lab_final] \
where msgVar = '"+potassium+"' \
group by msgVal \
order by cast(msgVal as float) asc"
cursor.execute(query_string)

with open('Output.html', 'a') as file:
    for row in cursor:
        file.write(str(row).replace("(",",[").replace(")","]").replace("'",""))


with open("Output.html", "a") as text_file:
    text_file.write("    ]);\n")

    text_file.write("   var options = {\n")
    text_file.write("        title: 'Potassium (3.0-4.7)',\n")
    text_file.write("        curveType: 'function',\n")
    text_file.write("        crosshair: {trigger: 'both'},\n")
    text_file.write("        colors: ['#FFC805','#565B6C'],\n")
    text_file.write("       legend: { position: 'right' }\n")
    text_file.write("    };\n")


    text_file.write("   var chart = new google.visualization.LineChart(document.getElementById('potassium_chart'));\n")

    text_file.write("    chart.draw(data, options);\n")
    text_file.write("   }\n")
# Function end Potassium#
##############################################
# Function begin Respiratory Rate #
    text_file.write("    function drawRespChart() { \n")
    text_file.write("      var data = google.visualization.arrayToDataTable([ \n")
    text_file.write("        ['Count', 'breaths/min'] \n")

# SQL insert
cursor = connection.cursor()
query_string = "select msgVal, count(msgVal) \
from [dbo].[vital_final] \
where msgVar = '"+resp+"' \
group by msgVal \
order by cast(msgVal as float) asc"
cursor.execute(query_string)

with open('Output.html', 'a') as file:
    for row in cursor:
        file.write(str(row).replace("(",",[").replace(")","]").replace("'",""))


with open("Output.html", "a") as text_file:
    text_file.write("    ]);\n")

    text_file.write("   var options = {\n")
    text_file.write("        title: 'Respiratory Rate (15-19)',\n")
    text_file.write("        curveType: 'function',\n")
    text_file.write("        crosshair: {trigger: 'both'},\n")
    text_file.write("        colors: ['#FFC805','#565B6C'],\n")
    text_file.write("       legend: { position: 'right' }\n")
    text_file.write("    };\n")


    text_file.write("   var chart = new google.visualization.LineChart(document.getElementById('resp_chart'));\n")

    text_file.write("    chart.draw(data, options);\n")
    text_file.write("   }\n")
# Function end Respiratory Rate #
##############################################
# Function begin Sodium #
    text_file.write("    function drawSodiumChart() { \n")
    text_file.write("      var data = google.visualization.arrayToDataTable([ \n")
    text_file.write("        ['Count', 'mEq/L'] \n")

# SQL insert
cursor = connection.cursor()
query_string = "select msgVal, count(msgVal) \
from [dbo].[lab_final] \
where msgVar = '"+sodium+"' \
group by msgVal \
order by cast(msgVal as float) asc"
cursor.execute(query_string)

with open('Output.html', 'a') as file:
    for row in cursor:
        file.write(str(row).replace("(",",[").replace(")","]").replace("'",""))


with open("Output.html", "a") as text_file:
    text_file.write("    ]);\n")

    text_file.write("   var options = {\n")
    text_file.write("        title: 'Sodium (135-145)',\n")
    text_file.write("        curveType: 'function',\n")
    text_file.write("        crosshair: {trigger: 'both'},\n")
    text_file.write("        colors: ['#FFC805','#565B6C'],\n")
    text_file.write("       legend: { position: 'right' }\n")
    text_file.write("    };\n")


    text_file.write("   var chart = new google.visualization.LineChart(document.getElementById('sodium_chart'));\n")

    text_file.write("    chart.draw(data, options);\n")
    text_file.write("   }\n")
# Function end Sodium#
##############################################
# Function begin Temperature #
    text_file.write("    function drawTempChart() { \n")
    text_file.write("      var data = google.visualization.arrayToDataTable([ \n")
    text_file.write("        ['Count', '°F'] \n")

# SQL insert
cursor = connection.cursor()
query_string = "select msgVal, count(msgVal) \
from [dbo].[vital_final] \
where msgVar = '"+temp+"' \
group by msgVal  \
order by cast(msgVal as float) asc"
cursor.execute(query_string)

with open('Output.html', 'a') as file:
    for row in cursor:
        file.write(str(row).replace("(",",[").replace(")","]").replace("'",""))


with open("Output.html", "a") as text_file:
    text_file.write("    ]);\n")

    text_file.write("   var options = {\n")
    text_file.write("        title: 'Temperature (97.0-100.1)',\n")
    text_file.write("        curveType: 'function',\n")
    text_file.write("        crosshair: {trigger: 'both'},\n")
    text_file.write("        colors: ['#FFC805','#565B6C'],\n")
    text_file.write("       legend: { position: 'right' }\n")
    text_file.write("    };\n")


    text_file.write("   var chart = new google.visualization.LineChart(document.getElementById('temp_chart'));\n")

    text_file.write("    chart.draw(data, options);\n")
    text_file.write("   }\n")
# Function end Temperature #
##############################################
# Function begin WBC #
    text_file.write("    function drawWBCChart() { \n")
    text_file.write("      var data = google.visualization.arrayToDataTable([ \n")
    text_file.write("        ['Count', 'mcL'] \n")

# SQL insert
cursor = connection.cursor()
query_string = "select msgVal, count(msgVal) \
from [dbo].[lab_final] \
where msgVar = '"+wbc+"' \
group by msgVal \
order by cast(msgVal as float) asc"
cursor.execute(query_string)

with open('Output.html', 'a') as file:
    for row in cursor:
        file.write(str(row).replace("(",",[").replace(")","]").replace("'",""))


with open("Output.html", "a") as text_file:
    text_file.write("    ]);\n")

    text_file.write("   var options = {\n")
    text_file.write("        title: 'White Blood Count (4.18-14.92)',\n")
    text_file.write("        curveType: 'function',\n")
    text_file.write("        crosshair: {trigger: 'both'},\n")
    text_file.write("        colors: ['#FFC805','#565B6C'],\n")
    text_file.write("       legend: { position: 'right' }\n")
    text_file.write("    };\n")


    text_file.write("   var chart = new google.visualization.LineChart(document.getElementById('wbc_chart'));\n")

    text_file.write("    chart.draw(data, options);\n")
    text_file.write("   }\n")
# Function end WBC#
##############################################
# Function begin BP Chart #
    text_file.write("    function drawBPCount() { \n")
    text_file.write("      var data = google.visualization.arrayToDataTable([ \n")
    text_file.write("        ['Count', 'Score'] \n")

# SQL insert
cursor = connection.cursor()
query_string = "select ''''+msgVar+'''', count(msgVal) \
from [dbo].[vital_final] \
where msgVar in ('"+diastolic+"','"+systolic+"') \
group by msgVar \
"
cursor.execute(query_string)

with open('Output.html', 'a') as file:
    for row in cursor:
        file.write(str(row).replace("(",",[").replace(")","]").replace("'",""))


with open("Output.html", "a") as text_file:
    text_file.write("    ]);\n")

    text_file.write("   var options = {\n")
    text_file.write("        title: 'Blood Pressure Counts (50/50)',\n")
    text_file.write("        is3D: true,\n")
    text_file.write("        crosshair: {trigger: 'both'},\n")
    text_file.write("        colors: ['#FFC805','#565B6C','#00B0AD','#A490C4','#7595CC'],\n")
    text_file.write("       legend: { position: 'right' }\n")
    text_file.write("    };\n")


    text_file.write("   var chart = new google.visualization.PieChart(document.getElementById('bp_count'));\n")

    text_file.write("    chart.draw(data, options);\n")
    text_file.write("   }\n")
# Function end BP Chart #

##############################################
##############################################
# Function begin Vitals #
    text_file.write("    function drawVitalCount() { \n")
    text_file.write("      var data = google.visualization.arrayToDataTable([ \n")
    text_file.write("        ['Count', 'Score'] \n")

# SQL insert
cursor = connection.cursor()
query_string = "select ''''+ msgVar+'''', count(msgVar) \
from [dbo].[vital_final] \
group by msgVar \
"
cursor.execute(query_string)

with open('Output.html', 'a') as file:
    for row in cursor:
        file.write(str(row).replace("(",",[").replace(")","]").replace("'",""))


with open("Output.html", "a") as text_file:
    text_file.write("    ]);\n")

    text_file.write("   var options = {\n")
    text_file.write("        title: 'Vital Counts by Type',\n")
    text_file.write("        is3D: true,\n")
    text_file.write("        colors: ['#FFC805','#565B6C','#00B0AD','#A490C4','#7595CC','#b3b4b5','#fe7d2f','#dc1b06'],\n")
    text_file.write("       legend: { position: 'right' }\n")
    text_file.write("    };\n")


    text_file.write("   var chart = new google.visualization.PieChart(document.getElementById('vital_count'));\n")

    text_file.write("    chart.draw(data, options);\n")
    text_file.write("   }\n")
# Function end Vitals #
##############################################
# Function begin Lab Count #
    text_file.write("    function drawLabCount() { \n")
    text_file.write("      var data = google.visualization.arrayToDataTable([ \n")
    text_file.write("        ['Count', 'Score'] \n")

# SQL insert
cursor = connection.cursor()
query_string = "select ''''+ msgVar+'''', count(msgVar) \
from [dbo].[lab_final] \
group by msgVar \
"
cursor.execute(query_string)

with open('Output.html', 'a') as file:
    for row in cursor:
        file.write(str(row).replace("(",",[").replace(")","]").replace("'",""))


with open("Output.html", "a") as text_file:
    text_file.write("    ]);\n")

    text_file.write("   var options = {\n")
    text_file.write("        title: 'Lab Counts by Type',\n")
    text_file.write("        is3D: true,\n")
    text_file.write("        colors: ['#FFC805','#565B6C','#00B0AD','#A490C4','#7595CC','#b3b4b5','#fe7d2f','#dc1b06'],\n")
    text_file.write("       legend: { position: 'right' }\n")
    text_file.write("    };\n")


    text_file.write("   var chart = new google.visualization.PieChart(document.getElementById('lab_count'));\n")

    text_file.write("    chart.draw(data, options);\n")
    text_file.write("   }\n")
# Function end Lab Count #
##############################################
# Function begin MRN Count #
    text_file.write("    function drawMRNCount() { \n")
    text_file.write("      var data = google.visualization.arrayToDataTable([ \n")
    text_file.write("        ['Count', 'Score'] \n")

# SQL insert
cursor = connection.cursor()
query_string = "select ''''+'adt'+'''',count(distinct mrn) \
from adt_final \
UNION ALL \
select ''''+'lab'+'''',count(distinct mrn) \
from lab_final \
UNION ALL \
select ''''+'nurse'+'''',count(distinct mrn) \
from nurse_final \
UNION ALL \
select ''''+'vital'+'''',count(distinct mrn) \
from vital_final \
"
cursor.execute(query_string)

with open('Output.html', 'a') as file:
    for row in cursor:
        file.write(str(row).replace("(",",[").replace(")","]").replace("'",""))


with open("Output.html", "a") as text_file:
    text_file.write("    ]);\n")

    text_file.write("   var options = {\n")
    text_file.write("        title: 'MRN Counts by File',\n")
    text_file.write("        is3D: true,\n")
    text_file.write("        colors: ['#FFC805','#565B6C','#00B0AD','#A490C4','#7595CC','#b3b4b5','#fe7d2f','#dc1b06'],\n")
    text_file.write("       legend: { position: 'right' }\n")
    text_file.write("    };\n")


    text_file.write("   var chart = new google.visualization.PieChart(document.getElementById('mrn_count'));\n")

    text_file.write("    chart.draw(data, options);\n")
    text_file.write("   }\n")
# Function end MRN Count #
##############################################
##############################################
# Function begin Visit Num Count #
    text_file.write("    function drawVisitNumCount() { \n")
    text_file.write("      var data = google.visualization.arrayToDataTable([ \n")
    text_file.write("        ['Count', 'Score'] \n")

# SQL insert
cursor = connection.cursor()
query_string = "select ''''+'adt'+'''',count(distinct visitnum) \
from adt_final \
UNION ALL \
select ''''+'lab'+'''',count(distinct visitnum) \
from lab_final \
UNION ALL \
select ''''+'nurse'+'''',count(distinct visitnum) \
from nurse_final \
UNION ALL \
select ''''+'vital'+'''',count(distinct visitnum) \
from vital_final \
"
cursor.execute(query_string)

with open('Output.html', 'a') as file:
    for row in cursor:
        file.write(str(row).replace("(",",[").replace(")","]").replace("'",""))


with open("Output.html", "a") as text_file:
    text_file.write("    ]);\n")

    text_file.write("   var options = {\n")
    text_file.write("        title: 'Visit Number Counts by File',\n")
    text_file.write("        is3D: true,\n")
    text_file.write("        colors: ['#FFC805','#565B6C','#00B0AD','#A490C4','#7595CC','#b3b4b5','#fe7d2f','#dc1b06'],\n")
    text_file.write("       legend: { position: 'right' }\n")
    text_file.write("    };\n")


    text_file.write("   var chart = new google.visualization.PieChart(document.getElementById('visitnum_count'));\n")

    text_file.write("    chart.draw(data, options);\n")
    text_file.write("   }\n")
# Function end VisitNum Count #
##############################################
# Function begin PatClass Chart #
    text_file.write("    function drawPatClassCount() { \n")
    text_file.write("      var data = google.visualization.arrayToDataTable([ \n")
    text_file.write("        ['Count', 'Score'] \n")

# SQL insert
cursor = connection.cursor()
query_string = "with cte (visitnum, patclass, maxdate)as \
(select visitnum, patclass, max(dischdt) \
from adt_final \
group by visitnum, patclass \
) \
select top 10 ''''+patclass+'''', count (patclass) \
from cte \
group by patclass \
order by count (patclass) desc"
cursor.execute(query_string)

with open('Output.html', 'a') as file:
    for row in cursor:
        file.write(str(row).replace("(",",[").replace(")","]").replace("'",""))


with open("Output.html", "a") as text_file:
    text_file.write("    ]);\n")

    text_file.write("   var options = {\n")
    text_file.write("        title: 'Patient Class Counts',\n")
    text_file.write("        is3D: true,\n")
    text_file.write("        colors: ['#FFC805','#565B6C','#00B0AD','#A490C4','#7595CC','#b3b4b5','#fe7d2f','#dc1b06'],\n")
    text_file.write("       legend: { position: 'right' }\n")
    text_file.write("    };\n")


    text_file.write("   var chart = new google.visualization.PieChart(document.getElementById('patclass_count'));\n")

    text_file.write("    chart.draw(data, options);\n")
    text_file.write("   }\n")
# Function end PatClass Chart #
##############################################
# Function begin Marital Status Chart #
    text_file.write("    function drawMaritalStCount() { \n")
    text_file.write("      var data = google.visualization.arrayToDataTable([ \n")
    text_file.write("        ['Count', 'Score'] \n")

# SQL insert
cursor = connection.cursor()
query_string = "with cte (visitnum, maritalst, maxdate)as \
(select visitnum, maritalst, max(dischdt) \
from adt_final \
group by visitnum, maritalst \
) \
select top 10 ''''+maritalst+'''', count (maritalst) \
from cte \
group by maritalst \
order by count (maritalst) desc"
cursor.execute(query_string)

with open('Output.html', 'a') as file:
    for row in cursor:
        file.write(str(row).replace("(",",[").replace(")","]").replace("'",""))


with open("Output.html", "a") as text_file:
    text_file.write("    ]);\n")

    text_file.write("   var options = {\n")
    text_file.write("        title: 'Marital Status Counts',\n")
    text_file.write("        is3D: true,\n")
    text_file.write("        colors: ['#FFC805','#565B6C','#00B0AD','#A490C4','#7595CC','#b3b4b5','#fe7d2f','#dc1b06'],\n")
    text_file.write("       legend: { position: 'right' }\n")
    text_file.write("    };\n")


    text_file.write("   var chart = new google.visualization.PieChart(document.getElementById('maritalst_count'));\n")

    text_file.write("    chart.draw(data, options);\n")
    text_file.write("   }\n")
# Function end Marital Status Chart #
##############################################
# Function begin Gender Chart #
    text_file.write("    function drawGenderCount() { \n")
    text_file.write("      var data = google.visualization.arrayToDataTable([ \n")
    text_file.write("        ['Count', 'Score'] \n")

# SQL insert
cursor = connection.cursor()
query_string = "with cte (visitnum, sex, maxdate)as \
(select visitnum, sex, max(dischdt) \
from adt_final \
group by visitnum, sex \
) \
select top 10 ''''+sex+'''', count (sex) \
from cte \
group by sex \
order by count (sex) desc"
cursor.execute(query_string)

with open('Output.html', 'a') as file:
    for row in cursor:
        file.write(str(row).replace("(",",[").replace(")","]").replace("'",""))


with open("Output.html", "a") as text_file:
    text_file.write("    ]);\n")

    text_file.write("   var options = {\n")
    text_file.write("        title: 'Gender Counts',\n")
    text_file.write("        is3D: true,\n")
    text_file.write("        colors: ['#FFC805','#565B6C','#00B0AD','#A490C4','#7595CC','#b3b4b5','#fe7d2f','#dc1b06'],\n")
    text_file.write("       legend: { position: 'right' }\n")
    text_file.write("    };\n")


    text_file.write("   var chart = new google.visualization.PieChart(document.getElementById('gender_count'));\n")

    text_file.write("    chart.draw(data, options);\n")
    text_file.write("   }\n")
# Function end Gender Chart #
##############################################
# Function begin Facility Chart #
    text_file.write("    function drawFacilityCount() { \n")
    text_file.write("      var data = google.visualization.arrayToDataTable([ \n")
    text_file.write("        ['Count', 'Score'] \n")

# SQL insert
cursor = connection.cursor()
query_string = "with cte (visitnum, fac, maxdate)as \
(select visitnum, fac, max(dischdt) \
from adt_final \
group by visitnum, fac \
) \
select top 10 ''''+fac+'''', count (fac) \
from cte \
group by fac \
order by count (fac) desc"
cursor.execute(query_string)

with open('Output.html', 'a') as file:
    for row in cursor:
        file.write(str(row).replace("(",",[").replace(")","]").replace("'",""))


with open("Output.html", "a") as text_file:
    text_file.write("    ]);\n")

    text_file.write("   var options = {\n")
    text_file.write("        title: 'Facility Counts',\n")
    text_file.write("        is3D: true,\n")
    text_file.write("        colors: ['#FFC805','#565B6C','#00B0AD','#A490C4','#7595CC','#b3b4b5','#fe7d2f','#dc1b06'],\n")
    text_file.write("       legend: { position: 'right' }\n")
    text_file.write("    };\n")


    text_file.write("   var chart = new google.visualization.PieChart(document.getElementById('facility_count'));\n")

    text_file.write("    chart.draw(data, options);\n")
    text_file.write("   }\n")
# Function end Facility Chart #
##############################################
# Function begin DischDisp Chart #
    text_file.write("    function drawDischDispCount() { \n")
    text_file.write("      var data = google.visualization.arrayToDataTable([ \n")
    text_file.write("        ['DischDisp', 'Count'] \n")

# SQL insert
cursor = connection.cursor()
query_string = "with cte (visitnum, dischdisp, maxdate)as \
(select visitnum, dischdisp, max(dischdt) \
from adt_final \
group by visitnum, dischdisp \
) \
select top 10 ''''+dischdisp+'''', count (dischdisp) \
from cte \
group by dischdisp \
order by count (dischdisp) desc"
cursor.execute(query_string)

with open('Output.html', 'a') as file:
    for row in cursor:
        file.write(str(row).replace("(",",[").replace(")","]").replace("'",""))


with open("Output.html", "a") as text_file:
    text_file.write("    ]);\n")

    text_file.write("   var options = {\n")
    text_file.write("        title: 'Top 10 Discharge Disposition Counts',\n")
    text_file.write("       vAxis: { title: 'Disch Disp', width:'100%' },\n")
    text_file.write("        colors: ['#FFC805','#565B6C','#00B0AD','#A490C4','#7595CC','#b3b4b5','#fe7d2f','#dc1b06'],\n")
    text_file.write("       hAxis: { title: 'Count' }\n")

    text_file.write("    };\n")


    text_file.write("   var chart = new google.visualization.BarChart(document.getElementById('dischdisp_count'));\n")

    text_file.write("    chart.draw(data, options);\n")
    text_file.write("   }\n")
# Function end Disch Disp Table #    
##############################################
# Function begin Mortality Rate Count #
    text_file.write("    function drawMortalityCount() { \n")
    text_file.write("      var data = google.visualization.arrayToDataTable([ \n")
    text_file.write("        ['Count', 'Score'] \n")

# SQL insert
cursor = connection.cursor()
query_string = "with cte (visitnum, dischdisp, maxdate)as \
(select visitnum, dischdisp, max(dischdt) \
from adt_final \
group by visitnum, dischdisp \
) \
select ''''+'Expired'+'''',count(dischDisp) \
from cte \
where dischDisp like '%Expired%' \
UNION ALL \
select ''''+'Not Expired'+'''',count(dischDisp) \
from cte \
where dischDisp not like '%Expired%' \
"
cursor.execute(query_string)

with open('Output.html', 'a') as file:
    for row in cursor:
        file.write(str(row).replace("(",",[").replace(")","]").replace("'",""))


with open("Output.html", "a") as text_file:
    text_file.write("    ]);\n")

    text_file.write("   var options = {\n")
    text_file.write("        title: 'Mortality Rate',\n")
    text_file.write("        is3D: true,\n")
    text_file.write("        colors: ['#FFC805','#565B6C','#00B0AD','#A490C4','#7595CC','#b3b4b5','#fe7d2f','#dc1b06'],\n")
    text_file.write("       legend: { position: 'right' },\n")
    text_file.write("       slices: { 1: {offset:0.3}  }\n")  
    text_file.write("    };\n")


    text_file.write("   var chart = new google.visualization.PieChart(document.getElementById('mortality_count'));\n")

    text_file.write("    chart.draw(data, options);\n")
    text_file.write("   }\n")
# Function end Mortality Rate Count #

############################################## 
#Function begin AdmitDisch Chart #
    text_file.write("    function drawAdmitDischChart() { \n")
    text_file.write("      var data = new google.visualization.DataTable(); \n")
    text_file.write("        data.addColumn('date','Month' ); \n")
    text_file.write("        data.addColumn('number','Admit Count' ); \n")
    text_file.write("        data.addColumn('number','Discharge Count' ); \n")   
    text_file.write("        data.addRows([  \n")

# SQL insert
cursor = connection.cursor()
query_string = "WITH cteAdmit(xYear, xMonth, countAdmit) \
     AS (SELECT YEAR(z.admitdt) AS xYear, \
                MONTH(z.admitdt) AS xMonth, \
                COUNT(DISTINCT z.visitnum) AS countAdmit \
         FROM adt_final z \
         GROUP BY YEAR(z.admitdt), \
                  MONTH(z.admitdt)), \
     cteDischarge(xYear, \
                  xMonth, \
                  countDischarge) \
     AS (SELECT YEAR(z.dischdt) AS xYear, \
                MONTH(z.dischdt) AS xMonth, \
                COUNT(DISTINCT z.visitnum) AS countDischarge \
         FROM adt_final z \
         GROUP BY YEAR(z.dischdt), \
                  MONTH(z.dischdt)) \
     SELECT 'new Date'+char(123) + cast (a.xYear as varchar) +','+cast(a.xMonth as varchar)+char(125)+char(32), \
            a.countAdmit, b.countDischarge \
     FROM cteAdmit a \
          FULL OUTER JOIN cteDischarge b ON a.xYear = b.xYear AND a.xMonth = b.xMonth \
     ORDER BY a.xYear, a.xMonth"
cursor.execute(query_string)

with open('Output.html', 'a') as file:
    x=0
    for row in cursor:
        if x==0:
            row=str(row).replace("(","[").replace(")","]").replace("'","").replace("{","(").replace("}",")").replace("None","0")
        else:
            row=str(row).replace("(",",[").replace(")","]").replace("'","").replace("{","(").replace("}",")").replace("None","0")
        file.write(row)
        x=x+1

#        file.write(str(row).replace("(",",[").replace(")","]").replace("'","").replace("{","(").replace("}",")"))


with open("Output.html", "a") as text_file:
    text_file.write("    ]);\n")
    text_file.write("   var options = {\n")
    text_file.write("        title: 'Admit Disch Counts by Month',\n")
    text_file.write("        curveType: 'function',\n")
    text_file.write("        colors: ['#FFC805','#565B6C','#00B0AD','#A490C4','#7595CC','#b3b4b5','#fe7d2f','#dc1b06'],\n")
    text_file.write("       legend: { position: 'right' }\n")
    text_file.write("    };\n")


    text_file.write("   var chart = new google.visualization.ColumnChart(document.getElementById('admitdisch_chart'));\n")

    text_file.write("    chart.draw(data, options);\n")
    text_file.write("   }\n")
# Function end AdmitDisch #
##############################################    
############################################## 
#Function begin VitalCount Chart #
    text_file.write("    function drawVitalCountChart() { \n")
    text_file.write("      var data = new google.visualization.DataTable(); \n")
    text_file.write("        data.addColumn('date','Month' ); \n")
    text_file.write("        data.addColumn('number','Temp Count' ); \n")
    text_file.write("        data.addColumn('number','Systolic Count' ); \n")   
    text_file.write("        data.addColumn('number','Diastolic Count' ); \n")
    text_file.write("        data.addColumn('number','Pulse Count' ); \n")   
    text_file.write("        data.addColumn('number','Resp Count' ); \n")
    text_file.write("        data.addColumn('number','Pulse Ox Count' ); \n")  
    text_file.write("        data.addRows([  \n")

# SQL insert
cursor = connection.cursor()
query_string = "WITH ctetemp(xyear, xmonth, counttemp) \
     AS (SELECT Year(z.recordeddt)  AS xYear, \
                Month(z.recordeddt) AS xMonth, \
                Count(z.msgvar)     AS countTemp \
         FROM   vital_final AS z \
         WHERE  z.msgvar = '"+temp+"' \
         GROUP  BY Year(z.recordeddt), \
                   Month(z.recordeddt)), \
     ctesystolic(xyear, xmonth, countsystolic) \
     AS (SELECT Year(z.recordeddt)  AS xYear, \
                Month(z.recordeddt) AS xMonth, \
                Count(z.msgvar)     AS countSystolic \
         FROM   vital_final AS z \
         WHERE  z.msgvar ='"+systolic+"' \
         GROUP  BY Year(z.recordeddt), \
                   Month(z.recordeddt)), \
     ctediastolic(xyear, xmonth, countdiastolic) \
     AS (SELECT Year(z.recordeddt)  AS xYear, \
                Month(z.recordeddt) AS xMonth, \
                Count(z.msgvar)     AS countDiastolic \
         FROM   vital_final AS z \
         WHERE  z.msgvar = '"+diastolic+"' \
         GROUP  BY Year(z.recordeddt), \
                   Month(z.recordeddt)), \
     ctepulse(xyear, xmonth, countpulse) \
     AS (SELECT Year(z.recordeddt)  AS xYear, \
                Month(z.recordeddt) AS xMonth, \
                Count(z.msgvar)     AS countPulse \
         FROM   vital_final AS z \
         WHERE  z.msgvar = '"+heartrate+"' \
         GROUP  BY Year(z.recordeddt), \
                   Month(z.recordeddt)), \
     cteresp(xyear, xmonth, countresp) \
     AS (SELECT Year(z.recordeddt)  AS xYear, \
                Month(z.recordeddt) AS xMonth, \
                Count(z.msgvar)     AS countResp \
         FROM   vital_final AS z \
         WHERE  z.msgvar = '"+resp+"' \
         GROUP  BY Year(z.recordeddt), \
                   Month(z.recordeddt)), \
     cteox(xyear, xmonth, countox) \
     AS (SELECT Year(z.recordeddt)  AS xYear, \
                Month(z.recordeddt) AS xMonth, \
                Count(z.msgvar)     AS countOx \
         FROM   vital_final AS z \
         WHERE  z.msgvar = '"+pulseox+"' \
         GROUP  BY Year(z.recordeddt), \
                   Month(z.recordeddt)), \
cteTotal(xyear, xmonth, countTotal) \
AS (SELECT Year(z.recordeddt)  AS xYear, \
           Month(z.recordeddt) AS xMonth, \
           Count(z.msgvar)     AS countTotal \
    FROM   vital_final AS z \
    WHERE  z.msgvar in ( '" + temp + "', '" + systolic + "', '" + diastolic + "', '" + heartrate + "', '" + resp + "', '" + pulseox + "' ) \
    GROUP  BY Year(z.recordeddt), \
              Month(z.recordeddt)) \
SELECT 'new Date' + Char(123) \
       + Cast(x.xyear AS VARCHAR) + ',' \
       + Cast(x.xmonth AS VARCHAR) + Char(125) \
       + Char(32), \
       a.counttemp, \
       b.countsystolic, \
       c.countdiastolic, \
       d.countpulse, \
       e.countresp, \
       f.countox \
FROM   cteTotal AS x \
       FULL OUTER JOIN ctetemp      AS a ON x.xyear = a.xyear AND x.xmonth = a.xmonth \
       FULL OUTER JOIN ctesystolic  AS b ON x.xyear = b.xyear AND x.xmonth = b.xmonth \
       FULL OUTER JOIN ctediastolic AS c ON x.xyear = c.xyear AND x.xmonth = c.xmonth \
       FULL OUTER JOIN ctepulse     AS d ON x.xyear = d.xyear AND x.xmonth = d.xmonth \
       FULL OUTER JOIN cteresp      AS e ON x.xyear = e.xyear AND x.xmonth = e.xmonth \
       FULL OUTER JOIN cteox        AS f ON x.xyear = f.xyear AND x.xmonth = f.xmonth \
       FULL OUTER JOIN ctetemp      AS g ON x.xyear = g.xyear AND x.xmonth = g.xmonth \
WHERE x.xyear > 2000 \
ORDER  BY x.xyear, x.xmonth; "
cursor.execute(query_string)

with open('Output.html', 'a') as file:
    x=0
    for row in cursor:
        if x==0:
            row=str(row).replace("(","[").replace(")","]").replace("'","").replace("{","(").replace("}",")").replace("None","0")
        else:
            row=str(row).replace("(",",[").replace(")","]").replace("'","").replace("{","(").replace("}",")").replace("None","0")
        file.write(row)
        x=x+1

#        file.write(str(row).replace("(",",[").replace(")","]").replace("'","").replace("{","(").replace("}",")"))


with open("Output.html", "a") as text_file:
    text_file.write("    ]);\n")
    text_file.write("   var options = {\n")
    text_file.write("        title: 'Vitals Counts by Month',\n")
    text_file.write("        curveType: 'function',\n")
    text_file.write("        colors: ['#FFC805','#565B6C','#00B0AD','#A490C4','#7595CC','#b3b4b5','#fe7d2f','#dc1b06'],\n")
    text_file.write("       legend: { position: 'right' }\n")
    text_file.write("    };\n")


    text_file.write("   var chart = new google.visualization.LineChart(document.getElementById('vitalcount_chart'));\n")

    text_file.write("    chart.draw(data, options);\n")
    text_file.write("   }\n")
# Function end VitalCount Chart #
##############################################  

############################################## 
#Function begin LabCount Chart #
    text_file.write("    function drawLabCountChart() { \n")
    text_file.write("      var data = new google.visualization.DataTable(); \n")
    text_file.write("        data.addColumn('date','Month' ); \n")
    text_file.write("        data.addColumn('number','BUN Count' ); \n")
    text_file.write("        data.addColumn('number','Chloride Count' ); \n")   
    text_file.write("        data.addColumn('number','Creatinine Count' ); \n")
    text_file.write("        data.addColumn('number','HGB Count' ); \n")   
    text_file.write("        data.addColumn('number','Potassium Count' ); \n")
    text_file.write("        data.addColumn('number','Sodium Count' ); \n")  
    text_file.write("        data.addColumn('number','WBC Count' ); \n")  
    text_file.write("        data.addRows([  \n")

# SQL insert
cursor = connection.cursor()
query_string = "WITH cteBUN(xyear, xmonth, countBUN) \
     AS (SELECT Year(z.recordeddt)  AS xYear, \
                Month(z.recordeddt) AS xMonth, \
                Count(z.msgvar)     AS countBUN \
         FROM   lab_final AS z \
         WHERE  z.msgvar = '"+bun+"' \
         GROUP  BY Year(z.recordeddt), \
                   Month(z.recordeddt)), \
     cteChloride(xyear, xmonth, countChloride) \
     AS (SELECT Year(z.recordeddt)  AS xYear, \
                Month(z.recordeddt) AS xMonth, \
                Count(z.msgvar)     AS countChloride \
         FROM   lab_final AS z \
         WHERE  z.msgvar ='"+chloride+"' \
         GROUP  BY Year(z.recordeddt), \
                   Month(z.recordeddt)), \
     cteCreatinine(xyear, xmonth, countCreatinine) \
     AS (SELECT Year(z.recordeddt)  AS xYear, \
                Month(z.recordeddt) AS xMonth, \
                Count(z.msgvar)     AS countCreatinine \
         FROM   lab_final AS z \
         WHERE  z.msgvar = '"+creatinine+"' \
         GROUP  BY Year(z.recordeddt), \
                   Month(z.recordeddt)), \
     cteHGB(xyear, xmonth, countHGB) \
     AS (SELECT Year(z.recordeddt)  AS xYear, \
                Month(z.recordeddt) AS xMonth, \
                Count(z.msgvar)     AS countHGB \
         FROM   lab_final AS z \
         WHERE  z.msgvar = '"+hgb+"' \
         GROUP  BY Year(z.recordeddt), \
                   Month(z.recordeddt)), \
     ctePotassium(xyear, xmonth, countPotassium) \
     AS (SELECT Year(z.recordeddt)  AS xYear, \
                Month(z.recordeddt) AS xMonth, \
                Count(z.msgvar)     AS countPotassium \
         FROM   lab_final AS z \
         WHERE  z.msgvar = '"+potassium+"' \
         GROUP  BY Year(z.recordeddt), \
                   Month(z.recordeddt)), \
     cteSodium(xyear, xmonth, countSodium) \
     AS (SELECT Year(z.recordeddt)  AS xYear, \
                Month(z.recordeddt) AS xMonth, \
                Count(z.msgvar)     AS countSodium \
         FROM   lab_final AS z \
         WHERE  z.msgvar = '"+sodium+"' \
         GROUP  BY Year(z.recordeddt), \
                   Month(z.recordeddt)), \
    cteWBC(xyear, xmonth, countWBC) \
     AS (SELECT Year(z.recordeddt)  AS xYear, \
                Month(z.recordeddt) AS xMonth, \
                Count(z.msgvar)     AS countWBC \
         FROM   lab_final AS z \
         WHERE  z.msgvar = '"+wbc+"' \
         GROUP  BY Year(z.recordeddt), \
                   Month(z.recordeddt)), \
    cteTotal(xyear, xmonth, countWBC) \
 AS (SELECT Year(z.recordeddt)  AS xYear, \
            Month(z.recordeddt) AS xMonth, \
            Count(z.msgvar)     AS countWBC \
     FROM   lab_final AS z \
     WHERE  z.msgvar in ( '" + creatinine + "', '" + hgb + "', '" + potassium + "', '" + sodium + "', '" + wbc + "','" + chloride + "','" + bun + "') \
     GROUP  BY Year(z.recordeddt), \
               Month(z.recordeddt)) \
SELECT 'new Date' + Char(123) \
       + Cast(x.xyear AS VARCHAR) + ',' \
       + Cast(x.xmonth AS VARCHAR) + Char(125) \
       + Char(32), \
       a.countBUN, \
       b.countChloride, \
       c.countCreatinine, \
       d.countHGB, \
       e.countPotassium, \
       f.countSodium, \
       g.countWBC \
FROM   cteTotal AS x \
       FULL OUTER JOIN cteBUN           AS a ON x.xyear = a.xyear AND x.xmonth = a.xmonth \
       FULL OUTER JOIN cteChloride      AS b ON x.xyear = b.xyear AND x.xmonth = b.xmonth \
       FULL OUTER JOIN cteCreatinine    AS c ON x.xyear = c.xyear AND x.xmonth = c.xmonth \
       FULL OUTER JOIN cteHGB           AS d ON x.xyear = d.xyear AND x.xmonth = d.xmonth \
       FULL OUTER JOIN ctePotassium     AS e ON x.xyear = e.xyear AND x.xmonth = e.xmonth \
       FULL OUTER JOIN cteSodium        AS f ON x.xyear = f.xyear AND x.xmonth = f.xmonth \
       FULL OUTER JOIN cteWBC           AS g ON x.xyear = g.xyear AND x.xmonth = g.xmonth \
       WHERE x.xyear > 2000 \
ORDER  BY x.xyear, x.xmonth; "
cursor.execute(query_string)

with open('Output.html', 'a') as file:
    x=0
    for row in cursor:
        if x==0:
            row=str(row).replace("(","[").replace(")","]").replace("'","").replace("{","(").replace("}",")").replace("None","0")
        else:
            row=str(row).replace("(",",[").replace(")","]").replace("'","").replace("{","(").replace("}",")").replace("None","0")
        file.write(row)
        x=x+1

#        file.write(str(row).replace("(",",[").replace(")","]").replace("'","").replace("{","(").replace("}",")"))


with open("Output.html", "a") as text_file:
    text_file.write("    ]);\n")
    text_file.write("   var options = {\n")
    text_file.write("        title: 'Lab Test Counts by Month',\n")
    text_file.write("        curveType: 'function',\n")
    text_file.write("        colors: ['#FFC805','#565B6C','#00B0AD','#A490C4','#7595CC','#b3b4b5','#fe7d2f','#dc1b06'],\n")
    text_file.write("       legend: { position: 'right' }\n")
    text_file.write("    };\n")


    text_file.write("   var chart = new google.visualization.LineChart(document.getElementById('labcount_chart'));\n")

    text_file.write("    chart.draw(data, options);\n")
    text_file.write("   }\n")
# Function end LabCount Chart #
##############################################  

##############################################
# Function begin BedMove Rate Count #
    text_file.write("    function drawBedMoveCount() { \n")
    text_file.write("      var data = google.visualization.arrayToDataTable([ \n")
    text_file.write("        ['Count', 'Score'] \n")

# SQL insert
cursor = connection.cursor()
query_string = "with cteBed as ( \
select visitNum, count (distinct bed) as bedcount \
from [dbo].[adt_final] group by visitNum) \
select ''''+'0'+'''', count (visitNum) from cteBed where bedcount =0 \
union all \
select ''''+'1'+'''', count (visitNum) from cteBed where bedcount =1 \
union all \
select ''''+'2'+'''', count (visitNum) from cteBed where bedcount =2 \
union all \
select ''''+'3'+'''', count (visitNum) from cteBed where bedcount =3 \
union all \
select ''''+'>=4'+'''', count (visitNum) from cteBed where bedcount >3; \
"
cursor.execute(query_string)

with open('Output.html', 'a') as file:
    for row in cursor:
        file.write(str(row).replace("(",",[").replace(")","]").replace("'",""))


with open("Output.html", "a") as text_file:
    text_file.write("    ]);\n")

    text_file.write("   var options = {\n")
    text_file.write("        title: 'Number of Beds per Patient Visit',\n")
    text_file.write("        is3D: true,\n")
    text_file.write("        colors: ['#FFC805','#565B6C','#00B0AD','#A490C4','#7595CC','#b3b4b5','#fe7d2f','#dc1b06'],\n")
    text_file.write("       legend: { position: 'right' },\n")
    text_file.write("    };\n")


    text_file.write("   var chart = new google.visualization.PieChart(document.getElementById('bedmove_count'));\n")

    text_file.write("    chart.draw(data, options);\n")
    text_file.write("   }\n")
# Function end BedMove Rate Count #

############################################## 

##############################################
# Function begin RoomMove Rate Count #
    text_file.write("    function drawRoomMoveCount() { \n")
    text_file.write("      var data = google.visualization.arrayToDataTable([ \n")
    text_file.write("        ['Count', 'Score'] \n")

# SQL insert
cursor = connection.cursor()
query_string = "with cteRoom as ( \
select visitNum, count (distinct room) as roomcount \
from [dbo].[adt_final] group by visitNum) \
select ''''+'0'+'''', count (visitNum) from cteRoom where roomcount =0 \
union all \
select ''''+'1'+'''', count (visitNum) from cteRoom where roomcount =1 \
union all \
select ''''+'2'+'''', count (visitNum) from cteRoom where roomcount =2 \
union all \
select ''''+'3'+'''', count (visitNum) from cteRoom where roomcount =3 \
union all \
select ''''+'>=4'+'''', count (visitNum) from cteRoom where roomcount >3; \
"
cursor.execute(query_string)

with open('Output.html', 'a') as file:
    for row in cursor:
        file.write(str(row).replace("(",",[").replace(")","]").replace("'",""))


with open("Output.html", "a") as text_file:
    text_file.write("    ]);\n")

    text_file.write("   var options = {\n")
    text_file.write("        title: 'Number of Rooms per Patient Visit',\n")
    text_file.write("        is3D: true,\n")
    text_file.write("        colors: ['#FFC805','#565B6C','#00B0AD','#A490C4','#7595CC','#b3b4b5','#fe7d2f','#dc1b06'],\n")
    text_file.write("       legend: { position: 'right' },\n")
    text_file.write("    };\n")


    text_file.write("   var chart = new google.visualization.PieChart(document.getElementById('roommove_count'));\n")

    text_file.write("    chart.draw(data, options);\n")
    text_file.write("   }\n")
# Function end RoomMove Rate Count #

############################################## 

##############################################
# Function begin Unitmove Rate Count #
    text_file.write("    function drawUnitMoveCount() { \n")
    text_file.write("      var data = google.visualization.arrayToDataTable([ \n")
    text_file.write("        ['Count', 'Score'] \n")

# SQL insert
cursor = connection.cursor()
query_string = "with cteUnit as ( \
select visitNum, count (distinct unit) as unitcount \
from [dbo].[adt_final] group by visitNum) \
select ''''+'0'+'''', count (visitNum) from cteUnit where unitcount =0 \
union all \
select ''''+'1'+'''', count (visitNum) from cteUnit where unitcount =1 \
union all \
select ''''+'2'+'''', count (visitNum) from cteUnit where unitcount =2 \
union all \
select ''''+'3'+'''', count (visitNum) from cteUnit where unitcount =3 \
union all \
select ''''+'>=4'+'''', count (visitNum) from cteUnit where unitcount >3; \
"
cursor.execute(query_string)

with open('Output.html', 'a') as file:
    for row in cursor:
        file.write(str(row).replace("(",",[").replace(")","]").replace("'",""))


with open("Output.html", "a") as text_file:
    text_file.write("    ]);\n")

    text_file.write("   var options = {\n")
    text_file.write("        title: 'Number of Units per Patient Visit',\n")
    text_file.write("        is3D: true,\n")
    text_file.write("        colors: ['#FFC805','#565B6C','#00B0AD','#A490C4','#7595CC','#b3b4b5','#fe7d2f','#dc1b06'],\n")
    text_file.write("       legend: { position: 'right' },\n")
    text_file.write("    };\n")


    text_file.write("   var chart = new google.visualization.PieChart(document.getElementById('unitmove_count'));\n")

    text_file.write("    chart.draw(data, options);\n")
    text_file.write("   }\n")
# Function end RoomMove Rate Count #

############################################## 













with open("Output.html", "a") as text_file:    

    text_file.write("    </script>\n")
    text_file.write("</head>\n")
    text_file.write("<body>\n")
    text_file.write("  <h3>Lab Checks</h3><br>\n")    
    text_file.write("    <div class=\"row\">\n")
    text_file.write("      <div class=\"col-sm-4 panel panel-success\" id=\"bun_chart\" style=\"height: 70%\"></div>\n")
    text_file.write("      <div class=\"col-sm-4 panel panel-success\" id=\"chloride_chart\" style=\"height: 70%\"></div>\n")
    text_file.write("      <div class=\"col-sm-4 panel panel-success\" id=\"creatinine_chart\" style=\"height: 70%\"></div>\n")
    text_file.write("    </div>\n")
    text_file.write("    <div class=\"row\">\n")
    text_file.write("      <div class=\"col-sm-4 panel panel-success\" id=\"hemoglobin_chart\" style=\"height: 70%\"></div>\n")
    text_file.write("      <div class=\"col-sm-4 panel panel-success\" id=\"potassium_chart\" style=\"height: 70%\"></div>\n")
    text_file.write("      <div class=\"col-sm-4 panel panel-success\" id=\"sodium_chart\" style=\"height: 70%\"></div>\n")
    text_file.write("    </div>\n")    
    text_file.write("    <div class=\"row\">\n")
    text_file.write("      <div class=\"col-sm-4 panel panel-success\" id=\"wbc_chart\" style=\"height: 70%\"></div>\n")
    text_file.write("      <div class=\"col-sm-4 panel panel-success\" id=\"lab_count\" style=\"height: 70%\"></div>\n")
    text_file.write("    </div>\n")
    text_file.write("    <div class=\"row\">\n")
    text_file.write("      <div class=\"col-sm-12 panel panel-success\" id=\"labcount_chart\"     style=\"height: 70%\"></div>\n")
    text_file.write("    </div>\n")    
    text_file.write("  <h3>Vital and Nurse Checks</h3><br>\n")        
    text_file.write("    <div class=\"row\">\n")    
    text_file.write("      <div class=\"col-sm-4 panel panel-success\" id=\"systolic_chart\" style=\"height: 70%\"></div>\n")
    text_file.write("      <div class=\"col-sm-4 panel panel-success\" id=\"diastolic_chart\" style=\"height: 70%\"></div>\n")
    text_file.write("      <div class=\"col-sm-4 panel panel-success\" id=\"braden_chart\" style=\"height: 70%\"></div>\n")    
    text_file.write("    </div>\n")
    text_file.write("    <div class=\"row\">\n")    
    text_file.write("      <div class=\"col-sm-4 panel panel-success\" id=\"heartrate_chart\" style=\"height: 70%\"></div>\n")
    text_file.write("      <div class=\"col-sm-4 panel panel-success\" id=\"O2sat_chart\" style=\"height: 70%\"></div>\n")
    text_file.write("      <div class=\"col-sm-4 panel panel-success\" id=\"resp_chart\" style=\"height: 70%\"></div>\n")
    text_file.write("    </div>\n")
    text_file.write("    <div class=\"row\">\n")
    text_file.write("      <div class=\"col-sm-4 panel panel-success\" id=\"temp_chart\"     style=\"height: 70%\"></div>\n")
    text_file.write("      <div class=\"col-sm-4 panel panel-success\" id=\"vital_count\"    style=\"height: 70%\"></div>\n")
    text_file.write("      <div class=\"col-sm-4 panel panel-success\" id=\"bp_count\"       style=\"height: 70%\"></div>\n")
    text_file.write("    </div>\n")
    text_file.write("    <div class=\"row\">\n")
    text_file.write("      <div class=\"col-sm-12 panel panel-success\" id=\"vitalcount_chart\"     style=\"height: 70%\"></div>\n")
    text_file.write("    </div>\n")
    text_file.write("  <h3>ADT Checks</h3><br>\n")
    text_file.write("    <div class=\"row\">\n")
    text_file.write("      <div class=\"col-sm-4 panel panel-success\" id=\"mrn_count\"      style=\"height: 70%\"></div>\n")
    text_file.write("      <div class=\"col-sm-4 panel panel-success\" id=\"visitnum_count\" style=\"height: 70%\"></div>\n")
    text_file.write("      <div class=\"col-sm-4 panel panel-success\" id=\"patclass_count\" style=\"height: 70%\"></div>\n")
    text_file.write("    </div>\n")
    text_file.write("    <div class=\"row\">\n")
    text_file.write("      <div class=\"col-sm-4 panel panel-success\" id=\"maritalst_count\"  style=\"height: 70%\"></div>\n")
    text_file.write("      <div class=\"col-sm-4 panel panel-success\" id=\"gender_count\"   style=\"height: 70%\"></div>\n")
    text_file.write("      <div class=\"col-sm-4 panel panel-success\" id=\"facility_count\" style=\"height: 70%\"></div>\n")
    text_file.write("    </div>\n")
    text_file.write("      <div class=\"col-sm-4 panel panel-success\" id=\"bedmove_count\"  style=\"height: 70%\"></div>\n")
    text_file.write("      <div class=\"col-sm-4 panel panel-success\" id=\"roommove_count\"   style=\"height: 70%\"></div>\n")
    text_file.write("      <div class=\"col-sm-4 panel panel-success\" id=\"unitmove_count\" style=\"height: 70%\"></div>\n")
    text_file.write("    </div>\n")
    text_file.write("    <div class=\"row\">\n")
    text_file.write("      <div class=\"col-sm-8 panel panel-success\" id=\"dischdisp_count\" style=\"height: 100%\"></div>\n")
    text_file.write("      <div class=\"col-sm-4 panel panel-success\" id=\"mortality_count\" style=\"height: 100%\"></div>\n") 
    text_file.write("    </div>\n")

    text_file.write("    <div class=\"row\">\n")
    text_file.write("      <div class=\"col-sm-12 panel panel-success\" id=\"admitdisch_chart\" style=\"height: 100%\"></div>\n")

    text_file.write("    </div>\n")    
    text_file.write("  </body>\n")
    text_file.write("</html>\n")

