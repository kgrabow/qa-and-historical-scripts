import pypyodbc


bun = "BUN"
chloride = "Chloride"
creatinine = "CREATININE"
hgb = "HEMOGLOBIN"
potassium = "potassium"
sodium = "sodium"
wbc = "WBC COUNT"

braden = "braden score"
diastolic = "DIASTOLIC BLOOD PRESSURE"
systolic = "SYSTOLIC BLOOD PRESSURE"
resp = "RESPIRATORY RATE"
temp = "Temperature Fahrenheit"
heartrate = "HEART RATE"
pulseox = "Oxygen Saturation"


connection = pypyodbc.connect('Driver={Sql Server};'
                              'Server=localhost;'
                              'Database=PeraHealthInput;'
                     

                                'Trusted_Connection=True'
                              )

with open("OutputTEST.html", "w") as text_file:
    text_file.write("<html>\n")
    text_file.write("  <head>\n")
    text_file.write("  <h2>Historical Extract Files QA Checks</h2><br>\n")

    text_file.write("	<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\" integrity=\"sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u\" crossorigin=\"anonymous\"> \n")
    text_file.write("	<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css\" integrity=\"sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp\" crossorigin=\"anonymous\"> \n")
    text_file.write("	<script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\" integrity=\"sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa\" crossorigin=\"anonymous\"></script> \n")

    
    text_file.write("    <script type=\"text/javascript\" src=\"https://www.gstatic.com/charts/loader.js\"></script>\n")
    text_file.write("    <script type=\"text/javascript\">\n")
    text_file.write("     google.charts.load('current', {'packages':['corechart','line']});\n")
    text_file.write("     google.charts.setOnLoadCallback(drawVitalCountChart);\n")
 


############################################## 
#Function begin VitalCount Chart #
    text_file.write("    function drawVitalCountChart() { \n")
    text_file.write("      var data = new google.visualization.DataTable(); \n")
    text_file.write("        data.addColumn('date','Month' ); \n")
    text_file.write("        data.addColumn('number','Temp Count' ); \n")
    text_file.write("        data.addColumn('number','Systolic Count' ); \n")   
    text_file.write("        data.addColumn('number','Diastolic Count' ); \n")
    text_file.write("        data.addColumn('number','Pulse Count' ); \n")   
    text_file.write("        data.addColumn('number','Resp Count' ); \n")
    text_file.write("        data.addColumn('number','Pulse Ox Count' ); \n")  
    text_file.write("        data.addRows([  \n")

# SQL insert
cursor = connection.cursor()
query_string = "WITH ctetemp(xyear, xmonth, counttemp) \
     AS (SELECT Year(z.recordeddt)  AS xYear, \
                Month(z.recordeddt) AS xMonth, \
                Count(z.msgvar)     AS countTemp \
         FROM   vital_historical_extract_qa AS z \
         WHERE  z.msgvar = '"+temp+"' \
         GROUP  BY Year(z.recordeddt), \
                   Month(z.recordeddt)), \
     ctesystolic(xyear, xmonth, countsystolic) \
     AS (SELECT Year(z.recordeddt)  AS xYear, \
                Month(z.recordeddt) AS xMonth, \
                Count(z.msgvar)     AS countSystolic \
         FROM   vital_historical_extract_qa AS z \
         WHERE  z.msgvar ='"+systolic+"' \
         GROUP  BY Year(z.recordeddt), \
                   Month(z.recordeddt)), \
     ctediastolic(xyear, xmonth, countdiastolic) \
     AS (SELECT Year(z.recordeddt)  AS xYear, \
                Month(z.recordeddt) AS xMonth, \
                Count(z.msgvar)     AS countDiastolic \
         FROM   vital_historical_extract_qa AS z \
         WHERE  z.msgvar = '"+diastolic+"' \
         GROUP  BY Year(z.recordeddt), \
                   Month(z.recordeddt)), \
     ctepulse(xyear, xmonth, countpulse) \
     AS (SELECT Year(z.recordeddt)  AS xYear, \
                Month(z.recordeddt) AS xMonth, \
                Count(z.msgvar)     AS countPulse \
         FROM   vital_historical_extract_qa AS z \
         WHERE  z.msgvar = '"+heartrate+"' \
         GROUP  BY Year(z.recordeddt), \
                   Month(z.recordeddt)), \
     cteresp(xyear, xmonth, countresp) \
     AS (SELECT Year(z.recordeddt)  AS xYear, \
                Month(z.recordeddt) AS xMonth, \
                Count(z.msgvar)     AS countResp \
         FROM   vital_historical_extract_qa AS z \
         WHERE  z.msgvar = '"+resp+"' \
         GROUP  BY Year(z.recordeddt), \
                   Month(z.recordeddt)), \
     cteox(xyear, xmonth, countox) \
     AS (SELECT Year(z.recordeddt)  AS xYear, \
                Month(z.recordeddt) AS xMonth, \
                Count(z.msgvar)     AS countOx \
         FROM   vital_historical_extract_qa AS z \
         WHERE  z.msgvar = 'PulseOx' \
         GROUP  BY Year(z.recordeddt), \
                   Month(z.recordeddt)) \
SELECT 'new Date' + Char(123) \
       + Cast(a.xyear AS VARCHAR) + ',' \
       + Cast(a.xmonth AS VARCHAR) + Char(125) \
       + Char(32), \
       a.counttemp, \
       b.countsystolic, \
       c.countdiastolic, \
       d.countpulse, \
       e.countresp, \
       f.countox \
FROM   ctetemp AS a \
       FULL OUTER JOIN ctesystolic AS b ON a.xyear = b.xyear AND a.xmonth = b.xmonth \
       FULL OUTER JOIN ctediastolic AS c ON a.xyear = c.xyear AND a.xmonth = c.xmonth \
       FULL OUTER JOIN ctepulse AS d ON a.xyear = d.xyear AND a.xmonth = d.xmonth \
       FULL OUTER JOIN cteresp AS e ON a.xyear = e.xyear AND a.xmonth = e.xmonth \
       FULL OUTER JOIN cteox AS f ON a.xyear = d.xyear AND a.xmonth = f.xmonth \
ORDER  BY a.xyear, a.xmonth; "
cursor.execute(query_string)

with open('OutputTEST.html', 'a') as file:
    x=0
    for row in cursor:
        if x==0:
            row=str(row).replace("(","[").replace(")","]").replace("'","").replace("{","(").replace("}",")").replace("None","0")
        else:
            row=str(row).replace("(",",[").replace(")","]").replace("'","").replace("{","(").replace("}",")").replace("None","0")
        file.write(row)
        x=x+1

#        file.write(str(row).replace("(",",[").replace(")","]").replace("'","").replace("{","(").replace("}",")"))


with open("OutputTEST.html", "a") as text_file:
    text_file.write("    ]);\n")
    text_file.write("   var options = {\n")
    text_file.write("   chart: {\n")
    text_file.write("       title: 'Vitals Counts by Month',\n")
    text_file.write("       subtitle: 'This is the subtitle'\n")
    text_file.write("       }\n")
#    text_file.write("       width: 900,\n")
#    text_file.write("       height: 500\n")
    text_file.write("    };\n")


    text_file.write("   var chart = new google.charts.Line(document.getElementById('vitalcount_chart'));\n")

    text_file.write("    chart.draw(data, options);\n")
    text_file.write("   }\n")
# Function end VitalCount Chart #
##############################################    


##############################################
# Function begin DischDisp Chart #
#    text_file.write("    function drawDischDispCount() { \n")
#    text_file.write("      var data = google.visualization.DataTable(); \n")
#    text_file.write("        data.addColumn('string','DischDisp' ); \n")
#    text_file.write("        data.addColumn('number','Count' ); \n")
#    text_file.write("        data.addRows([  \n")

# SQL insert
#cursor = connection.cursor()
#query_string = "select ''''+dischdisp+'''', count(dischdisp) \
#from [dbo].[adt_historical_extract_qa] \
# \
#group by dischdisp \
#order by count(dischdisp) desc \
#"
#cursor.execute(query_string)
#
#with open('OutputTEST.html', 'a') as file:
#    for row in cursor:
#        file.write(str(row).replace("(",",[").replace(")","]").replace("'",""))#
#
#
#
#with open("OutputTEST.html", "a") as text_file:
#    text_file.write("    ]);\n")
#
#    text_file.write("   var options = {\n")
#    text_file.write("        title: 'Discharge Disposition Counts',\n")
#    text_file.write("        is3D: true,\n")
#    text_file.write("       legend: { position: 'right' }\n")
#    text_file.write("    };\n")
#
#
#   text_file.write("   var table = new google.visualization.Table(document.getElementById('dischdisp_count'));\n")
#
#    text_file.write("    table.draw(data, options);\n")
#    text_file.write("   }\n")
# Function end Disch Disp Table #    
##############################################





with open("OutputTEST.html", "a") as text_file:    

    text_file.write("    </script>\n")
    text_file.write("</head>\n")
    text_file.write("<body>\n")
    text_file.write("  <h3>Lab Checks</h3><br>\n")    
    text_file.write("    <div class=\"row\">\n")
    text_file.write("      <div class=\"col-sm-12 panel panel-success\" id=\"vitalcount_chart\" style=\"height: 70%\"></div>\n")

    text_file.write("    </div>\n")

    
    text_file.write("  </body>\n")
    text_file.write("</html>\n")

