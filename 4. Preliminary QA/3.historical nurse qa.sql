DECLARE @nurse_qa TABLE
(description VARCHAR(255),
 totRows     INT,
 distRows    INT,
 numNulls    INT,
 minValue    VARCHAR(255),
 maxValue    VARCHAR(255)
);

-- fac
INSERT INTO @nurse_qa
       SELECT 'fac',
              COUNT(fac),
              COUNT(DISTINCT fac),
              NULL,
              MIN(fac),
              MAX(fac)
       FROM [dbo].[nurse_final];
UPDATE @nurse_qa
  SET
      numNulls =
(
    SELECT COUNT(msgDt)
    FROM [dbo].[nurse_final]
    WHERE msgDt IS NULL
);

-- mrn
INSERT INTO @nurse_qa
       SELECT 'mrn',
              COUNT(mrn),
              COUNT(DISTINCT mrn),
              NULL,
              MIN(mrn),
              MAX(mrn)
       FROM [dbo].[nurse_final];
UPDATE @nurse_qa
  SET
      numNulls =
(
    SELECT COUNT(mrn)
    FROM [dbo].[nurse_final]
    WHERE mrn IS NULL
);

-- visit number
INSERT INTO @nurse_qa
       SELECT 'visitNum',
              COUNT(visitNum),
              COUNT(DISTINCT visitNum),
              NULL,
              MIN(visitNum),
              MAX(visitNum)
       FROM [dbo].[nurse_final];
UPDATE @nurse_qa
  SET
      numNulls =
(
    SELECT COUNT(visitNum)
    FROM [dbo].[nurse_final]
    WHERE visitNum IS NULL
);


-- msgDt
INSERT INTO @nurse_qa
       SELECT 'msgDt',
              COUNT(msgDt),
              COUNT(DISTINCT msgDt),
              NULL,
              MIN(CONVERT( DATETIME2, msgDt)),
              MAX(CONVERT( DATETIME2, msgDt))
       FROM [dbo].[nurse_final];
UPDATE @nurse_qa
  SET
      numNulls =
(
    SELECT COUNT(msgDt)
    FROM [dbo].[nurse_final]
    WHERE msgDt IS NULL
);

-- recordedDt
INSERT INTO @nurse_qa
       SELECT 'recordedDt',
              COUNT(recordedDt),
              COUNT(DISTINCT recordedDt),
              NULL,
              MIN(CONVERT( DATETIME2, recordedDt)),
              MAX(CONVERT( DATETIME2, recordedDt))
       FROM [dbo].[nurse_final];
UPDATE @nurse_qa
  SET
      numNulls =
(
    SELECT COUNT(recordedDt)
    FROM [dbo].[nurse_final]
    WHERE recordedDt IS NULL
);




-- Cardiac
INSERT INTO @nurse_qa
       SELECT '%cardiac%',
              COUNT(msgVar),
              COUNT(DISTINCT msgVar),
              NULL,
              MIN(msgVar),
              MAX(msgVar)
       FROM [dbo].[nurse_final]
       WHERE msgVar LIKE '%cardiac%';
UPDATE @nurse_qa
  SET
      numNulls =
(
    SELECT COUNT(msgVar)
    FROM [dbo].[nurse_final]
    WHERE msgVal IS NULL
          AND msgVar LIKE '%cardiac%'
);

-- neurological
INSERT INTO @nurse_qa
       SELECT '%neuro%',
              COUNT(msgVar),
              COUNT(DISTINCT msgVar),
              NULL,
              MIN(msgVar),
              MAX(msgVar)
       FROM [dbo].[nurse_final]
       WHERE msgVar LIKE '%neuro%';
UPDATE @nurse_qa
  SET
      numNulls =
(
    SELECT COUNT(msgVar)
    FROM [dbo].[nurse_final]
    WHERE msgVal IS NULL
          AND msgVar LIKE '%neuro%'
);

-- nutri
INSERT INTO @nurse_qa
       SELECT '%nutri%',
              COUNT(msgVar),
              COUNT(DISTINCT msgVar),
              NULL,
              MIN(msgVar),
              MAX(msgVar)
       FROM [dbo].[nurse_final]
       WHERE msgVar LIKE '%nutri%';
UPDATE @nurse_qa
  SET
      numNulls =
(
    SELECT COUNT(msgVar)
    FROM [dbo].[nurse_final]
    WHERE msgVal IS NULL
          AND msgVar LIKE '%nutri%'
);

-- gastro
INSERT INTO @nurse_qa
       SELECT '%gastro%',
              COUNT(msgVar),
              COUNT(DISTINCT msgVar),
              NULL,
              MIN(msgVar),
              MAX(msgVar)
       FROM [dbo].[nurse_final]
       WHERE msgVar LIKE '%gastro%';
UPDATE @nurse_qa
  SET
      numNulls =
(
    SELECT COUNT(msgVar)
    FROM [dbo].[nurse_final]
    WHERE msgVal IS NULL
          AND msgVar LIKE '%gastro%'
);

-- urinary
INSERT INTO @nurse_qa
       SELECT '%urinary%',
              COUNT(msgVar),
              COUNT(DISTINCT msgVar),
              NULL,
              MIN(msgVar),
              MAX(msgVar)
       FROM [dbo].[nurse_final]
       WHERE msgVar LIKE '%urinary%';
UPDATE @nurse_qa
  SET
      numNulls =
(
    SELECT COUNT(msgVar)
    FROM [dbo].[nurse_final]
    WHERE msgVal IS NULL
          AND msgVar LIKE '%urinary%'
);

-- braden
INSERT INTO @nurse_qa
       SELECT '%braden%',
              COUNT(msgVar),
              COUNT(DISTINCT msgVar),
              NULL,
              MIN(msgVar),
              MAX(msgVar)
       FROM [dbo].[nurse_final]
       WHERE msgVar LIKE '%braden%';
UPDATE @nurse_qa
  SET
      numNulls =
(
    SELECT COUNT(msgVar)
    FROM [dbo].[nurse_final]
    WHERE msgVal IS NULL
          AND msgVar LIKE '%braden%'
);

-- heart
INSERT INTO @nurse_qa
       SELECT '%heart%',
              COUNT(msgVar),
              COUNT(DISTINCT msgVar),
              NULL,
              MIN(msgVar),
              MAX(msgVar)
       FROM [dbo].[nurse_final]
       WHERE msgVar LIKE '%heart%';
UPDATE @nurse_qa
  SET
      numNulls =
(
    SELECT COUNT(msgVar)
    FROM [dbo].[nurse_final]
    WHERE msgVal IS NULL
          AND msgVar LIKE '%heart%'
);

-- resp
INSERT INTO @nurse_qa
       SELECT '%respir%',
              COUNT(msgVar),
              COUNT(DISTINCT msgVar),
              NULL,
              MIN(msgVar),
              MAX(msgVar)
       FROM [dbo].[nurse_final]
       WHERE msgVar LIKE '%respir%';
UPDATE @nurse_qa
  SET
      numNulls =
(
    SELECT COUNT(msgVar)
    FROM [dbo].[nurse_final]
    WHERE msgVal IS NULL
          AND msgVar LIKE '%respir%'
);



-- morse
INSERT INTO @nurse_qa
       SELECT '%morse%',
              COUNT(msgVar),
              COUNT(DISTINCT msgVar),
              NULL,
              MIN(msgVar),
              MAX(msgVar)
       FROM [dbo].[nurse_final]
       WHERE msgVar LIKE '%morse%';
UPDATE @nurse_qa
  SET
      numNulls =
(
    SELECT COUNT(msgVar)
    FROM [dbo].[nurse_final]
    WHERE msgVal IS NULL
          AND msgVar LIKE '%morse%'
);

-- skin
INSERT INTO @nurse_qa
       SELECT '%skin%',
              COUNT(msgVar),
              COUNT(DISTINCT msgVar),
              NULL,
              MIN(msgVar),
              MAX(msgVar)
       FROM [dbo].[nurse_final]
       WHERE msgVar LIKE '%skin%';
UPDATE @nurse_qa
  SET
      numNulls =
(
    SELECT COUNT(msgVar)
    FROM [dbo].[nurse_final]
    WHERE msgVal IS NULL
          AND msgVar LIKE '%skin%'
);

-- vasc
INSERT INTO @nurse_qa
       SELECT '%vasc%',
              COUNT(msgVar),
              COUNT(DISTINCT msgVar),
              NULL,
              MIN(msgVar),
              MAX(msgVar)
       FROM [dbo].[nurse_final]
       WHERE msgVar LIKE '%vasc%';
UPDATE @nurse_qa
  SET
      numNulls =
(
    SELECT COUNT(msgVar)
    FROM [dbo].[nurse_final]
    WHERE msgVal IS NULL
          AND msgVar LIKE '%vasc%'
);

-- psychosocial
INSERT INTO @nurse_qa
       SELECT '%psychosocial%',
              COUNT(msgVar),
              COUNT(DISTINCT msgVar),
              NULL,
              MIN(msgVar),
              MAX(msgVar)
       FROM [dbo].[nurse_final]
       WHERE msgVar LIKE '%psycho%';
UPDATE @nurse_qa
  SET
      numNulls =
(
    SELECT COUNT(msgVar)
    FROM [dbo].[nurse_final]
    WHERE msgVal IS NULL
          AND msgVar LIKE '%psycho%'
);

-- musculo
INSERT INTO @nurse_qa
       SELECT '%musculo%',
              COUNT(msgVar),
              COUNT(DISTINCT msgVar),
              NULL,
              MIN(msgVar),
              MAX(msgVar)
       FROM [dbo].[nurse_final]
       WHERE msgVar LIKE '%musculo%';
UPDATE @nurse_qa
  SET
      numNulls =
(
    SELECT COUNT(msgVar)
    FROM [dbo].[nurse_final]
    WHERE msgVal IS NULL
          AND msgVar LIKE '%musculo%'
);


-- Final Query
SELECT *
FROM @nurse_qa;