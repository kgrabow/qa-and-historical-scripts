DECLARE @demo_qa TABLE
(description VARCHAR(255),
 totRows     INT,
 distRows    INT,
 numNulls    INT,
 minValue    VARCHAR(255),
 maxValue    VARCHAR(255)
);

-- facility
INSERT INTO @demo_qa
       SELECT 'fac',
              COUNT(fac),
              COUNT(DISTINCT fac),
              NULL,
              MIN(fac),
              MAX(fac)
       FROM adt_final;
UPDATE @demo_qa
  SET
      numNulls =
(
    SELECT COUNT(fac)
    FROM adt_final
    WHERE fac IS NULL
);

-- mrn
INSERT INTO @demo_qa
       SELECT 'mrn',
              COUNT(mrn),
              COUNT(DISTINCT mrn),
              NULL,
              MIN(mrn),
              MAX(mrn)
       FROM adt_final;
UPDATE @demo_qa
  SET
      numNulls =
(
    SELECT COUNT(mrn)
    FROM adt_final
    WHERE mrn IS NULL
);

-- visitNum
INSERT INTO @demo_qa
       SELECT 'visitNum',
              COUNT(visitNum),
              COUNT(DISTINCT visitNum),
              NULL,
              MIN(visitNum),
              MAX(visitNum)
       FROM adt_final;
UPDATE @demo_qa
  SET
      numNulls =
(
    SELECT COUNT(visitNum)
    FROM adt_final
    WHERE visitNum IS NULL
);

-- acctNum
INSERT INTO @demo_qa
       SELECT 'acctNum',
              COUNT(acctNum),
              COUNT(DISTINCT acctNum),
              NULL,
              MIN(acctNum),
              MAX(acctNum)
       FROM adt_final;
UPDATE @demo_qa
  SET
      numNulls =
(
    SELECT COUNT(acctNum)
    FROM adt_final
    WHERE acctNum IS NULL
);

-- ein
INSERT INTO @demo_qa
       SELECT 'ein',
              COUNT(mrn),
              COUNT(DISTINCT mrn),
              NULL,
              MIN(mrn),
              MAX(mrn)
       FROM adt_final;
UPDATE @demo_qa
  SET
      numNulls =
(
    SELECT COUNT(mrn)
    FROM adt_final
    WHERE mrn IS NULL
);

-- firstName
INSERT INTO @demo_qa
       SELECT 'firstName',
              COUNT(firstName),
              COUNT(DISTINCT firstName),
              NULL,
              MIN(firstName),
              MAX(firstName)
       FROM adt_final;
UPDATE @demo_qa
  SET
      numNulls =
(
    SELECT COUNT(firstName)
    FROM adt_final
    WHERE firstName IS NULL
);

-- lastName
INSERT INTO @demo_qa
       SELECT 'lastName',
              COUNT(lastName),
              COUNT(DISTINCT lastName),
              NULL,
              MIN(lastName),
              MAX(lastName)
       FROM adt_final;
UPDATE @demo_qa
  SET
      numNulls =
(
    SELECT COUNT(lastName)
    FROM adt_final
    WHERE lastName IS NULL
);

-- birthDt
INSERT INTO @demo_qa
       SELECT 'birthDt',
              COUNT(birthDt),
              COUNT(DISTINCT birthDt),
              NULL,
              MIN(birthDt),
              MAX(birthDt)
       FROM adt_final;
UPDATE @demo_qa
  SET
      numNulls =
(
    SELECT COUNT(birthDt)
    FROM adt_final
    WHERE birthDt IS NULL
);

-- sex
INSERT INTO @demo_qa
       SELECT 'sex',
              COUNT(sex),
              COUNT(DISTINCT sex),
              NULL,
              MIN(sex),
              MAX(sex)
       FROM adt_final;
UPDATE @demo_qa
  SET
      numNulls =
(
    SELECT COUNT(sex)
    FROM adt_final
    WHERE sex IS NULL
);

-- maritalSt
INSERT INTO @demo_qa
       SELECT 'maritalSt',
              COUNT(maritalSt),
              COUNT(DISTINCT maritalSt),
              NULL,
              MIN(maritalSt),
              MAX(maritalSt)
       FROM adt_final;
UPDATE @demo_qa
  SET
      numNulls =
(
    SELECT COUNT(maritalSt)
    FROM adt_final
    WHERE maritalSt IS NULL
);

-- zipCd
INSERT INTO @demo_qa
       SELECT 'zipCd',
              COUNT(zipCd),
              COUNT(DISTINCT zipCd),
              NULL,
              MIN(zipCd),
              MAX(zipCd)
       FROM adt_final;
UPDATE @demo_qa
  SET
      numNulls =
(
    SELECT COUNT(zipCd)
    FROM adt_final
    WHERE zipCd IS NULL
);

-- provider
INSERT INTO @demo_qa
       SELECT 'provider',
              COUNT(provider),
              COUNT(DISTINCT provider),
              NULL,
              MIN(provider),
              MAX(provider)
       FROM adt_final;
UPDATE @demo_qa
  SET
      numNulls =
(
    SELECT COUNT(provider)
    FROM adt_final
    WHERE provider IS NULL
);

-- patClass
INSERT INTO @demo_qa
       SELECT 'patClass',
              COUNT(patClass),
              COUNT(DISTINCT patClass),
              NULL,
              MIN(patClass),
              MAX(patClass)
       FROM adt_final;
UPDATE @demo_qa
  SET
      numNulls =
(
    SELECT COUNT(patClass)
    FROM adt_final
    WHERE patClass IS NULL
);

-- admitDt
INSERT INTO @demo_qa
       SELECT 'admitDt',
              COUNT(admitDt),
              COUNT(DISTINCT admitDt),
              NULL,
              MIN(admitDt),
              MAX(admitDt)
       FROM adt_final;
UPDATE @demo_qa
  SET
      numNulls =
(
    SELECT COUNT(admitDt)
    FROM adt_final
    WHERE admitDt IS NULL
);

-- dischDt
INSERT INTO @demo_qa
       SELECT 'dischDt',
              COUNT(dischDt),
              COUNT(DISTINCT dischDt),
              NULL,
              MIN(dischDt),
              MAX(dischDt)
       FROM adt_final;
UPDATE @demo_qa
  SET
      numNulls =
(
    SELECT COUNT(dischDt)
    FROM adt_final
    WHERE dischDt IS NULL
);


-- dischDisp
INSERT INTO @demo_qa
       SELECT 'dischDisp',
              COUNT(dischDisp),
              COUNT(DISTINCT dischDisp),
              NULL,
              MIN(dischDisp),
              MAX(dischDisp)
       FROM adt_final;
UPDATE @demo_qa
  SET
      numNulls =
(
    SELECT COUNT(dischDisp)
    FROM adt_final
    WHERE dischDisp IS NULL
);



-- unit
INSERT INTO @demo_qa
       SELECT 'unit',
              COUNT(unit),
              COUNT(DISTINCT unit),
              NULL,
              MIN(unit),
              MAX(unit)
       FROM adt_final;
UPDATE @demo_qa
  SET
      numNulls =
(
    SELECT COUNT(unit)
    FROM adt_final
    WHERE unit IS NULL
);


-- room
INSERT INTO @demo_qa
       SELECT 'room',
              COUNT(room),
              COUNT(DISTINCT room),
              NULL,
              MIN(room),
              MAX(room)
       FROM adt_final;
UPDATE @demo_qa
  SET
      numNulls =
(
    SELECT COUNT(room)
    FROM adt_final
    WHERE room IS NULL
);

-- bed
INSERT INTO @demo_qa
       SELECT 'bed',
              COUNT(bed),
              COUNT(DISTINCT bed),
              NULL,
              MIN(bed),
              MAX(bed)
       FROM adt_final;
UPDATE @demo_qa
  SET
      numNulls =
(
    SELECT COUNT(bed)
    FROM adt_final
    WHERE bed IS NULL
);

-- diagnosis
INSERT INTO @demo_qa
       SELECT 'diagnosis',
              COUNT(diagnosis),
              COUNT(DISTINCT diagnosis),
              NULL,
              MIN(diagnosis),
              MAX(diagnosis)
       FROM adt_final;
UPDATE @demo_qa
  SET
      numNulls =
(
    SELECT COUNT(diagnosis)
    FROM adt_final
    WHERE diagnosis IS NULL
);


-- diagCode
INSERT INTO @demo_qa
       SELECT 'diagCode',
              COUNT(diagCode),
              COUNT(DISTINCT diagCode),
              NULL,
              MIN(diagCode),
              MAX(diagCode)
       FROM adt_final;
UPDATE @demo_qa
  SET
      numNulls =
(
    SELECT COUNT(diagCode)
    FROM adt_final
    WHERE diagCode IS NULL
);


-- diagType
INSERT INTO @demo_qa
       SELECT 'diagType',
              COUNT(diagType),
              COUNT(DISTINCT diagType),
              NULL,
              MIN(diagType),
              MAX(diagType)
       FROM adt_final;
UPDATE @demo_qa
  SET
      numNulls =
(
    SELECT COUNT(diagType)
    FROM adt_final
    WHERE diagType IS NULL
);

-- msgDt
INSERT INTO @demo_qa
       SELECT 'msgDt',
              COUNT(msgDt),
              COUNT(DISTINCT msgDt),
              NULL,
              MIN(CONVERT( DATETIME2, msgDt)),
              MAX(CONVERT( DATETIME2, msgDt))
       FROM adt_final;
UPDATE @demo_qa
  SET
      numNulls =
(
    SELECT COUNT(msgDt)
    FROM adt_final
    WHERE msgDt IS NULL
);
SELECT *
FROM @demo_qa;