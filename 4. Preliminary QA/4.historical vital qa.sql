DECLARE @systolic VARCHAR(30);
DECLARE @diastolic VARCHAR(30);
DECLARE @pulse VARCHAR(30);
DECLARE @oxygen VARCHAR(30);
DECLARE @resp VARCHAR(30);
DECLARE @temp VARCHAR(30);

-- Fill in this section with proper variable names for vitals
-- use the following query to get the proper variable names

-- SELECT distinct msgvar from vital_final

SET @systolic = 'Systolic Blood Pressure';
SET @diastolic = 'Diastolic Blood Pressure';
SET @oxygen = 'Oxygen Saturation';
SET @pulse = 'Heart Rate';
SET @resp = 'Respiratory Rate';
SET @temp = 'Temperature Fahrenheit';

DECLARE @vitals_qa TABLE
(description VARCHAR(255),
 totRows     INT,
 distRows    INT,
 numNulls    INT,
 minValue    VARCHAR(255),
 maxValue    VARCHAR(255)
);



-- fac
INSERT INTO @vitals_qa
       SELECT 'fac',
              COUNT(fac),
              COUNT(DISTINCT fac),
              NULL,
              MIN(fac),
              MAX(fac)
       FROM [dbo].[vital_final];
UPDATE @vitals_qa
  SET
      numNulls =
(
    SELECT COUNT(msgDt)
    FROM [dbo].[vital_final]
    WHERE msgDt IS NULL
);

-- mrn
INSERT INTO @vitals_qa
       SELECT 'mrn',
              COUNT(mrn),
              COUNT(DISTINCT mrn),
              NULL,
              MIN(mrn),
              MAX(mrn)
       FROM [dbo].[vital_final];
UPDATE @vitals_qa
  SET
      numNulls =
(
    SELECT COUNT(mrn)
    FROM [dbo].[vital_final]
    WHERE mrn IS NULL
);

-- visit number
INSERT INTO @vitals_qa
       SELECT 'visitNum',
              COUNT(visitNum),
              COUNT(DISTINCT visitNum),
              NULL,
              MIN(visitNum),
              MAX(visitNum)
       FROM [dbo].[vital_final];
UPDATE @vitals_qa
  SET
      numNulls =
(
    SELECT COUNT(visitNum)
    FROM [dbo].[vital_final]
    WHERE visitNum IS NULL
);


-- msgDt
INSERT INTO @vitals_qa
       SELECT 'msgDt',
              COUNT(msgDt),
              COUNT(DISTINCT msgDt),
              NULL,
              MIN(CONVERT( DATETIME2, msgDt)),
              MAX(CONVERT( DATETIME2, msgDt))
       FROM [dbo].[vital_final];
UPDATE @vitals_qa
  SET
      numNulls =
(
    SELECT COUNT(msgDt)
    FROM [dbo].[vital_final]
    WHERE msgDt IS NULL
);

-- recordedDt
INSERT INTO @vitals_qa
       SELECT 'recordedDt',
              COUNT(recordedDt),
              COUNT(DISTINCT recordedDt),
              NULL,
              MIN(CONVERT( DATETIME2, recordedDt)),
              MAX(CONVERT( DATETIME2, recordedDt))
       FROM [dbo].[vital_final];
UPDATE @vitals_qa
  SET
      numNulls =
(
    SELECT COUNT(recordedDt)
    FROM [dbo].[vital_final]
    WHERE recordedDt IS NULL
);







-- systolic
INSERT INTO @vitals_qa
       SELECT 'sytstolic',
              COUNT(msgVar),
              COUNT(msgVar),
              NULL,
              MIN(CONVERT( FLOAT, msgVal)),
              MAX(CONVERT( FLOAT, msgVal))
       FROM [dbo].[vital_final]
       WHERE msgVar = @systolic;
UPDATE @vitals_qa
  SET
      numNulls =
(
    SELECT COUNT(msgVar)
    FROM [dbo].[vital_final]
    WHERE msgVal IS NULL
          AND msgVar = @systolic
);

-- diastolic
INSERT INTO @vitals_qa
       SELECT 'diastolic',
              COUNT(msgVar),
              COUNT(msgVar),
              NULL,
              MIN(CONVERT( FLOAT, msgVal)),
              MAX(CONVERT( FLOAT, msgVal))
       FROM [dbo].[vital_final]
       WHERE msgVar = @diastolic;
UPDATE @vitals_qa
  SET
      numNulls =
(
    SELECT COUNT(msgVar)
    FROM [dbo].[vital_final]
    WHERE msgVal IS NULL
          AND msgVar = @diastolic
);

-- heart rate
INSERT INTO @vitals_qa
       SELECT 'heart rate',
              COUNT(msgVar),
              COUNT(msgVar),
              NULL,
              MIN(CONVERT( FLOAT, msgVal)),
              MAX(CONVERT( FLOAT, msgVal))
       FROM [dbo].[vital_final]
       WHERE msgVar = @pulse;
UPDATE @vitals_qa
  SET
      numNulls =
(
    SELECT COUNT(msgVar)
    FROM [dbo].[vital_final]
    WHERE msgVal IS NULL
          AND msgVar = @pulse
);


-- pulse ox

INSERT INTO @vitals_qa
       SELECT 'pulse ox',
              COUNT(msgVar),
              COUNT(msgVar),
              NULL,
              MIN(CONVERT( FLOAT, msgVal)),
              MAX(CONVERT( FLOAT, msgVal))
       FROM [dbo].[vital_final]
       WHERE msgVar = @oxygen;
UPDATE @vitals_qa
  SET
      numNulls =
(
    SELECT COUNT(msgVar)
    FROM [dbo].[vital_final]
    WHERE msgVal IS NULL
          AND msgVar = @oxygen
);

-- resp rate
INSERT INTO @vitals_qa
       SELECT 'resp rate',
              COUNT(msgVar),
              COUNT(msgVar),
              NULL,
              MIN(CONVERT( FLOAT, msgVal)),
              MAX(CONVERT( FLOAT, msgVal))
       FROM [dbo].[vital_final]
       WHERE msgVar = @resp;
UPDATE @vitals_qa
  SET
      numNulls =
(
    SELECT COUNT(msgVar)
    FROM [dbo].[vital_final]
    WHERE msgVal IS NULL
          AND msgVar = @resp
);

-- temperature
INSERT INTO @vitals_qa
       SELECT 'temperature',
              COUNT(msgVar),
              COUNT(msgVar),
              NULL,
              MIN(CONVERT( FLOAT, msgVal)),
              MAX(CONVERT( FLOAT, msgVal))
       FROM [dbo].[vital_final]
       WHERE msgVar = @temp;
UPDATE @vitals_qa
  SET
      numNulls =
(
    SELECT COUNT(msgVar)
    FROM [dbo].[vital_final]
    WHERE msgVal IS NULL
          AND msgVar = @temp
);

-- Final Query
SELECT *
FROM @vitals_qa;