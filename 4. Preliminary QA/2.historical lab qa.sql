DECLARE @bun VARCHAR(30);
DECLARE @creatinine VARCHAR(30);
DECLARE @wbc VARCHAR(30);
DECLARE @hgb VARCHAR(30);
DECLARE @chloride VARCHAR(30);
DECLARE @sodium VARCHAR(30);
DECLARE @potassium VARCHAR(30);

-- Fill in this section with proper names for labs
-- use the following query to get all value names
-- SELECT distinct msgvar from lab_final
SET @bun = 'BUN';
SET @chloride = 'Chloride';
SET @creatinine = 'creatinine';
SET @hgb = 'hemoglobin';
SET @potassium = 'potassium';
SET @sodium = 'sodium';
SET @wbc = 'wbc count';
DECLARE @labs_qa TABLE
(description VARCHAR(255),
 totRows     INT,
 distRows    INT,
 numNulls    INT,
 minValue    VARCHAR(255),
 maxValue    VARCHAR(255)
);

-- fac
INSERT INTO @labs_qa
       SELECT 'fac',
              COUNT(fac),
              COUNT(DISTINCT fac),
              NULL,
              MIN(fac),
              MAX(fac)
       FROM [dbo].[vital_final];
UPDATE @labs_qa
  SET
      numNulls =
(
    SELECT COUNT(msgDt)
    FROM [dbo].[vital_final]
    WHERE msgDt IS NULL
);

-- mrn
INSERT INTO @labs_qa
       SELECT 'mrn',
              COUNT(mrn),
              COUNT(DISTINCT mrn),
              NULL,
              MIN(mrn),
              MAX(mrn)
       FROM lab_final;
UPDATE @labs_qa
  SET
      numNulls =
(
    SELECT COUNT(mrn)
    FROM lab_final
    WHERE mrn IS NULL
);

-- visit number
INSERT INTO @labs_qa
       SELECT 'visitNum',
              COUNT(visitNum),
              COUNT(DISTINCT visitNum),
              NULL,
              MIN(visitNum),
              MAX(visitNum)
       FROM [dbo].[vital_final];
UPDATE @labs_qa
  SET
      numNulls =
(
    SELECT COUNT(visitNum)
    FROM [dbo].[vital_final]
    WHERE visitNum IS NULL
);

-- msgDt
INSERT INTO @labs_qa
       SELECT 'msgDt',
              COUNT(msgDt),
              COUNT(DISTINCT msgDt),
              NULL,
              MIN(CONVERT( DATETIME2, msgDt)),
              MAX(CONVERT( DATETIME2, msgDt))
       FROM lab_final;
UPDATE @labs_qa
  SET
      numNulls =
(
    SELECT COUNT(msgDt)
    FROM lab_final
    WHERE msgDt IS NULL
);

-- recordedDt
INSERT INTO @labs_qa
       SELECT 'recordedDt',
              COUNT(recordedDt),
              COUNT(DISTINCT recordedDt),
              NULL,
              MIN(CONVERT( DATETIME2, recordedDt)),
              MAX(CONVERT( DATETIME2, recordedDt))
       FROM lab_final;
UPDATE @labs_qa
  SET
      numNulls =
(
    SELECT COUNT(recordedDt)
    FROM lab_final
    WHERE recordedDt IS NULL
);


-- BUN
INSERT INTO @labs_qa
       SELECT 'bun',
              COUNT(msgVar),
              COUNT(msgVar),
              NULL,
              MIN(CONVERT( FLOAT, REPLACE(REPLACE(msgVal, '', ''), '<', ''))),
              MAX(CONVERT( FLOAT, REPLACE(REPLACE(msgVal, '', ''), '<', '')))
       FROM lab_final
       WHERE msgVar = @bun;
UPDATE @labs_qa
  SET
      numNulls =
(
    SELECT COUNT(msgVar)
    FROM lab_final
    WHERE msgVal IS NULL
          AND msgVar = @bun
);

-- Creatinine
INSERT INTO @labs_qa
       SELECT 'creatinine',
              COUNT(msgVar),
              COUNT(msgVar),
              NULL,
              MIN(CONVERT( FLOAT, REPLACE(REPLACE(msgVal, '', ''), '<', ''))),
              MAX(CONVERT( FLOAT, REPLACE(REPLACE(msgVal, '', ''), '<', '')))
       FROM lab_final
       WHERE msgVar = @creatinine;
UPDATE @labs_qa
  SET
      numNulls =
(
    SELECT COUNT(msgVar)
    FROM lab_final
    WHERE msgVal IS NULL
          AND msgVar = @creatinine
);

-- wbc
INSERT INTO @labs_qa
       SELECT 'wbc',
              COUNT(msgVar),
              COUNT(msgVar),
              NULL,
              MIN(CONVERT( FLOAT, REPLACE(REPLACE(msgVal, '', ''), '<', ''))),
              MAX(CONVERT( FLOAT, REPLACE(REPLACE(msgVal, '', ''), '<', '')))
       FROM lab_final
       WHERE msgVar = @wbc;
UPDATE @labs_qa
  SET
      numNulls =
(
    SELECT COUNT(msgVar)
    FROM lab_final
    WHERE msgVal IS NULL
          AND msgVar = @wbc
);

-- hemoglobin
INSERT INTO @labs_qa
       SELECT 'hemoglobin',
              COUNT(msgVar),
              COUNT(msgVar),
              NULL,
              MIN(CONVERT( FLOAT, REPLACE(REPLACE(msgVal, '', ''), '<', ''))),
              MAX(CONVERT( FLOAT, REPLACE(REPLACE(msgVal, '', ''), '<', '')))
       FROM lab_final
       WHERE msgVar = @hgb;
UPDATE @labs_qa
  SET
      numNulls =
(
    SELECT COUNT(msgVar)
    FROM lab_final
    WHERE msgVal IS NULL
          AND msgVar = @hgb
);

-- chloride
INSERT INTO @labs_qa
       SELECT 'chloride',
              COUNT(msgVar),
              COUNT(msgVar),
              NULL,
              MIN(CONVERT( FLOAT, REPLACE(REPLACE(msgVal, '', ''), '<', ''))),
              MAX(CONVERT( FLOAT, REPLACE(REPLACE(msgVal, '', ''), '<', '')))
       FROM lab_final
       WHERE msgVar = @chloride;
UPDATE @labs_qa
  SET
      numNulls =
(
    SELECT COUNT(msgVar)
    FROM lab_final
    WHERE msgVal IS NULL
          AND msgVar = @chloride
);

-- sodium
INSERT INTO @labs_qa
       SELECT 'sodium',
              COUNT(msgVar),
              COUNT(msgVar),
              NULL,
              MIN(CONVERT( FLOAT, REPLACE(REPLACE(msgVal, '', ''), '<', ''))),
              MAX(CONVERT( FLOAT, REPLACE(REPLACE(msgVal, '', ''), '<', '')))
       FROM lab_final
       WHERE msgVar = @sodium;
UPDATE @labs_qa
  SET
      numNulls =
(
    SELECT COUNT(msgVar)
    FROM lab_final
    WHERE msgVal IS NULL
          AND msgVar = @sodium
);

-- potassium
INSERT INTO @labs_qa
       SELECT 'potassium',
              COUNT(msgVar),
              COUNT(msgVar),
              NULL,
              MIN(CONVERT( FLOAT, REPLACE(REPLACE(msgVal, '', ''), '>', ''))),
              MAX(CONVERT( FLOAT, REPLACE(REPLACE(msgVal, '', ''), '>', '')))
       FROM lab_final
       WHERE msgVar = @potassium;
UPDATE @labs_qa
  SET
      numNulls =
(
    SELECT COUNT(msgVar)
    FROM lab_final
    WHERE msgVal IS NULL
          AND msgVar = @potassium
);
SELECT *
FROM @labs_qa
